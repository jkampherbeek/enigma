# Enigma - software for research into astrology

## Current version 0.5

Enigma is written in Free Pascal to explore and test the possibilities of astrology.
The current version 0.5 is still in a preliminary state, it only performs calculations of planetary positions, shows a drawing of a chart, and handles database access.

Please note that you need to install both a dll and datafiles from the Swiss Ephemeris which are not in my GitLab project.

To create the database you need to run the test project FPCUnitProject.

You also need to install the font EnigmaAstrology.ttf .

You can find information about this in the technical documentation at http://radixpro.org/documentation/

For more information check the site http://radixpro.org
