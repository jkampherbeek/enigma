# Enigma - Release notes

[TOC]

## Release 0.5  -  2019-04-14

- Enigma is able to show the drawing of a chart.
- Glyphs are used in both the chart drawing and in the tabular overview of positions.
- The exit function works on all levels.
- The start-screen (Dashboard) is redesigned and shows an introductory text.

## Release 0.4  -  2019-01-29

* Enigma now uses a database. It is able to retrieve housesystems, ayanamsha’s and many other items from the database. These objects are not yet shown . 
* A Greenfield for the database is automatically created for testing and will also be used for version control of the database.
* Fixes: no error after cancelling input for chartdata (thanks to Jean Cremers for mentioning this error).
* Minor improvements: if the user enters no seconds for longitude, latitude or time, the seconds are assumed to be zero (thanks to Jean Cremers for the tip).
* Refactoring: started with using interfaces, factories and fakes.

## Release 0.3  -  2018-11-07

* Adds name to input screen for chart data.
* No further functional changes, only internal improvements.

---

## Release 0.2  -  2018-10-10

* Calculation of Sun, Moon, Planets, Pluto and mean node is in place.
* Calculation of housecusps according to Placidus is in place.
* Additional calculations: obliquity of the earth axis, Vertex, Eastpoint.
* All positions are given for the ecliptical, equatorial and the horizontal referencesystem

---

## Release 0.1  -  2018-08-28

* Setup of development environment been done.   	
* A minimal Dashboard is available and a form for the input of chart data. The input is checked for validness.   




