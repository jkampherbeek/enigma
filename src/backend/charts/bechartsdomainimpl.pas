unit BeChartsDomainImpl;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, BeChartsDomain;

type
  THorizontalPosition = class(TInterfacedObject, IHorizontalPosition)
  private
    AltitudeValue, AzimuthValue: Double;
    function GetAltitude: Double;
    function GetAzimuth: Double;
  public
    constructor Create(PAltitude, PAzimuth: Double);
    property Altitude: Double read GetAltitude;
    property Azimuth: Double read GetAzimuth;
  end;


implementation

{ THorizontalPosition }
  constructor THorizontalPosition.Create(PAltitude, PAzimuth: Double);
  begin
    AltitudeValue:= PAltitude;
    AzimuthValue:= PAzimuth;
  end;

  function THorizontalPosition.GetAltitude: Double;
  begin
    Result:= AltitudeValue;
  end;

  function THorizontalPosition.GetAzimuth: Double;
  begin
    Result:= AzimuthValue;
  end;

end.

