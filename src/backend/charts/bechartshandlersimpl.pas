{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit BeChartsHandlersImpl;
{< Implementations for interfaces in BeChartsHandlers. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, BeSharedSeFrontend, XChartsDomain, BeChartsHandlers;

type

  TFullChartHandler = class(TInterfacedObject, IFullChartHandler)
    strict private
      Request: IFullChartRequest;
      SeFrontend: TSeFrontend;
    public
      constructor Create;
      function HandleRequest(PFullChartRequest: IFullChartRequest): IFullChartResponse;
    end;

implementation

uses XChartsDomainImpl, XSharedDomain, BeSwissDelphi ;

{ TFullChartHandler }
  constructor TFullChartHandler.Create;
    begin
      SeFrontend:= TSeFrontend.Create;
    end;

  function TFullChartHandler.HandleRequest(PFullChartRequest: IFullChartRequest): IFullChartResponse;
    var
      CelestialObjects: TIntArray;
      CalculatedObjects: TCelestialObjectFullArray;
      HouseSystemSpec: IHouseSystemSpec;
      HousesResponse: IHousesResponse;
      Location: IValidatedLocation;
      i: Integer;
      Flags: LongInt;
      JulianDay, Obliquity: Double;
    begin
      Request:= PFullChartRequest;
      JulianDay:= SeFrontend.JulDay(Request.Date, Request.Time);
      Obliquity:= SeFrontend.CalculateObliquity(JulianDay);
      Flags:= SEFLG_SWIEPH or SEFLG_SPEED;
      Flags:= Flags or Request.CalculationSettings.ReferenceFrame.Flags;
      Location:= Request.Location;
      CelestialObjects:= Request.CalculationSettings.Objects;
      HouseSystemSpec:= Request.CalculationSettings.HouseSystemSpec;
      SetLength(CalculatedObjects, Length(CelestialObjects));
      for i:= 0 to Length(CelestialObjects)-1 do begin
         CalculatedObjects[i]:= SeFrontend.CalculateCelestialObjectFull(JulianDay, CelestialObjects[i], Flags, Location);
      end;
      HousesResponse:= SeFrontend.CalculateHouses(JulianDay, Obliquity, Location, HouseSystemSpec);
      Result:= TFullChartResponse.Create(Request.GetName, CalculatedObjects, HousesResponse, Request);
    end;


end.

