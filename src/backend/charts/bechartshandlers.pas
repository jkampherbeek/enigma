{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit BeChartsHandlers;
{< Handlers that are specific for Charts. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XChartsDomain;

type

  { Handler for the calculation of a full chart. Returns a FullChartRespponse that contains data for equatorial, ecliptic
  and horizontal coördinates.@br
  - Created: 2018 @br
  - Last update: 2019-03-20 }
  IFullChartHandler = Interface ['{078158F1-B8F7-4E0C-9AF9-EBDAD48EA6CF}']
    function HandleRequest(PFullChartRequest: IFullChartRequest): IFullChartResponse;
  end;


implementation


end.

