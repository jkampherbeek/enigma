{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit BeDbDatabaseUpdaterImpl;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, BeDbDatabaseUpdater, sqldb, db, sqlite3conn, BeDbDao;

type

  TDatabaseUpdater = class(TInterfacedObject, IDatabaseUpdater)
    private
      DatabaseInstance: IDatabaseInstance;
      LookupValueDao: ILookUpValueDao;
      VersionsDao: IVersionsDao;
      HouseSystemsDao: IHouseSystemsDao;
      AyanamshasDao: IAyanamshasDao;
      ConfigurationsDao: IConfigurationsDao;
      GlyphsBodiesDao: IGlyphsBodiesDao;
      GlyphsConfigurationDao: IGlyphsConfigurationDao;
      BodiesDao: IBodiesDao;
      AspectsDao: IAspectsDao;
      OldVersion, NewVersion: String;
      Connection: TSQLite3Connection;
      Transaction: TSQLTransaction;
      SqlQuery: TSQLQuery;
      procedure PerformSelectedUpdates;
      procedure PerformUpdateDDL_0_4;
      procedure PerformUpdateDML_0_4;
      procedure PerformUpdateDDL_0_5;
      procedure PerformUpdateDML_0_5;
      procedure RunDdl(PQuery: String);
      procedure Prepare;
      procedure Finish;
    public
      Constructor Create(PNewVersion: String);
      procedure CreateDbVersion;
  end;


implementation

uses BeDbDaoImpl, BeDbDaoFactories;



  { TDatabaseUpdater }
  constructor TDatabaseUpdater.Create(PNewVersion: String);
  begin
    DatabaseInstance:= TDatabaseInstance.Create;
    NewVersion:= PNewVersion;
    // define old version (access database), for now assume 0.0    { TODO : Lookup current version }
    OldVersion:= '0.0';
  end;

  procedure TDatabaseUpdater.CreateDbVersion;
  begin
    if (OldVersion <> NewVersion) then begin
      PerformSelectedUpdates;
    end;
  end;

  procedure TDatabaseUpdater.PerformSelectedUpdates;
  begin
    if (OldVersion < NewVersion) then begin       { TODO : change comparing versions to support all combinations }
      PerformUpdateDDL_0_4;
      PerformUpdateDML_0_4;
      PerformUpdateDDL_0_5;
      PerformUpdateDML_0_5;
    end;
  end;

  procedure TDatabaseUpdater.RunDdl(PQuery: String);
  var
    Query: String;

  begin
    Query:= PQuery;
    try
      Prepare;
      SqlQuery:= TSQLQuery.Create(Nil);
      SqlQuery.DataBase:= Connection;
      SqlQuery.SQL.Text:= query;
      SqlQuery.ExecSQL;
      Transaction.Commit;
      Finish;
    except
      on E: Exception do begin
        Transaction.Free;
        { TODO : recreate EEnigmaDdlException }
        //raise EEnigmaDdlException.Create('Error while performing the following DDL : ' + Query
        //                                + ' . Orignal exception : ' + E.Message);
      end;
    end;
  end;


  procedure TDatabaseUpdater.Prepare;
    begin
      Connection:= DatabaseInstance.Connection;
      Connection.Open;
      Transaction:= TSQLTransaction.Create(Connection);
      Transaction.DataBase:= Connection;
      Connection.Transaction:= Transaction;
      Transaction.Active:= true;
  end;

   procedure TDatabaseUpdater.Finish;
    begin
      Transaction.EndTransaction;
      Transaction.Free;
      DatabaseInstance.ReleaseConnection;
  end;

  procedure TDatabaseUpdater.PerformUpdateDDL_0_4;
  var
    ApplicationVersionsTableDdl: String = 'create table "ApplicationVersions"'+
                                '("id" integer not null primary key autoincrement,' +
                                '"Major" integer not null, ' +
                                '"Minor" integer not null, ' +
                                '"Micro" integer not null, ' +
                                '"Date" char(10) not null );';
    DatabaseVersionsTableDdl: String = 'create table "DatabaseVersions"'+
                                '("id" integer not null primary key autoincrement,' +
                                '"Major" integer not null, ' +
                                '"Minor" integer not null, ' +
                                '"Micro" integer not null, ' +
                                '"Date" char(10) not null );';
    AspectCategoriesTableDdl: String = 'create table "AspectCategories"'+
                               '("id" integer not null primary key,' +
                               '"Name" char(120) not null,' +
                               '"Description" char(255) not null);';
    BodyCategoriesTableDdl: String = 'create table "BodyCategories"'+
                               '("id" integer not null primary key,' +
                               '"Name" char(120) not null,' +
                               '"Description" char(255) not null);';
    ProjectionTypesTableDdl: String = 'create table "ProjectionTypes"'+
                               '("id" integer not null primary key,' +
                               '"Name" char(120) not null,' +
                               '"Description" char(255) not null);';
    ObserverPositionsTableDdl: String = 'create table "ObserverPositions"'+
                               '("id" integer not null primary key,' +
                               '"Name" char(120) not null,' +
                               '"Description" char(255) not null);';
    CoordinateSystemsTableDdl: String = 'create table "CoordinateSystems"'+
                               '("id" integer not null primary key,' +
                               '"Name" char(120) not null,' +
                               '"Description" char(255) not null);';
    HouseSystemsTableDdl: String = 'create table "HouseSystems"'+
                               '("id" integer not null primary key,' +
                               '"Name" char(120) not null,' +
                               '"Description" char(255) not null,' +
                               '"NrOfHouses" integer not null, ' +
                               '"CounterClockWise" integer not null, ' +
                               '"QuadrantSystem" integer not null, ' +
                               '"CuspIsStart" integer not null);' ;
    AyanamshasTableDdl: String = 'create table "Ayanamshas"'+
                               '("id" integer not null primary key,' +
                               '"Name" char(120) not null,' +
                               '"Description" char(255) not null, ' +
                               '"Offset2000" double not null);';
    ConfigurationsTableDdl: String = 'create table "Configurations"'+
                               '("id" integer not null primary key autoincrement,' +
                               '"Name" char(120) not null,' +
                               '"Description" char(255) not null, ' +
                               '"BaseOrbAspects" real not null,' +
                               '"BaseOrbMidpoints" real not null,' +
                               '"CoordinateSystem" integer not null references CoordinateSystems(id) on update cascade, ' +
                               '"Ayanamsha" integer not null references Ayanamshas(id) on update cascade, '+
                               '"HouseSystem" integer not null references HouseSystems(id) on update cascade, '+
                               '"ObserverPosition" integer not null references ObserverPositions(id) on update cascade, '+
                               '"ProjectionType" integer not null references ProjectionTypes(id) on update cascade);';
    Configurations_ConfigurationsTableDdl: String = 'create table "Configurations_Configurations"'+
                               '("Configuration" integer not null references Configurations(id) on update cascade, '+
                               '"Parent" integer not null references Configurations(id) on update cascade);';
    BodiesTableDdl: String = 'create table "Bodies"'+
                               '("id" integer not null primary key autoincrement,' +
                               '"Name" char(120) not null,' +
                               '"BodyCategory" integer not null references BodyCategories(id) on update cascade); ';
    Bodies_ConfigurationsTableDdl: String = 'create table "Bodies_Configurations"'+
                               '("Body" integer not null references Bodies(id) on update cascade, '+
                               '"Configuration" integer not null references Configurations(id) on update cascade, '+
                               '"BaseOrbPercentage" integer not null);';
    AspectsTableDdl: String = 'create table "Aspects"'+
                               '("id" integer not null primary key autoincrement,' +
                               '"Name" char(120) not null,' +
                               '"Angle" real not null, '+
                               '"AspectCategory" integer not null references AspectCategories(id));';
    Aspects_ConfigurationsTableDdl: String = 'create table "Aspects_Configurations"'+
                               '("Aspect" integer not null references Aspects(id) on update cascade, '+
                               '"Configuration" integer not null references Configurations(id) on update cascade, '+
                               '"BaseOrbPercentage" integer not null);';
    RatingsTableDdl: String = 'create table "Ratings"'+
                               '("id" integer not null primary key autoincrement,' +
                               '"Name" char(10) not null,' +
                               '"Description" char(255) not null);';
    ChartTypesTableDdl: String = 'create table "ChartTypes"'+
                               '("id" integer not null primary key autoincrement,' +
                               '"Name" char(120) not null,' +
                               '"Description" char(255) not null);';
    CategoriesTableDdl: String = 'create table "Categories"'+
                               '("id" integer not null primary key autoincrement,' +
                               '"Name" char(120) not null,' +
                               '"Description" char(255) not null);';
    ChartsTableDdl: String = 'create table "Charts"'+
                               '("id" integer not null primary key autoincrement,' +
                               '"Name" char(255) not null,' +
                               '"Date" datetime not null, '+
                               '"UT" real not null, '+
                               '"JDET" real not null, '+
                               '"JDUT" real not null, '+
                               '"Location" char(255) not null, '+
                               '"Longitude" real not null, '+
                               '"Latitude" real not null, '+
                               '"OriginalInput" char(255) not null, '+
                               '"Rating" integer not null references Ratings(id) on update cascade, ' +
                               '"ChartType" integer not null references ChartTypes(id) on update cascade);';
    Charts_CategoriesTableDdl: String = 'create table "Charts_Categories"'+
                               '("Category" integer not null references Categories(id) on update cascade, ' +
                               '"Chart" integer not null references Charts(id) on update cascade,' +
                               '"BodyCategory" integer not null references BodyCategories(id) on update cascade); ';


  begin

    RunDdl(ApplicationVersionsTableDdl);
    RunDdl(DatabaseVersionsTableDdl);
    RunDdl(CategoriesTableDdl);
    RunDdl(AspectCategoriesTableDdl);
    RunDdl(BodyCategoriesTableDdl);
    RunDdl(ProjectionTypesTableDdl);
    RunDdl(ObserverPositionsTableDdl);
    RunDdl(CoordinateSystemsTableDdl);
    RunDdl(HouseSystemsTableDdl);
    RunDdl(AyanamshasTableDdl);
    RunDdl(ConfigurationsTableDdl);
    RunDdl(Configurations_ConfigurationsTableDdl);
    RunDdl(BodiesTableDdl);
    RunDdl(Bodies_ConfigurationsTableDdl);
    RunDdl(AspectsTableDdl);
    RunDdl(RatingsTableDdl);
    RunDdl(ChartTypesTableDdl);
    RunDdl(ChartsTableDdl);
    RunDdl(Charts_CategoriesTableDdl);
  end;

  procedure TDatabaseUpdater.PerformUpdateDML_0_4;
  begin

    // CoordinateSystems
    LookupValueDao:= TFactoryLookUpValueDao.Create('CoordinateSystems').GetInstance;
    LookupValueDao.Insert(0, 'Ecliptical', 'Measured alongside the ecliptic. Uses longitude and latitude. Standard.');
    LookupValueDao.Insert(1, 'Equatorial', 'Measured alongside the celestial equator. Uses right ascension and declination.');
    LookupValueDao.Insert(2, 'Horizontal', 'Measured alongside the horizon. Uses azimuth (compass direction) and altitude (height).');

    // ObserverPositions
    LookupValueDao:= TFactoryLookUpValueDao.Create('ObserverPositions').GetInstance;
    LookupValueDao.Insert(0, 'Geocentric', 'Measured from the centre of the Earth. Omits the corrections for parallax. Standard.' );
    LookupValueDao.Insert(1, 'Topocentric', 'Measured from the location of the person or event. Takes parallax into account.' );
    LookupValueDao.Insert(2, 'Heliocentric', 'Measured from the centre of the Sun. Omits Sun, Moon and Lunar nodes will be omitted and Earth will be added.' );

    // ProjectionTypes
    LookupValueDao:= TFactoryLookUpValueDao.Create('ProjectionTypes').GetInstance;
    LookupValueDao.Insert(0, 'Right', 'Projection in a right angle, Standard.');
    LookupValueDao.Insert(1, 'Oblique', 'Projection in an oblique angle, as used by the School of Ram.');

    // AspectCategories
    LookupValueDao:= TFactoryLookUpValueDao.Create('AspectCategories').GetInstance;
    LookupValueDao.Insert(0, 'Major', 'Conjunct, Sextile, Square, Trigon, Opposition');
    LookupValueDao.Insert(1, 'Minor', 'Semisextile, Semisquare, Quintile, Sesquiquadrate, Biquintile, Quincunx/Inconjunct');
    LookupValueDao.Insert(2, 'Micro', 'Semiquintile, Nonile, Septile');
    LookupValueDao.Insert(3, 'Equatorial', 'Parallel, Oppositionparallel');

    // BodyCategories
    LookupValueDao:= TFactoryLookUpValueDao.Create('BodyCategories').GetInstance;
    LookupValueDao.Insert(0, 'Classic bodies', 'Visible bodies, Sun and Moon up to Saturn.');
    LookupValueDao.Insert(1, 'Modern bodies', 'Uranus, Neptune and Pluto.');
    LookupValueDao.Insert(2, 'Extra Plutonians', 'MakeMake etc.');
    LookupValueDao.Insert(3, 'Asteroids', 'Ceres, Vesta etc.');
    LookupValueDao.Insert(4, 'Centaurs', 'Cheiron, Nexus, Pholus');
    LookupValueDao.Insert(5, 'Intersections', 'Nodes, aphelia and perihelia');
    LookupValueDao.Insert(6, 'Hypothetical points', 'School of Ram, Uranian etc.');

    // Ratings
    LookupValueDao:= TFactoryLookUpValueDao.Create('Ratings').GetInstance;
    LookupValueDao.Insert(0, 'AA', 'Accurate/accurate. As recorded by family or state.');
    LookupValueDao.Insert(1, 'A', 'Quoted by the person, kin, friend, or associate');
    LookupValueDao.Insert(2, 'B', 'Biography or autobiography.');
    LookupValueDao.Insert(3, 'C', 'Caution, no source.');
    LookupValueDao.Insert(4, 'DD', 'Dirty data. Conflicting quotes that are unqualified.');
    LookupValueDao.Insert(5, 'X', 'No time of birth.');
    LookupValueDao.Insert(6, 'XX', 'No date of birth.');

    // Charttypes
    LookupValueDao:= TFactoryLookUpValueDao.Create('Charttypes').GetInstance;
    LookupValueDao.Insert(0, 'Birthchart', 'The common geniature or birthchart.');
    LookupValueDao.Insert(1, 'Event', 'The beginning of an event.');
    LookupValueDao.Insert(2, 'Horary', 'Chart for a question.');

    // Categories
    LookupValueDao:= TFactoryLookUpValueDao.Create('Categories').GetInstance;
    LookupValueDao.Insert(0, 'None', 'No categories defined.');

    // ApplicationsVersions
    VersionsDao:= TFactoryVersionsDao.Create.GetInstance('ApplicationVersions');
    VersionsDao.Insert(0, 0, 1, 0, '2018-08-28');
    VersionsDao.Insert(1, 0, 4, 0, '2019-01-01');

    // DatabaseVersions
    VersionsDao:= TFactoryVersionsDao.Create.GetInstance('DatabaseVersions');
    VersionsDao.Insert(0, 0, 1, 0, '2019-01-01');

    // HouseSystems   parametrs: Id, Name, Description, NrOfHouses, CounterClockwise, QuadrantSystem, CuspIsStart
    HouseSystemsDao:= TFactoryHouseSystemsDao.Create.GetInstance;
    HouseSystemsDao.Insert(0, 'Placidus', 'Timebased system. Each cusp is on a specific part of its own semi-arc.',
                               12, true, true, true);
    HouseSystemsDao.Insert(1, 'Regiomontanus', 'Equator based system. Divides the equator using circles through the northpoint and '+
                              'southpoint on the horizon.', 12, true, true, true);
    HouseSystemsDao.Insert(2, 'Campanus', 'Space based system. Divides the celestial globe using circles through the northpoint and '+
                              'southpoint on the horizon.', 12, true, true, true);
    HouseSystemsDao.Insert(3, 'Whole Sign Houses', 'Ecliptic based system. Divides the ecliptic in the usual signs and takes '+
                              'sign on the horizon as the first house. One of the first systems.', 12, true, true, true);
    HouseSystemsDao.Insert(4, 'Alcabitius', 'Equator based system. Trisects the quadrants of the equator to find the '+
                              'cusps on the ecliptic. ', 12, true, true, true);
    HouseSystemsDao.Insert(5, 'Porphyri', 'Ecliptic based system. Trisects the quadrants of the ecliptic directly. ',
                               12, true, true, true);
    HouseSystemsDao.Insert(6, 'Koch / GOH - published', 'Timebased system. Divides the semi-arc of the MC and calculates '+
                              'the corresponding acendants. Gives partly incorrect results, see documentation. ',
                               12, true, true, true);
    HouseSystemsDao.Insert(7, 'Equals Asc', 'Ecliptic based system. Divides the ecliptic in equal parts, starting with '+
                              'the ascendant', 12, true, false, true);
    HouseSystemsDao.Insert(8, 'Equals MC', 'Ecliptic based system. Divides the ecliptic in equal parts, starting with '+
                              'the MC', 12, true, false, true);
    HouseSystemsDao.Insert(9, 'Krusinsky', 'Space based system. Divides the celestial globe using circles through the ascendant/descendant '+
                              'axis', 12, true, true, true);
    HouseSystemsDao.Insert(10,'Morinus', 'Equator based system. Divides the equator in equal parts and uses the '+
                              'intersections wioth the ecliptic as cusps. Does not take geographic latitude into account.',
                               12, true, false, true);
    HouseSystemsDao.Insert(11,'APC / Haley', 'Mixed system. Divides a small circle, parallel to the equator and running through '+
                              'the ascendant in equal parts. Intermediate cusps are not oppositional. Used in the School of Ram',
                               12, true, true, true);
    HouseSystemsDao.Insert(12,'Gauquelin', 'Time based system. Comparable to Placidus but uses 36 houses.',
                               36, true, false, true);
    HouseSystemsDao.Insert(13,'Axial / Meridian / Zariel', 'Equator based system. Divides the equator in 12 equal parts ' +
                              'starting with the Right Ascension of the MC. Used in Uranian Astrology.',
                              12, true, false, true);
    HouseSystemsDao.Insert(14,'Carter', 'Equator based system. Divides the equator in 12 equal parts, ' +
                              'starting with the East Point.',
                               12, true, false, true);
    HouseSystemsDao.Insert(15,'Azimuthal / Horizontal', 'Horizon based system. Divides the horizon in 12 equal parts ' +
                              'starting with the East Point.',
                               12, true, false, true);
    HouseSystemsDao.Insert(16,'Topocentric', 'Not based on the usual coördinates. Uses a cone to construct the cusps.',
                               12, true, true, true);
    HouseSystemsDao.Insert(17, 'Equal/Aries', 'Uses signs as houses, starting with Aries.', 12, true, false, true);
    HouseSystemsDao.Insert(18, 'Sunshine/Makransky 1', 'Divides the semiarcs of the Sun and draws great circles through ' +
                               'the divisions and the north and south point. Intermediate cusps are not oppositional.',
                               12, true, false, true);
    HouseSystemsDao.Insert(19, 'Sunshine/Makransky 2', 'Remarks not yet written', 12, true, false, true); { TODO : describe second version of Sunshine houses }
    HouseSystemsDao.Insert(20, 'Vehlow', 'Ecliptic based system. Divides the ecliptic in equal parts, starting with the '+
                               'ascendant. All cusps are the center of the corresponding house.', 12, true, false, false);

  // Ayanamsha's      { TODO : Complete description and offsets for Ayanamsa's }
    AyanamshasDao:= TFactoryAyanamshasDao.Create.GetInstance;
    AyanamshasDao.Insert(0, 'None', 'Tropical: no ayanamsha', 0.0);
    AyanamshasDao.Insert(1, 'Fagan/Bradley', 'Based on Spica but disregarding its proper motion. Proposed by Cyril Fagan and Tonad Bradley around 1950', 0.0);
    AyanamshasDao.Insert(2, 'Lahiri', 'Official standard in India, as proposed by S. B. Dixit in 1896, but named after Nirmala Chandra Lahiri', 0.0);
    AyanamshasDao.Insert(3, 'De Luce', 'todo', 0.0);
    AyanamshasDao.Insert(4, 'Raman', 'todo', 0.0);
    AyanamshasDao.Insert(5, 'Usha/Shashi', 'Startpoint near the star Revatî', 0.0);
    AyanamshasDao.Insert(6, 'Krishnamurti', 'Spica at 180°4' + '′' + '51″, as proposed by K. S. Krishnamurti', 0.0);
    AyanamshasDao.Insert(7, 'Djwhal Khul', 'todo', 0.0);
    AyanamshasDao.Insert(8, 'Yukteshwar', 'todo', 0.0);
    AyanamshasDao.Insert(9, 'J.N. Bhasin', 'todo', 0.0);
    AyanamshasDao.Insert(10, 'Babylonian/Kugler 1', 'First Babylonian version from three, as propesed by Franz Xaver Kugler arouond 1900', 0.0);
    AyanamshasDao.Insert(11, 'Babylonian/Kugler 2', 'Second Babylonian version from three, as propesed by Franz Xaver Kugler arouond 1900', 0.0);
    AyanamshasDao.Insert(12, 'Babylonian/Kugler 3', 'Third Babyloinan version from three, as propesed by Franz Xaver Kugler arouond 1900', 0.0);
    AyanamshasDao.Insert(13, 'Babylonian/Huber', 'Mean value in Babylonian texts as calculated by Peter Huber in 1958', 0.0);
    AyanamshasDao.Insert(14, 'Babylonian/Eta Piscium', 'Starting with the star Eta Piscium as proposed by Raymond Mercier on 1977', 0.0);
    AyanamshasDao.Insert(15, 'Babylonian/Aldebaran = 15 Tau', 'Defined by Aldebaran and Antares on 15 degrees in fixed signs, as proposed by Ronald Bradley', 0.0);
    AyanamshasDao.Insert(16, 'Hipparchus', 'As used by Hipparchus according to Raymond Mercier', 0.0);
    AyanamshasDao.Insert(17, 'Sassanian', 'Newly defined during a Sassanian reform of astronomical tables', 0.0);
    AyanamshasDao.Insert(18, 'Galact. Center = 0 Sag', 'Based on the Galactic center at 0° Sagittarius', 0.0);
    AyanamshasDao.Insert(19, 'J2000', 'todo', 0.0);
    AyanamshasDao.Insert(20, 'J1900', 'todo', 0.0);
    AyanamshasDao.Insert(21, 'B1950', 'todo', 0.0);
    AyanamshasDao.Insert(22, 'Suryasiddhanta', 'As described in the Sûryasiddhânta, according to Raymond Mercier', 0.0);
    AyanamshasDao.Insert(23, 'Suryasiddhanta, mean Sun', 'Same as Sûryasiddhânta, but taken the position of the Sun at a specific starting point', 0.0);
    AyanamshasDao.Insert(24, 'Aryabhata', 'Variant at Suryasiddhanta, according to Govindasvamin (850 CE)', 0.0);
    AyanamshasDao.Insert(25, 'Aryabhata, mean Sun', 'Variant at Suryasiddhanta, mean Sun, according to Govindasvamin (850 CE)', 0.0);
    AyanamshasDao.Insert(26, 'SS Revati', 'todo', 0.0);
    AyanamshasDao.Insert(27, 'SS Citra', 'Based on Spica at 180° in polar longitude (ecliptic longitude, but projection on meridian lines', 0.0);
    AyanamshasDao.Insert(28, 'True Citra', 'Based on Spica at 180° in ecliptic longitude', 0.0);
    AyanamshasDao.Insert(29, 'True Revati', 'Based on Revati/zePsc 359°50 in ecliptic longitude’ ', 0.0);
    AyanamshasDao.Insert(30, 'True Pushya', 'Based on Pushya/deCnC at 106° in ecliptic longitude', 0.0);
    AyanamshasDao.Insert(31, 'Galactic Center (Gil Brand)', 'Galactic centre at the golden section between 0° Scorpion and 0° Aquarius, as proposed by Rafel Gil Brand', 0.0);
    AyanamshasDao.Insert(32, 'Galactic Equator (IAU1958)', 'todo', 0.0);
    AyanamshasDao.Insert(33, 'Galactic Equator', 'todo', 0.0);
    AyanamshasDao.Insert(34, 'Galactic Equator mid-Mula', 'todo', 0.0);
    AyanamshasDao.Insert(35, 'Skydram (Mardyks)', 'Based on 30° on the autumn equinox 1998,poposed by Raymond Mardyks in 1991', 0.0);
    AyanamshasDao.Insert(36, 'True Mula (Chandra Hari)', 'todo', 0.0);
    AyanamshasDao.Insert(37, 'Dhruva/Gal.Center/Mula', 'A great circle through celestial north pole and galactic center intersects ecliptic in the cnter of the nakshatra Mula. Proposed by Ernst Wilhelm in 2004', 0.0);
    AyanamshasDao.Insert(38, 'Aryabhata 522', 'todo', 0.0);
    AyanamshasDao.Insert(39, 'Babylonian/Britton', 'Mean value in Babylonian texts as calculated by John P.Britton in 2010', 0.0);
    AyanamshasDao.Insert(40, 'Galact. Center = 0 Cap.', 'Based on the Galactic center at 0° Capricorn, as proposed by David Cochrane in 2017', 0.0);

    // Bodies                             { TODO : Complete list of bodies }
    BodiesDao:= TFactoryBodiesDao.Create.GetInstance;
    BodiesDao.Insert(0, 'Sun', 0);
    BodiesDao.Insert(1, 'Moon', 0);
    BodiesDao.Insert(2, 'Mercury', 0);
    BodiesDao.Insert(3, 'Venus', 0);
    BodiesDao.Insert(4, 'Earth', 0);
    BodiesDao.Insert(5, 'Mars', 0);
    BodiesDao.Insert(6, 'Jupiter', 0);
    BodiesDao.Insert(7, 'Saturn', 0);
    BodiesDao.Insert(8, 'Uranus', 1);
    BodiesDao.Insert(9, 'Neptune',1);
    BodiesDao.Insert(10, 'Pluto',1);
    BodiesDao.Insert(11, 'Cheiron',4);
    BodiesDao.Insert(12, 'Mean Node',5);
    BodiesDao.Insert(13, 'Oscillatng Node',5);

    // Aspects
    AspectsDao:= TFactoryAspectsDao.Create.GetInstance;
    AspectsDao.Insert(0, 'Conjunction', 0.0, 0);
    AspectsDao.Insert(1, 'Opposition', 180.0, 0);
    AspectsDao.Insert(2, 'Triangle', 120.0, 0);
    AspectsDao.Insert(3, 'Square', 90.0, 0);
    AspectsDao.Insert(4, 'Sextile', 60.0, 0);
    AspectsDao.Insert(5, 'Parallel', 360.0, 3);
    AspectsDao.Insert(6, 'Opposition Parallel',360.0, 3);
    AspectsDao.Insert(7, 'Semisextile', 30.0, 1);
    AspectsDao.Insert(8, 'Semisquare', 45.0, 1);
    AspectsDao.Insert(9, 'Quintile', 72.0, 1);
    AspectsDao.Insert(10, 'Sesquiquadrate', 135.0, 1);
    AspectsDao.Insert(11, 'Biquintile', 144.0, 1);
    AspectsDao.Insert(12, 'Quincunx/Inconjunct', 150.0, 1);
    AspectsDao.Insert(13, 'Septile', 51.4285714, 2);
    AspectsDao.Insert(14, 'Biseptile', 102.8571429, 2);
    AspectsDao.Insert(15, 'Triseptile', 154.2857143, 2);
    AspectsDao.Insert(16, 'Vigintile', 18.0, 2);
    AspectsDao.Insert(17, 'Semiquintile', 36.0, 2);
    AspectsDao.Insert(18, 'Tridecile', 108.0, 2);
    AspectsDao.Insert(19, 'Novile/Nonile', 40.0, 2);
    AspectsDao.Insert(20, 'Binovile/Binonile', 80.0, 2);
    AspectsDao.Insert(21, 'Quadranovile/Quadranonile', 160.0, 2);
    AspectsDao.Insert(22, 'Undecile', 33.0, 2);
    AspectsDao.Insert(23, 'Centile/Sentagon', 100.0, 2);

    // Configurations     { TODO : add id of parent }
    ConfigurationsDao:= TFactoryConfigurationsDao.Create.GetInstance;
    { values: Id, Name, Description, BaseOrbAspects, PBaseOrbMidpoints, CoordinateSystem, Ayanamsha, HouseSystem,
              ObserverPosition, ProjectionType }
    ConfigurationsDao.Insert(0, 'Standard Western', 'Default values as used in western tropical astrology',
                             10.0, 1.6, 0, 0, 0, 0, 0);
    //ConfigurationsDao.Insert(1, 'School of Ram', 'Settings for the dutch School of Ram',
    //                         7.0, nil, nil, nil, 11, nil, 1);

  end;


  procedure TDatabaseUpdater.PerformUpdateDDL_0_5;
    var
      SupportedGlyphsBodiesDdl: String = 'create table GlyphsBodies '+
                                '(BodyId integer not null ,' +
                                'Glyph char(1) not null, ' +
                                'foreign key (BodyId) references Bodies(id), ' +
                                'primary key(BodyId, Glyph));';
      GlyphsConfigurationDdl: String = 'create table GlyphsConfiguration '+
                                '(ConfigId integer not null ,' +
                                'ItemId integer not null ,' +
                                'Category char(1) not null , ' +
                                'Glyph char(1) not null , ' +
                                'foreign key (ConfigId) references Configurations(id), ' +      { TODO : combine all items tables and add fk at this location }
                                'primary key(ConfigId, ItemId));';

    begin
         RunDdl(SupportedGlyphsBodiesDdl);
         RunDdl(GlyphsConfigurationDdl);
    end;

  procedure TDatabaseUpdater.PerformUpdateDML_0_5;
    begin
      { GlyphsBodiesDao }
      GlyphsBodiesDao := TFactoryGlyphsBodiesDao.Create.GetInstance;
      GlyphsBodiesDao.Insert(0, 'a');   // Sun
      GlyphsBodiesDao.Insert(0, 'l');   // Sun School of Ram
      GlyphsBodiesDao.Insert(1, 'b');   // Moon
      GlyphsBodiesDao.Insert(2, 'c');   // Mercure
      GlyphsBodiesDao.Insert(2, 'm');   // Mercure (Vulcan) School of Ram
      GlyphsBodiesDao.Insert(3, 'd');   // Venus
      GlyphsBodiesDao.Insert(4, 'e');   // Earth
      GlyphsBodiesDao.Insert(5, 'f');   // Mars
      GlyphsBodiesDao.Insert(5, 'n');   // Mars School of Ram
      GlyphsBodiesDao.Insert(6, 'g');   // Jupiter
      GlyphsBodiesDao.Insert(6, 'o');   // Jupiter School of Ram
      GlyphsBodiesDao.Insert(7, 'h');   // Saturn
      GlyphsBodiesDao.Insert(7, 'p');   // Saturn School of Ram
      GlyphsBodiesDao.Insert(8, 'i');   // Uranus (Herschel symbol)
      GlyphsBodiesDao.Insert(8, 'q');   // Uranus (arrow symbol)
      GlyphsBodiesDao.Insert(8, 'r');   // Uranus School of Ram
      GlyphsBodiesDao.Insert(9, 'j');   // Neptune
      GlyphsBodiesDao.Insert(9, 's');   // Neptune School of Ram
      GlyphsBodiesDao.Insert(10, 'k');  // Pluto (PL symbol)
      GlyphsBodiesDao.Insert(10, 't');  // Pluto School of Ram
      GlyphsBodiesDao.Insert(10, 'u');  // Pluto Thomas Ring
      GlyphsBodiesDao.Insert(10, 'v');  // Pluto third alternative
      GlyphsBodiesDao.Insert(10, 'ä');  // Pluto fourth alternative
      GlyphsBodiesDao.Insert(10, 'è');  // Pluto fifth alternative
      GlyphsBodiesDao.Insert(11, 'w');  // Chiron
      GlyphsBodiesDao.Insert(11, 'x');  // Chiron CH symbol
      GlyphsBodiesDao.Insert(12, '{');  // Lunar Node North

      { GlyphsConfigurationDao }
      GlyphsConfigurationDao:= TFactoryGlyphsConfigurationDao.Create.GetInstance;;
      GlyphsConfigurationDao.Insert(0, 0, 'O', 'a');   // Sun
      GlyphsConfigurationDao.Insert(0, 1, 'O', 'b');   // Moon
      GlyphsConfigurationDao.Insert(0, 2, 'O', 'c');   // Mercure
      GlyphsConfigurationDao.Insert(0, 3, 'O', 'd');   // Venus
      GlyphsConfigurationDao.Insert(0, 4, 'O', 'e');   // Earth
      GlyphsConfigurationDao.Insert(0, 5, 'O', 'f');   // Mars
      GlyphsConfigurationDao.Insert(0, 6, 'O', 'g');   // Jupiter
      GlyphsConfigurationDao.Insert(0, 7, 'O', 'h');   // Saturn
      GlyphsConfigurationDao.Insert(0, 8, 'O', 'i');   // Uranus
      GlyphsConfigurationDao.Insert(0, 9, 'O', 'j');   // Neptune
      GlyphsConfigurationDao.Insert(0, 10, 'O', 'k');  // Pluto
      GlyphsConfigurationDao.Insert(0, 11, 'O', 'w');  // Chiron

    end;

end.



