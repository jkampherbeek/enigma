{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit BeDbDaoFactories;
{< Factories for dao's. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, BeDbDao;

type

{ Factory for DaoSelectHelper. @br
 - Created: 2019-01-06 @br
 - Last update: 2019-01-06  }
 TFactoryDaoSelectHelper = class
   strict private
     DatabaseInstance: IDatabaseInstance;
   public
     function GetInstance: IDaoSelectHelper;
 end;

 { Factory for DMLCudHelper. @br
 - Created: 2019-01-06 @br
 - Last update: 2019-01-06  }
 TFactoryDaoDMLCudHelper = class
   strict private
     DatabaseInstance: IDatabaseInstance;
   public
     function GetInstance: IDaoDMLCudHelper;
 end;

 { Factory for LookUpValueDao. @br
 - Created: 2019-01-06 @br
 - Last update: 2019-01-21  }
 TFactoryLookUpValueDao = class
   strict private
     DaoselectHelper: IDaoselectHelper;
     DaoDMLCudHelper: IDaoDMLCudHelper;
     TableName: String;
   public
     Constructor create(PTableName: String);
     function GetInstance: ILookUpValueDao;
 end;

 { Factory for VersionsDao. @br
 - Created: 2019-01-06 @br
 - Last update: 2019-01-06  }
 TFactoryVersionsDao = class
   strict private
     DaoselectHelper: IDaoselectHelper;
     DaoDMLCudHelper: IDaoDMLCudHelper;
     TableName: String;
   public
     function GetInstance(PTableName: String): IVersionsDao;
 end;

 { Factory for HouseSystems. @br
 - Created: 2019-01-06 @br
 - Last update: 2019-01-06  }
 TFactoryHouseSystemsDao = class
   strict private
     DaoselectHelper: IDaoselectHelper;
     DaoDMLCudHelper: IDaoDMLCudHelper;
   public
     function GetInstance: IHouseSystemsDao;
 end;

 { Factory for Ayanamshas. @br
 - Created: 2019-01-06 @br
 - Last update: 2019-01-06  }
 TFactoryAyanamshasDao = class
   strict private
     DaoselectHelper: IDaoselectHelper;
     DaoDMLCudHelper: IDaoDMLCudHelper;
   public
     function GetInstance: IAyanamshasDao;
 end;

 { Factory for Bodies. @br
 - Created: 2019-01-06 @br
 - Last update: 2019-01-06  }
 TFactoryBodiesDao = class
   strict private
     DaoselectHelper: IDaoselectHelper;
     DaoDMLCudHelper: IDaoDMLCudHelper;
   public
     function GetInstance: IBodiesDao;
 end;

 { Factory for Aspects. @br
 - Created: 2019-01-07 @br
 - Last update: 2019-01-07  }
 TFactoryAspectsDao  = class
   strict private
     DaoselectHelper: IDaoselectHelper;
     DaoDMLCudHelper: IDaoDMLCudHelper;
   public
     function GetInstance: IAspectsDao;
 end;

 { Factory for Configurations. @br
 - Created: 2019-01-11 @br
 - Last update: 2019-01-11  }
 TFactoryConfigurationsDao = class
   strict private
     DaoselectHelper: IDaoselectHelper;
     DaoDMLCudHelper: IDaoDMLCudHelper;
   public
     function GetInstance: IConfigurationsDao;
 end;

 { Factory for GlyphsBodies (supported combinations for glyphs and bodies). @br
 - Created: 2019-02-10 @br
 - Last update: 2019-02-10  }
 TFactoryGlyphsBodiesDao = class
 strict private
   DaoselectHelper: IDaoselectHelper;
   DaoDMLCudHelper: IDaoDMLCudHelper;
 public
   function GetInstance: IGlyphsBodiesDao;
 end;

 { Factory for GlyphsConfiguration (selected combinations for glyphs and items in a specific configuration). @br
 - Created: 2019-02-13 @br
 - Last update: 2019-02-13  }
 TFactoryGlyphsConfigurationDao = class
 strict private
   DaoselectHelper: IDaoselectHelper;
   DaoDMLCudHelper: IDaoDMLCudHelper;
 public
   function GetInstance: IGlyphsConfigurationDao;
 end;



implementation

uses
  BeDbDaoImpl;

{ TFactoryDaoSelectHelper }
function TFactoryDaoSelectHelper.GetInstance: IDaoSelectHelper;
  begin
    Result:= TDaoSelectHelper.Create(TDatabaseInstance.Create);
  end;

{ TFactoryDaoDMLCudHelper }
function TFactoryDaoDMLCudHelper.GetInstance: IDaoDMLCudHelper;
  begin
    Result:= TDaoDMLCudHelper.Create(TDatabaseInstance.Create);
  end;

{ TFactoryLookUpValueDao }
constructor TFactoryLookUpValueDao.Create(PTableName: String);
  begin
    TableName:= PTableName;;
  end;

function TFactoryLookUpValueDao.GetInstance: ILookUpValueDao;
  begin
    Result:= TLookUpValueDao.Create(TFactoryDaoSelectHelper.Create.GetInstance,
                                    TFactoryDaoDMLCudHelper.Create.Getinstance, TableName);
  end;

{ TFactoryVersionsDao }
function TFactoryVersionsDao.GetInstance(PTableName: String): IVersionsDao;
  begin
    Result:= TVersionsDao.Create(TFactoryDaoSelectHelper.Create.GetInstance,
                                 TFactoryDaoDMLCudHelper.Create.Getinstance, PTableName);
  end;

{ TFactoryHouseSystemsDao }
function TFactoryHouseSystemsDao.GetInstance: IHouseSystemsDao;
  begin
    Result:= THouseSystemsDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
  end;

{ TFactoryAyanamshasDao }
function TFactoryAyanamshasDao.GetInstance: IAyanamshasDao;
  begin
    Result:= TAyanamshasDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
  end;

{ TFactoryBodiesDao }
function TFactoryBodiesDao.GetInstance: IBodiesDao;
  begin
    Result:= TBodiesDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
  end;

{ TFactoryBodiesDao }
function TFactoryAspectsDao.GetInstance: IAspectsDao;
  begin
    Result:= TAspectsDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
  end;

{ TFactoryConfigurationsDao }
function TFactoryConfigurationsDao.GetInstance: IConfigurationsDao;
  begin
    Result:= TConfigurationsDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
  end;

{ TFactoryGlyphsBodiesDao }
function TFactoryGlyphsBodiesDao.GetInstance: IGlyphsBodiesDao;
  begin
    Result:= TGlyphsBodiesDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
  end;

{ TFactoryGlyphsConfigurationDao }
function TFactoryGlyphsConfigurationDao.GetInstance: IGlyphsConfigurationDao;
  begin
    Result:= TGlyphsConfigurationDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
  end;




end.

