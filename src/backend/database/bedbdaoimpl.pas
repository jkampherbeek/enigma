{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit BeDbDaoImpl;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, sqldb, sqlite3conn, BeDbDao, XChartsDomain;

type

  TDatabaseInstance = class(TInterfacedObject, IDatabaseInstance)
    strict private
      Connection: TSQLite3Connection;
    function GetConnection: TSQLite3Connection;
    procedure ReleaseConnection;
  end;

  TDaoSelectHelper = class(TInterfacedObject, IDaoSelectHelper)
    strict private
      DatabaseInstance: IDatabaseInstance;
      SqlQuery: TSQLQuery;
      Connection: TSQLite3Connection;
      SQLTransaction: TSQLTransaction;
    public
      constructor Create(PDatabaseInstance: TDatabaseInstance);
      function InitializeSelect(PQueryText: String): TSQLQuery;
      procedure Release;
  end;

  TDaoDMLCudHelper = class(TInterfacedObject, IDaoDMLCudHelper)
    strict private
      DatabaseInstance: IDatabaseInstance;
      Connection: TSQLite3Connection;
      Transaction: TSQLTransaction;
      SqlQuery: TSQLQuery;
    public
      constructor Create(PDatabaseInstance: TDatabaseInstance);
      procedure RunDmlCUD(PQueryText: String);
      function RunDml(PQueryText: String): String;
  end;

  TLookUpValueDao = class(TInterfacedObject, ILookUpValueDao)
    strict private
      DaoSelectHelper: IDaoSelectHelper;
      DaoDmlCudHelper: IDaoDMLCudHelper;
      TableName, QueryText: String;
      function PerformRead(PQueryText: String): TLookUpValueDtoArray;
    public
      constructor Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper; PTableName: String);
      function Read(PId: Integer): TLookUpValueDtoArray;
      function ReadAll(SortColumn: String): TLookUpValueDtoArray;
      function Insert(PId: Integer; PName, PDescription: String): String;
      function Update(PId: Integer; PName, PDescription: String): String;
      function Delete(PId: Integer): String;
  end;

  TVersionsDao = class(TInterfacedObject, IVersionsDao)
    strict private
      DaoSelectHelper: IDaoSelectHelper;
      DaoDmlCudHelper: IDaoDMLCudHelper;
      TableName, QueryText: String;
      function PerformRead(PQueryText: String): TVersionDtoArray;
    public
      constructor Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper; PTableName: String);
      function Read(PId: Integer): TVersionDtoArray;
      function ReadAll(PSortColumn: String): TVersionDtoArray;
      function Insert(PId, PMajor, PMinor, PMicro: Integer; PDate: String): String;
  end;

  THouseSystemsDao = class(TInterfacedObject, IHouseSystemsDao)
    strict private
      DaoSelectHelper: IDaoSelectHelper;
      DaoDmlCudHelper: IDaoDMLCudHelper;
      QueryText: String;
      function PerformRead(PQueryText: String): THouseSystemDtoArray;
    public
      constructor Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
      function Read(PId: Integer): THouseSystemDtoArray;
      function ReadAll(PSortColumn: String): THouseSystemDtoArray;
      function Insert(PId: Integer; PName, PDescription: String; PNrOfHouses: Integer;
                      PCounterClockWise, PQuadrantSystem, PCuspIsStart: Boolean): String;
      function Update(PId: Integer; PName, PDescription: String; PNrOfHouses: Integer;
                      PCounterClockWise, PQuadrantSystem, PCuspIsStart: Boolean): String;
      function Delete(PId: Integer): String;
  end;

  TAyanamshasDao = class(TInterfacedObject, IAyanamshasDao)
    strict private
      DaoSelectHelper: IDaoSelectHelper;
      DaoDmlCudHelper: IDaoDMLCudHelper;
      QueryText: String;
      function PerformRead(PQueryText: String): TAyanamshaDtoArray;
    public
      constructor Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
      function Read(PId: Integer): TAyanamshaDtoArray;
      function ReadAll(PSortColumn: String): TAyanamshaDtoArray;
      function Insert(PId: Integer; PName, PDescription: String; POffset2000: Double): String;
      function Update(PId: Integer; PName, PDescription: String; POffset2000: Double): String;
      function Delete(PId: Integer): String;
    end;

  TBodiesDao = class(TInterfacedObject, IBodiesDao)
    strict private
      DaoSelectHelper: IDaoSelectHelper;
      DaoDmlCudHelper: IDaoDMLCudHelper;
      QueryText: String;
      function PerformRead(PQueryText: String): TBodyDtoArray;
    public
      constructor Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
      function Read(PId: Integer): TBodyDtoArray;
      function ReadAll(PSortColumn: String): TBodyDtoArray;
      function Insert(PId: Integer; PName: String; PBodyCategory: Integer): String;
      function Update(PId: Integer; PName: String; PBodyCategory: Integer): String;
      function Delete(PId: Integer): String;
    end;

  TAspectsDao = class(TInterfacedObject, IAspectsDao)
    strict private
      DaoSelectHelper: IDaoSelectHelper;
      DaoDmlCudHelper: IDaoDMLCudHelper;
      QueryText: String;
      function PerformRead(PQueryText: String): TAspectDtoArray;
    public
      constructor Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
      function Read(PId: Integer): TAspectDtoArray;
      function ReadAll(PSortColumn: String): TAspectDtoArray;
      function Insert(PId: Integer; PName: String; PAngle: Double; PAspectCategory: Integer): String;
      function Update(PId: Integer; PName: String; PAngle: Double; PAspectCategory: Integer): String;
      function Delete(PId: Integer): String;
    end;

  TConfigurationsDao = class(TInterfacedObject, IConfigurationsDao)
    strict private
      DaoSelectHelper: IDaoSelectHelper;
      DaoDmlCudHelper: IDaoDMLCudHelper;
      QueryText: String;
      function PerformRead(PQueryText: String): TConfigurationDtoArray;
    public
      constructor Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
      function Read(PId: Integer): TConfigurationDtoArray;
      function ReadAll(PSortColumn: String): TConfigurationDtoArray;
      function Insert(PId: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
          PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer):String;
      function Update(PId: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
          PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer): String;
      function Delete(PId: Integer): String;
  end;

  TGlyphsBodiesDao = class(TInterfacedObject, IGlyphsBodiesDao)
    strict private
      DaoSelectHelper: IDaoSelectHelper;
      DaoDmlCudHelper: IDaoDMLCudHelper;
      QueryText: String;
      function PerformRead(PQueryText: String): TGlyphsBodiesDtoArray;
    public
      constructor Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
      function Read(PBodyId: Integer): TGlyphsBodiesDtoArray;
      function ReadAll(PSortColumn: String): TGlyphsBodiesDtoArray;
      function Insert(PBodyId: Integer; PGlyph: String): String;
    end;


  TGlyphsConfigurationDao = class(TInterfacedObject, IGlyphsConfigurationDao)
    strict private
      DaoSelectHelper: IDaoSelectHelper;
      DaoDmlCudHelper: IDaoDMLCudHelper;
      QueryText: String;
      function PerformRead(PQueryText: String): TGlyphsConfigurationDtoArray;
    public
      constructor Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
      function Read(PConfigId, PItemId: Integer; PCategory: String): TGlyphsConfigurationDtoArray;
      function ReadAll(PSortColumn: String; PCategory: String): TGlyphsConfigurationDtoArray;
      function Insert(PConfigId, PItemId: Integer; PCategory, PGlyph: String): String;
    end;

implementation

uses
  XChartsDomainImpl;

const
  EnigmaDatabase = 'enigma.db';

  { TDatabaseInstance }
  function TDatabaseInstance.GetConnection: TSQLite3Connection;
    begin
      Connection:= TSQLite3Connection.Create(Nil);
      Connection.DatabaseName:= EnigmaDatabase;
      Result:= Connection;
    end;

  procedure TDatabaseInstance.ReleaseConnection;
    begin
      Connection.Close;
      Connection.Free;
    end;

  { TDaoDMLCudHelper }

  constructor TDaoDMLCudHelper.Create(PDatabaseInstance: TDatabaseInstance);
    begin
      DatabaseInstance:= PDatabaseInstance;
    end;

  procedure TDaoDMLCudHelper.RunDmlCUD(PQueryText: String);
    begin
      try
        Connection:= DatabaseInstance.Connection;
        Connection.Open;
        Transaction:= TSQLTransaction.Create(Connection);
        Transaction.DataBase:= Connection;
        Connection.Transaction:= Transaction;
        Transaction.Active:= true;
        SqlQuery:= TSQLQuery.Create(nil);
        SqlQuery.DataBase:= Connection;
        SqlQuery.Transaction:= Transaction;
        SqlQuery.SQL.Clear;
        SqlQuery.SQL.Text:= PQueryText;
        SqlQuery.ExecSQL;
        Transaction.Commit;
        Transaction.Free;
        DatabaseInstance.ReleaseConnection;
      except
        on E: Exception do begin
          Connection.Close;
          Transaction.Free;
          Connection.Free;
          { TODO : R_0.6 log error condition and abort program. }
          //text to log('Error while performing the following DML : ' + QueryText
          //                                + ' . Orignal exception : ' + E.Message);
        end;
      end;
    end;


  function TDaoDMLCudHelper.RunDml(PQueryText: String): String;
    var
      QueryText: String;
      ResultText: String = 'OK';
    begin
      QueryText:= PQueryText;
      try
        Connection:= DatabaseInstance.Connection;
        Connection.Open;
        Transaction:= TSQLTransaction.Create(Connection);
        Transaction.DataBase:= Connection;
        Connection.Transaction:= Transaction;
        Transaction.Active:= true;
        SqlQuery:= TSQLQuery.Create(nil);
        SqlQuery.DataBase:= Connection;
        SqlQuery.Transaction:= Transaction;
        SqlQuery.SQL.Clear;
        SqlQuery.SQL.Text:= PQueryText;
        SqlQuery.ExecSQL;
        Transaction.Commit;
        Transaction.Free;
        DatabaseInstance.ReleaseConnection;
      except
        on E: Exception do begin
          Connection.Close;
          Transaction.Free;
          Connection.Free;
          ResultText:= 'Error while performing: ' + QueryText + ' . Orignal exception : ' + E.Message;
        end;
      end;
      Result:= ResultText;
    end;

  { TDaoSelectHelper }

  constructor TDaoSelectHelper.Create(PDatabaseInstance: TDatabaseInstance);
    begin
      DatabaseInstance := PDatabaseInstance;
    end;


  function TDaoSelectHelper.InitializeSelect(PQueryText: String): TSQLQuery;
    begin
      Connection:= DatabaseInstance.Connection;
      SQLTransaction:= TSQLTransaction.Create(Connection);
      Connection.Transaction := SQLTransaction;
      SqlQuery:= TSQLQuery.Create(nil);
      SqlQuery.DataBase:= Connection;
      Connection.Connected:= True;
      SqlQuery.SQL.Clear;
      SqlQuery.SQL.Text:= PQueryText;
      Result:= SqlQuery;
    end;

  procedure TDaoSelectHelper.Release;
    begin
      DatabaseInstance.ReleaseConnection;
    end;

  { TLookupValueDao }

  Constructor TLookUpValueDao.Create(PDaoSelectHelper: IDaoSelectHelper;
                                     PDaoDmlCudHelper: IDaoDMLCudHelper; PTableName: String);
    begin
      TableName:= PTableName;
      DaoSelectHelper:= PDaoSelectHelper;
      DaoDmlCudHelper:= PDaoDmlCudHelper;
    end;

  function TLookUpValueDao.PerformRead(PQueryText: String): TLookUpValueDtoArray;
    var
     SqlQuery: TSQLQuery;
     ResultArray: TLookupValueDtoArray;
     ResultSize: Integer = 0;
     LookUpValueDto: TLookUpValueDto;
    begin
      try
        SqlQuery:= DaoSelectHelper.InitializeSelect(PQueryText);
        SqlQuery.Open;
        while not SqlQuery.eof do begin
          LookUpValueDto := TLookUpValueDto.Create(SqlQuery.FieldByName('Id').AsInteger,
                                                   SqlQuery.FieldByName('Name').AsString,
                                                    SqlQuery.FieldByName('Description').AsString);
          SetLength(ResultArray, ResultSize + 1);
          ResultArray[ResultSize]:= LookUpValueDto;
          Inc(ResultSize);
          SqlQuery.Next;
        end;
      except
        on E: Exception do begin SetLength(ResultArray, 0); end;
      end;
      DaoSelectHelper.Release;
      Result:= ResultArray;
    end;

  function TLookupValueDao.Read(PId: Integer): TLookUpValueDtoArray;
   begin
     QueryText:= 'select id, name, description from ' + TableName + ' where id = ' + Pid.ToString + ';';
     Result:= PerformRead(QueryText);
    end;

  function TLookupValueDao.ReadAll(SortColumn: String): TLookUpValueDtoArray;
    begin
      QueryText:= 'select id, name, description from ' + TableName + ' order by ' + SortColumn + ';';
      Result:= PerformRead(QueryText);
    end;

  function TLookupValueDao.Insert(PId: Integer; PName, PDescription: String): String;
    begin
      QueryText:= 'insert into ' + TableName + '(id, name, description) values (' +
          PId.ToString + ', "' + PName + '", "' +  PDescription + '");';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  function TLookupValueDao.Update(PId: Integer; PName, PDescription: String): String;
    begin
      QueryText:= 'update ' + TableName + ' set name = "' + PName + '", Description = "'+ PDescription +
                  '" where id = '+ Pid.ToString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  function TLookupValueDao.Delete(PId: Integer): String;
    begin
      QueryText:=  'delete from ' + TableName + ' where id = ' + PId.toString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  { TVersionsDao }
  constructor TVersionsDao.Create(PDaoSelectHelper: IDaoSelectHelper;
                                  PDaoDmlCudHelper: IDaoDMLCudHelper; PTableName: String);
    begin
      TableName:= PTableName;
      DaoSelectHelper:= PDaoSelectHelper;
      DaoDmlCudHelper:= PDaoDmlCudHelper;
    end;

  function TVersionsDao.PerformRead(PQueryText: String): TVersionDtoArray;
    var
      SqlQuery: TSQLQuery;
      ResultArray: TVersionDtoArray;
      ResultSize: Integer = 0;
      VersionDto: TVersionDto;
    begin
      try
        SqlQuery:= DaoSelectHelper.InitializeSelect(PQueryText);
        SqlQuery.Open;
        while not SqlQuery.eof do begin
          VersionDto := TVersionDto.Create(SqlQuery.FieldByName('Id').AsInteger,
                                           SqlQuery.FieldByName('Major').AsInteger,
                                           SqlQuery.FieldByName('Minor').AsInteger,
                                           SqlQuery.FieldByName('Micro').AsInteger,
                                           SqlQuery.FieldByName('Date').AsString);
          SetLength(ResultArray, ResultSize + 1);
          ResultArray[ResultSize]:= VersionDto;
          Inc(ResultSize);
          SqlQuery.Next;
        end;
      except
        on E: Exception do begin SetLength(ResultArray, 0); end;
      end;
      DaoSelectHelper.Release;
      Result:= ResultArray;
    end;

  function TVersionsDao.Read(PId: Integer): TVersionDtoArray;
    begin
      QueryText:= 'select id, major, minor, micro, date from ' + TableName + ' where id = ' + Pid.ToString + ';';
      Result:= PerformRead(QueryText);
    end;

  function TVersionsDao.ReadAll(PSortColumn: String): TVersionDtoArray;
    begin
      QueryText:= 'select id, major, minor, micro, date from ' + TableName + ' order by ' + PSortColumn + ';';
      Result:= PerformRead(QueryText);
    end;


  function TVersionsDao.Insert(PId, PMajor, PMinor, PMicro: Integer; PDate: String): String;
    begin
      QueryText:= 'insert into ' + TableName + '(id, major, minor, micro, date) values (' + PId.ToString + ','''
          + PMajor.ToString + ''',''' +  PMinor.ToString + ''',''' + PMicro.ToString  + ''',''' + PDate + ''');';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  { THouseSystemsDao }

  constructor THouseSystemsDao.Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
    begin
      DaoSelectHelper:= PDaoSelectHelper;
      DaoDmlCudHelper:= PDaoDmlCudHelper;
    end;

  function THouseSystemsDao.PerformRead(PQueryText: String): THouseSystemDtoArray;
  var
    SqlQuery: TSQLQuery;
    ResultArray: THouseSystemDtoArray;
    HouseSystemDto: THouseSystemDto;
    ResultSize: Integer = 0;
  begin
    try
      SqlQuery:= DaoSelectHelper.InitializeSelect(PQueryText);
      SqlQuery.Open;
      while not SqlQuery.eof do begin
        HouseSystemDto := THouseSystemDto.Create(SqlQuery.FieldByName('Id').AsInteger,
                                                 SqlQuery.FieldByName('Name').AsString,
                                                 SqlQuery.FieldByName('Description').AsString,
                                                 SqlQuery.FieldByName('NrOfHouses').AsInteger,
                                                 SqlQuery.FieldByName('CounterClockWise').AsInteger,
                                                 SqlQuery.FieldByName('QuadrantSystem').AsInteger,
                                                 SqlQuery.FieldByName('CuspIsStart').AsInteger);
        SetLength(ResultArray, ResultSize + 1);
        ResultArray[ResultSize]:= HouseSystemDto;
        Inc(ResultSize);
        SqlQuery.Next;
      end;
    except
      on E: Exception do begin SetLength(ResultArray, 0); end;
    end;
    DaoSelectHelper.Release;
    Result:= ResultArray;
  end;

  function THouseSystemsDao.Read(PId: Integer): THouseSystemDtoArray;
    begin
     QueryText:= 'select id, name, description, nrofhouses, counterclockwise, quadrantsystem, cuspisstart ' +
                 'from HouseSystems where id = ' + Pid.ToString + ';';
     Result:= PerformRead(QueryText);
    end;

  function THouseSystemsDao.ReadAll(PSortColumn: String): THouseSystemDtoArray;
    begin
     QueryText:= 'select id, name, description, nrofhouses, counterclockwise, quadrantsystem, cuspisstart ' +
                 'from HouseSystems order by ' + PSortColumn + ';';
     Result:= PerformRead(QueryText);
    end;

  function THouseSystemsDao.Insert(PId: Integer; PName, PDescription: String; PNrOfHouses: Integer;
                  PCounterClockWise, PQuadrantSystem, PCuspIsStart: Boolean): String;
    var
      CounterClockWise, QuadrantSystem, CuspIsStart: Integer;
    begin
      if (PCounterClockWise) then CounterClockWise:= 1 else CounterClockWise:= 0;
      if (PQuadrantSystem) then QuadrantSystem:= 1 else QuadrantSystem:= 0;
      if (PCuspIsStart) then CuspIsStart:= 1 else CuspIsStart:= 0;
      QueryText:= 'insert into HouseSystems (id, name, description, nrofhouses, counterclockwise, quadrantsystem, cuspisstart ) '+
                  'values (' + PId.ToString + ',''' + PName + ''',''' +  PDescription + ''',''' + PNrOfHouses.ToString + ''',''' +
                  CounterClockWise.toString + ''',''' +  QuadrantSystem.ToString + ''',''' +  CuspIsStart.toString + ''');';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  function THouseSystemsDao.Update(PId: Integer; PName, PDescription: String; PNrOfHouses: Integer;
                  PCounterClockWise, PQuadrantSystem, PCuspIsStart: Boolean): String;
    var
      CounterClockWise, QuadrantSystem, CuspIsStart: Integer;
    begin
      if (PCounterClockWise) then CounterClockWise:= 1 else CounterClockWise:= 0;
      if (PQuadrantSystem) then QuadrantSystem:= 1 else QuadrantSystem:= 0;
      if (PCuspIsStart) then CuspIsStart:= 1 else CuspIsStart:= 0;
      QueryText:= 'update HouseSystems set name = "' + PName + '", description = "'+ PDescription + '", ' +
                  'nrofhouses = "' + PNrOfHouses.toString + '", counterclockwise = "' + CounterClockWise.toString +
                  '", quadrantsystem = "'  + QuadrantSystem.toString + '", cuspisstart = "' + CuspIsStart.toString +
                  '" where id = '+ Pid.ToString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;


  function THouseSystemsDao.Delete(PId: Integer): String;
    begin
      QueryText:=  'delete from HouseSystems where id = ' + PId.toString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;


  { TAyanamshasDao }

  constructor TAyanamshasDao.Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
    begin
      DaoSelectHelper:= PDaoSelectHelper;
      DaoDmlCudHelper:= PDaoDmlCudHelper;
    end;

  function TAyanamshasDao.PerformRead(PQueryText: String): TAyanamshaDtoArray;
    var
      SqlQuery: TSQLQuery;
      ResultArray: TAyanamshaDtoArray;
      AyanamshaDto: TAyanamshaDto;
      ResultSize: Integer = 0;
    begin
      try
        SqlQuery:= DaoSelectHelper.InitializeSelect(PQueryText);
        SqlQuery.Open;
        while not SqlQuery.eof do begin
          AyanamshaDto:= TAyanamshaDto.Create(SqlQuery.FieldByName('Id').AsInteger,
                                              SqlQuery.FieldByName('Name').AsString,
                                              SqlQuery.FieldByName('Description').AsString,
                                              SqlQuery.FieldByName('Offset2000').AsFloat);
          SetLength(ResultArray, ResultSize + 1);
          ResultArray[ResultSize]:= AyanamshaDto;
          Inc(ResultSize);
          SqlQuery.Next;
        end;
      except
        on E: Exception do begin SetLength(ResultArray, 0); end;
      end;
      DaoSelectHelper.Release;
      Result:= ResultArray;
    end;

  function TAyanamshasDao.Read(PId: Integer): TAyanamshaDtoArray;
    begin
      QueryText:= 'select id, name, description, offset2000 ' +
                  'from Ayanamshas where id = ' + Pid.ToString + ';';
      Result:= PerformRead(QueryText);
    end;

  function TAyanamshasDao.ReadAll(PSortColumn: String): TAyanamshaDtoArray;
    begin
      QueryText:= 'select id, name, description, offset2000 ' +
                  'from Ayanamshas order by ' + PSortColumn + ';';
      Result:= PerformRead(QueryText);
    end;

  function TAyanamshasDao.Insert(PId: Integer; PName, PDescription: String; POffset2000: Double): String;
    var
      Offset2000Text: String;
    begin
      Offset2000Text:= StringReplace(POffset2000.ToString, ',','.',[]);
      QueryText:= 'insert into Ayanamshas (id, name, description, offset2000) '+
                  'values('+ PId.ToString + ',''' + PName + ''',''' +  PDescription + ''',''' + Offset2000Text + ''');';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  function TAyanamshasDao.Update(PId: Integer; PName, PDescription: String; POffset2000: Double): String;
    var
      Offset2000Text: String;
    begin
      Offset2000Text:= StringReplace(POffset2000.ToString, ',','.',[]);
      QueryText:= 'update Ayanamshas set name = "' + PName + '", Description = "'+ PDescription +
                  '", Offset2000 = "' + Offset2000Text +
                  '" where id = '+ Pid.ToString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  function TAyanamshasDao.Delete(PId: Integer): String;
    begin
      QueryText:=  'delete from Ayanamshas where id = ' + PId.toString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;


  { TBodiesDao }
  constructor TBodiesDao.Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
    begin
      DaoSelectHelper:= PDaoSelectHelper;
      DaoDmlCudHelper:= PDaoDmlCudHelper;
    end;

  function TBodiesDao.PerformRead(PQueryText: String): TBodyDtoArray;
    var
      SqlQuery: TSQLQuery;
      ResultArray: TBodyDtoArray;
      BodyDto: TBodyDto;
      ResultSize: Integer = 0;
    begin
      try
        SqlQuery:= DaoSelectHelper.InitializeSelect(PQueryText);
        SqlQuery.Open;
        while not SqlQuery.eof do begin
          BodyDto:= TBodyDto.Create(SqlQuery.FieldByName('Id').AsInteger,
                                    SqlQuery.FieldByName('Name').AsString,
                                    SqlQuery.FieldByName('BodyCategory').AsInteger);
          SetLength(ResultArray, ResultSize + 1);
          ResultArray[ResultSize]:= BodyDto;
          Inc(ResultSize);
          SqlQuery.Next;
        end;
      except
        on E: Exception do begin SetLength(ResultArray, 0); end;
      end;
      DaoSelectHelper.Release;
      Result:= ResultArray;
    end;

  function TBodiesDao.Read(PId: Integer): TBodyDtoArray;
    begin
      QueryText:= 'select id, name, bodycategory ' +
                  'from bodies where id = ' + Pid.ToString + ';';
      Result:= PerformRead(QueryText);
    end;

  function TBodiesDao.ReadAll(PSortColumn: String): TBodyDtoArray;
      begin
      QueryText:= 'select id, name, bodycategory ' +
                  'from bodies order by ' + PSortColumn + ';';
      Result:= PerformRead(QueryText);
    end;

  function TBodiesDao.Insert(PId: Integer; PName: String; PBodyCategory: Integer): String;
    begin
      QueryText:= 'insert into bodies (id, name, bodycategory) '+
                  'values('+ PId.ToString + ',''' + PName + ''',''' + PBodyCategory.toString  + ''');';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  function TBodiesDao.Update(PId: Integer; PName: String; PBodyCategory: Integer): String;
    begin
      QueryText:= 'update bodies set name = "' + PName + '", BodyCategory = "'+ PBodyCategory.toString +
                  '" where id = '+ Pid.ToString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  function TBodiesDao.Delete(PId: Integer): String;
    begin
      QueryText:=  'delete from bodies where id = ' + PId.toString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  { TAspectsDao }
  constructor TAspectsDao.Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
    begin
      DaoSelectHelper:= PDaoSelectHelper;
      DaoDmlCudHelper:= PDaoDmlCudHelper;
    end;

  function TAspectsDao.PerformRead(PQueryText: String): TAspectDtoArray;
    var
      SqlQuery: TSQLQuery;
      ResultArray: TAspectDtoArray;
      AspectDto: TAspectDto;
      ResultSize: Integer = 0;
    begin
      try
        SqlQuery:= DaoSelectHelper.InitializeSelect(PQueryText);
        SqlQuery.Open;
        while not SqlQuery.eof do begin
          AspectDto:= TAspectDto.Create(SqlQuery.FieldByName('Id').AsInteger,
                                        SqlQuery.FieldByName('Name').AsString,
                                        SqlQuery.FieldByName('Angle').AsFloat,
                                        SqlQuery.FieldByName('AspectCategory').AsInteger);
          SetLength(ResultArray, ResultSize + 1);
          ResultArray[ResultSize]:= AspectDto;
          Inc(ResultSize);
          SqlQuery.Next;
        end;
      except
        on E: Exception do begin SetLength(ResultArray, 0); end;
      end;
      DaoSelectHelper.Release;
      Result:= ResultArray;
    end;

  function TAspectsDao.Read(PId: Integer): TAspectDtoArray;
    begin
      QueryText:= 'select id, name, angle, aspectcategory ' +
                  'from aspects where id = ' + Pid.ToString + ';';
      Result:= PerformRead(QueryText);
    end;

  function TAspectsDao.ReadAll(PSortColumn: String): TAspectDtoArray;
    begin
       QueryText:= 'select id, name, angle, aspectcategory ' +
                   'from aspects order by ' + PSortColumn + ';';
      Result:= PerformRead(QueryText);
    end;

  function TAspectsDao.Insert(PId: Integer; PName: String; PAngle: Double; PAspectCategory: Integer): String;
    var
      AngleText: String;
    begin
      AngleText:= StringReplace(PAngle.ToString, ',','.',[]);
      QueryText:= 'insert into aspects (id, name, angle, aspectcategory) '+
                  'values('+ PId.ToString + ',''' + PName + ''',''' + AngleText + ''',''' +
                  PAspectCategory.toString  + ''');';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  function TAspectsDao.Update(PId: Integer; PName: String; PAngle: Double; PAspectCategory: Integer): String;
    var
      AngleText: String;
    begin
      AngleText:= StringReplace(PAngle.ToString, ',','.',[]);
      QueryText:= 'update aspects set name = "' + PName + '", Angle = "'+ AngleText + '", AspectCategory = "' +
                  PAspectCategory.toString +
                  '" where id = '+ Pid.ToString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  function TAspectsDao.Delete(PId: Integer): String;
    begin
      QueryText:=  'delete from aspects where id = ' + PId.toString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  { TConfigurationsDao }

  constructor TConfigurationsDao.Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
    begin
      DaoSelectHelper:= PDaoSelectHelper;
      DaoDmlCudHelper:= PDaoDmlCudHelper;
    end;

  function TConfigurationsDao.PerformRead(PQueryText: String): TConfigurationDtoArray;
  var
    SqlQuery: TSQLQuery;
    ResultArray: TConfigurationDtoArray;
    ConfigurationDto: TConfigurationDto;
    ResultSize: Integer = 0;
  begin
    try
      SqlQuery:= DaoSelectHelper.InitializeSelect(PQueryText);
      SqlQuery.Open;
      while not SqlQuery.eof do begin
        ConfigurationDto:= TConfigurationDto.Create(SqlQuery.FieldByName('Id').AsInteger,
                                                    SqlQuery.FieldByName('Name').AsString,
                                                    SqlQuery.FieldByName('Description').AsString,
                                                    SqlQuery.FieldByName('BaseOrbAspects').AsFloat,
                                                    SqlQuery.FieldByName('BaseOrbMidpoints').AsFloat,
                                                    SqlQuery.FieldByName('CoordinateSystem').AsInteger,
                                                    SqlQuery.FieldByName('Ayanamsha').AsInteger,
                                                    SqlQuery.FieldByName('HouseSystem').AsInteger,
                                                    SqlQuery.FieldByName('ObserverPosition').AsInteger,
                                                    SqlQuery.FieldByName('ProjectionType').AsInteger);
        SetLength(ResultArray, ResultSize + 1);
        ResultArray[ResultSize]:= ConfigurationDto;
        Inc(ResultSize);
        SqlQuery.Next;
      end;
    except
      on E: Exception do begin SetLength(ResultArray, 0); end;
    end;
    DaoSelectHelper.Release;
    Result:= ResultArray;
  end;

  function TConfigurationsDao.Read(PId: Integer): TConfigurationDtoArray;
    begin
      QueryText:= 'select id, name, description, baseorbaspects, baseorbmidpoints, coordinatesystem, ayanamsha, '+
                  'housesystem, observerposition, projectiontype ' +
                  'from configurations where id = ' + Pid.ToString + ';';
      Result:= PerformRead(QueryText);
    end;

  function TConfigurationsDao.ReadAll(PSortColumn: String): TConfigurationDtoArray;
    begin
      QueryText:= 'select id, name, description, baseorbaspects, baseorbmidpoints, coordinatesystem, ayanamsha, '+
                  'housesystem, observerposition, projectiontype ' +
                  'from configurations order by ' + PSortColumn + ';';
      Result:= PerformRead(QueryText);
    end;

  function TConfigurationsDao.Insert(PId: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
      PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer): String;
  var
    BaseOrbAspectsText, BaseOrbMidpointsText: String;
  begin
    BaseOrbAspectsText:= StringReplace(PBaseOrbAspects.ToString, ',','.',[]);
    BaseOrbMidpointsText:= StringReplace(PBaseOrbMidpoints.ToString, ',','.',[]);
    QueryText:= 'insert into configurations (id, name, description, baseorbaspects, baseorbmidpoints, coordinatesystem, '+
                ' ayanamsha, housesystem, observerposition, projectiontype) '+
                'values('+ PId.ToString + ',''' + PName + ''',''' + PDescription + ''',''' + BaseOrbAspectsText +
                ''',''' + BaseOrbMidpointsText + ''',''' + PCoordinateSystem.ToString + ''',''' + PAyanamsha.ToString  +
                ''',''' + PHouseSystem.ToString + ''',''' + PObserverPosition.ToString + ''',''' +
                PProjectionType.ToString + ''');';
    Result:= DaoDmlCudHelper.RunDml(QueryText);

    end;
  function TConfigurationsDao.Update(PId: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
      PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer): String;
    var
      BaseOrbAspectsText, BaseOrbMidpointsText: String;
    begin
      BaseOrbAspectsText:= StringReplace(PBaseOrbAspects.ToString, ',','.',[]);
      BaseOrbMidpointsText:= StringReplace(PBaseOrbMidpoints.ToString, ',','.',[]);
      QueryText:= 'update configurations set id = "' + PId.ToString + '", name = "' + PName + '", description= "' +
                  PDescription + '", baseorbaspects= "' + BaseOrbAspectsText + '", baseorbmidpoints= "' +
                  BaseOrbMidpointsText + '", coordinatesystem= "' + PCoordinateSystem.ToString + '", '+
                 ' ayanamsha= "' + PAyanamsha.ToString + '",housesystem= "' + PHouseSystem.ToString +
                 '", observerposition= "' + PObserverPosition.ToString + '", projectiontype= "' + PProjectionType.ToString + '" '+
                 'where id = "'+ Pid.ToString + '";';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  function TConfigurationsDao.Delete(PId: Integer): String;
    begin
      QueryText:=  'delete from configurations where id = ' + PId.toString + ';';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

  { TGlyphsBodiesDao }
  constructor TGlyphsBodiesDao.Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
    begin
      DaoSelectHelper:= PDaoSelectHelper;
      DaoDmlCudHelper:= PDaoDmlCudHelper;
    end;

  function TGlyphsBodiesDao.PerformRead(PQueryText: String): TGlyphsBodiesDtoArray;
    var
      SqlQuery: TSQLQuery;
      ResultArray: TGlyphsBodiesDtoArray;
      GlyphsBodiesDto: TGlyphsBodiesDto;
      ResultSize: Integer = 0;
    begin
      try
        SqlQuery:= DaoSelectHelper.InitializeSelect(PQueryText);
        SqlQuery.Open;
        while not SqlQuery.eof do begin
          GlyphsBodiesDto:= GlyphsBodiesDto.Create(SqlQuery.FieldByName('BodyId').AsInteger,
                                                   SqlQuery.FieldByName('Glyph').AsString);
          SetLength(ResultArray, ResultSize + 1);
          ResultArray[ResultSize]:= GlyphsBodiesDto;
          Inc(ResultSize);
          SqlQuery.Next;
        end;
      except
        on E: Exception do begin SetLength(ResultArray, 0); end;
      end;
      DaoSelectHelper.Release;
      Result:= ResultArray;
    end;

  function TGlyphsBodiesDao.Read(PBodyId: Integer): TGlyphsBodiesDtoArray;
    begin
      QueryText:= 'select bodyid, glyph ' +
                  'from GlyphsBodies where bodyid = ' + PBodyId.ToString + ';';
      Result:= PerformRead(QueryText);
    end;

  function TGlyphsBodiesDao.ReadAll(PSortColumn: String): TGlyphsBodiesDtoArray;
    begin
      QueryText:= 'select bodyid, glyph ' +
                  'from GlyphsBodies order by ' + PSortColumn + ';';
      Result:= PerformRead(QueryText);
    end;

  function TGlyphsBodiesDao.Insert(PBodyId: Integer; PGlyph: String): String;
    begin
      QueryText:= 'insert into GlyphsBodies (bodyId, glyph) '+
                  'values('+ PBodyId.ToString + ',''' + PGlyph  + ''');';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;


  { TGlyphsConfigurationDao }

  constructor TGlyphsConfigurationDao.Create(PDaoSelectHelper: IDaoSelectHelper; PDaoDmlCudHelper: IDaoDMLCudHelper);
    begin
      DaoSelectHelper:= PDaoSelectHelper;
      DaoDmlCudHelper:= PDaoDmlCudHelper;
    end;

  function TGlyphsConfigurationDao.PerformRead(PQueryText: String): TGlyphsConfigurationDtoArray;
    var
      SqlQuery: TSQLQuery;
      ResultArray: TGlyphsConfigurationDtoArray;
      GlyphsConfigurationDto: TGlyphsConfigurationDto;
      ResultSize: Integer = 0;
    begin
      try
        SqlQuery:= DaoSelectHelper.InitializeSelect(PQueryText);
        SqlQuery.Open;
        while not SqlQuery.eof do begin
          GlyphsConfigurationDto:= GlyphsConfigurationDto.Create(SqlQuery.FieldByName('ConfigId').AsInteger,
                                                                 SqlQuery.FieldByName('ItemId').AsInteger,
                                                                 SqlQuery.FieldByName('Category').AsString,
                                                                 SqlQuery.FieldByName('Glyph').AsString);
          SetLength(ResultArray, ResultSize + 1);
          ResultArray[ResultSize]:= GlyphsConfigurationDto;
          Inc(ResultSize);
          SqlQuery.Next;
        end;
      except
        on E: Exception do begin SetLength(ResultArray, 0); end;
      end;
      DaoSelectHelper.Release;
      Result:= ResultArray;

    end;

  function TGlyphsConfigurationDao.Read(PConfigId, PItemId: Integer; PCategory: String): TGlyphsConfigurationDtoArray;
    begin
      QueryText:= 'select configid, itemid, category, glyph ' +
                  'from GlyphsConfiguration where configid = ' + PConfigId.ToString +
                  ' and bodyid = ' + PItemId.ToString +
                  ' and category = ' + PCategory +  ';';
      Result:= PerformRead(QueryText);
    end;

  function TGlyphsConfigurationDao.ReadAll(PSortColumn: String; PCategory: String): TGlyphsConfigurationDtoArray;
    begin
      QueryText:= 'select configid, itemid, category, glyph ' +
                  'from GlyphsConfiguration ' +
                  'where category = ' + PCategory +
                  'order by ' + PSortColumn + ';';
      Result:= PerformRead(QueryText);
    end;

  function TGlyphsConfigurationDao.Insert(PConfigId, PItemId: Integer; PCategory, PGlyph: String): String;
    begin
      QueryText:= 'insert into GlyphsConfiguration (configid, itemId, category, glyph) '+
                  'values('+ PConfigId.toString + ', ' + PItemId.ToString + ',''' + PCategory + ''',''' + PGlyph  + ''');';
      Result:= DaoDmlCudHelper.RunDml(QueryText);
    end;

end.

