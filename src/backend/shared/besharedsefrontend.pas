unit BeSharedSeFrontend;
{< Contains a frontend for the Swiss Ephemeris (SE). }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, BeChartsDomain, XChartsDomain, BeChartsDomainImpl, XSharedDomain, BeSwissDelphi;

const
  DLL_NAME = 'swedll32.dll';

type

  TDoubleArray = Array of Double;

  { Frontend to calculations with the Swiss Ephemeris (SE).
   It is implemented as a singleton as the SE does not support multiple instances.}
   TSeFrontend = class        // singleton based on last example at http://wiki.freepascal.org/Singleton_Pattern
     private
       constructor Init;
       function CreateHousePosition(PJulianDay, PLongitude, PObliquity: Double; PLocation: IValidatedLocation): IHousePositionFull;
     public
       { Static class to prevent using 'Create' as a constructor.
       The constructor is the private method 'Init' and this is how the singleton is implemented. }
       class function Create: TSeFrontend;

       { Calculate Julian Day Number. Always use Universal time for input as DeltaT is automatically taken care off.}
       function JulDay(PDate:IValidatedDate; PTime:IValidatedTime): Double;

       { Checks if a data is valid, takes calendar and leap years into account.}
       function CheckDate(PDay, PMonth, PYear: Integer; PCalendar: String): Boolean;

       { Calculate a celestial object. Uses Julian day for UT, the id for the object (PlanetId) and the combined flags
       for the type of calculation.}
       function CalculateCelestialObject(PJulianDay:Double; PPlanetId:Integer; PFlags:LongInt): ICelestialObjectSimple;

       { Calculate horizontal positions. Uses Julian day for UT, the location and a CelestialObject containing
       ecliptical positions. Altitude is only correctly calculated as true altitude (no apparent altitude). }
       function CalculateAzimuthAndAltitude(PJulianDay:Double; PLocation: IValidatedLocation;
                  PCelObject: ICelestialObjectSimple): IHorizontalPosition;

       { Calcualte full position, including ecliptic, equatorial and horizontal coördinates.}
       function CalculateCelestialObjectFull(PJulianDay:Double; PPlanetId:Integer; PFlags:LongInt;
                  PLocation: IValidatedLocation): ICelestialObjectFull;

       { Calculate mean value of obliquity (Epsilon) }
       function CalculateObliquity(PJulianDay:Double): Double;

       { Convert ecliptical coördinates into equatorial coördinates. The resulting array contains the right ascension
       at index 0 and the declination at index 1, index 2 can be ignored. }
       function Ecliptic2Equatorial(PLong, Plat, PEpsilon: Double): TDoubleArray;

       { Calculate all cusps for a specific housesystem, includes longitude, altitude, right ascension, declination,
       azimuth and altitude. }
       function CalculateHouses(PJulianDay, PObliquity:Double; PLocation: IValidatedLocation;
                                PHouseSYstem: IHouseSystemSpec): IHousesResponse ;
   end;



implementation

uses XChartsDomainImpl;

const
  ZERO_DBL = 0.0;

var
  Singleton: TSeFrontend = nil;
{ TSeFrontend }

constructor TSeFrontend.init;
begin
  inherited Create;
end;

class function TSeFrontend.Create: TseFrontend;
begin
  if Singleton = nil then begin
    Singleton:= TSeFrontend.Init;
    swe_set_ephe_path('.\\se');   // required to use the SE Data and for initialization of the SE.
  end;
  Result:= Singleton;
end;

function TSeFrontend.JulDay(PDate:IValidatedDate; PTime:IValidatedTime): Double;
var
  GregorianFlag: Integer;
  Date: IValidatedDate;
  Time: IValidatedTime;
begin
  Date:=PDate;
  Time:=PTime;
  if LowerCase(PDate.Calendar) = 'g' then GregorianFlag:=1
  else GregorianFlag:=0;
  Result := swe_julday(Date.Year, Date.Month, Date.Day, Time.DecimalTime, GregorianFlag);
end;

function TSeFrontend.CheckDate(PDay, PMonth, PYear: Integer; PCalendar: String): Boolean;
var
  Day, Month, Year, ReturnStatus: Integer;
  GregorianFlag: char;
  JulianDay: Double;
begin
  JulianDay:=0.0;
  Day := PDay;
  Month := PMonth;
  Year := PYear;
  if LowerCase(PCalendar) = 'g' then GregorianFlag:= 'g'
  else GregorianFlag:= 'j';
  ReturnStatus := swe_date_conversion(Year, Month, Day, 0.0, GregorianFlag, JulianDay);
  Result := ReturnStatus = 0;
end;

function TSeFrontend.CalculateCelestialObject(PJulianDay: Double; PPlanetId: Integer;
                         PFlags: LongInt): ICelestialObjectSimple;
var
  Positions: Array[0..5] of Double;
  CalcResult: Longint;
      CelObject: ICelestialObjectSimple;
  ErrorText: Array[0..255] of Char;
begin
  CalcResult := swe_calc_ut(PJulianDay, PPlanetId, PFlags, Positions[0], ErrorText);  // todo check Calcresult, log errors
  CelObject:= TCelestialObjectSimple.Create(Positions);
  Result:= CelObject;
end;

function TSeFrontend.CalculateAzimuthAndAltitude(PJulianDay:Double; PLocation: IValidatedLocation;
  PCelObject: ICelestialObjectSimple): IHorizontalPosition;
var
  GeoPos, EclipticCoordinates, AzAlt: Array[0..2] of Double;
  AtPress, AtTemp: Double;
  Flags: Longint;
begin
  GeoPos[0]:= PLocation.Longitude;
  GeoPos[1]:= PLocation.Latitude;
  GeoPos[2]:= ZERO_DBL;    // height above sea, ignored for real altitude
  EclipticCoordinates[0]:= PCelObject.MainPos;
  EclipticCoordinates[1]:= PCelObject.DeviationPos;
  EclipticCoordinates[2]:= PCelObject.DistancePos;
  AtPress:= ZERO_DBL;   // Atmospheric pressure in mbar, ignored for real altitude
  AtTemp:= ZERO_DBL;    // Atmospheric temperature in degrees Celsius, ignored for real altitude
  Flags:= SE_ECL2HOR;
  swe_azalt(PJulianDay, Flags, GeoPos[0], AtPress, AtTemp, EclipticCoordinates[0], AzAlt[0]);
  Result:= THorizontalPosition.Create(AzAlt[1], AzAlt[0]);
end;

function TSeFrontend.CalculateCelestialObjectFull(PJulianDay:Double; PPlanetId:Integer; PFlags:LongInt;
  PLocation: IValidatedLocation): ICelestialObjectFull;
var
  CelestialObjectEcliptic, CelestialObjectEquatorial, CelestialObjectHorizontal: ICelestialObjectSimple;
  HorizontalPosition: IHorizontalPosition;
  HorizontalArray: Array[0..5] of Double;
  ActualFlags: LongInt;
  i: Integer;
begin
  ActualFlags:= PFlags;
  CelestialObjectEcliptic:= CalculateCelestialObject(PJulianDay, PPlanetId, ActualFlags);
  ActualFlags := PFlags or SEFLG_EQUATORIAL;
  CelestialObjectEquatorial:= CalculateCelestialObject(PJulianDay, PPlanetId, ActualFlags);
  HorizontalPosition:= CalculateAzimuthAndAltitude(PJulianDay, PLocation, CelestialObjectEcliptic);
  HorizontalArray[0]:= HorizontalPosition.Azimuth;
  HorizontalArray[1]:= HorizontalPosition.Altitude;
  for i:= 2 to 5 do HorizontalArray[i]:= ZERO_DBL;
  CelestialObjectHorizontal:= TCelestialObjectSimple.Create(HorizontalArray);
  Result:= TCelestialObjectFull.Create(PPlanetId, CelestialObjectEcliptic, CelestialObjectEquatorial,
                                       CelestialObjectHorizontal);
end;

function TSeFrontend.CalculateObliquity(PJulianDay:Double):Double;
var
  Positions: Array[0..6] of Double;
  CalcResult: Longint;
  ErrorText: Array[0..255] of Char;
begin
  CalcResult := swe_calc_ut(PJulianDay, SE_ECL_NUT, 0, Positions[0], ErrorText);  // todo check Calcresult, log errors
  Result:= Positions[1];
end;


function TSeFrontend.Ecliptic2Equatorial(PLong, Plat, PEpsilon: Double): TDoubleArray;
var
  EclipticValues, EquatorialValues: TDoubleArray;
begin
  SetLength(EclipticValues, 3);
  SetLength(EquatorialValues, 3);
  EclipticValues[0]:= PLong;
  EclipticValues[1]:= PLat;
  EclipticValues[2]:= 1.0;   // distance can be ignored, the value 1.0 is just a placeholder.
  swe_cotrans(EclipticValues[0], EquatorialValues[0], PEpsilon);
  Result:= EquatorialValues;
end;

function TSeFrontend.CreateHousePosition(PJulianDay, PLongitude, PObliquity: Double;
                                         PLocation: IValidatedLocation): IHousePositionFull;
var
    EquatorialCoordinates: TDoubleArray;
    RightAscension, Declination, Azimuth, Altitude: Double;
    SimplePos: TCelestialObjectSimple;
    AzAlt: IHorizontalPosition;
    TempValues: Array of Double;
    i: Integer;
begin
    SetLength(TempValues, 6);
    EquatorialCoordinates:= Ecliptic2Equatorial(PLongitude, 0.0, PObliquity);
    RightAscension:= EquatorialCoordinates[0];
    Declination:= EquatorialCoordinates[1];
    TempValues[0]:= PLongitude;
    TempValues[1]:= 0.0;
    for i:= 2 to 5 do TempValues[i]:= 0.0;
    SimplePos:= TCelestialObjectSimple.Create(TempValues);
    AzAlt:= CalculateAzimuthAndAltitude(PJulianDay, PLocation, SimplePos);
    Azimuth:= AzAlt.GetAzimuth;
    Altitude:= AzAlt.GetAltitude;
    Result:= THousePositionFull.Create(PLongitude, RightAscension, Declination, Azimuth, Altitude);
end;

function TSeFrontend.CalculateHouses(PJulianDay, PObliquity:Double; PLocation: IValidatedLocation;
                                     PHouseSystem: IHouseSystemSpec): IHousesResponse ;
var
  AllCusps: THousePositionFullArray;
  HouseValues: Array of Double;
  AscMcValues: Array[0..10] of Double;
  Mc, Asc: IHousePositionFull;
  AllHousePositions: THousePositionFullArray;
  ReturnValue: Integer;
  SeChar: AnsiChar;
  i: Integer;
begin
  SetLength(HouseValues, PHouseSystem.GetNumberOfHouses + 1);
  SetLength(AllHousePositions, PHouseSystem.GetNumberOfHouses + 1);
  SetLength(AllCusps, PHouseSystem.GetNumberOfHouses + 1);
  SeChar := AnsiChar(PHouseSystem.GetSeId[1]);
  ReturnValue:= swe_houses(PJulianDay, PLocation.Latitude, PLocation.Longitude, SeChar,
                           HouseValues[0], AscMcValues[0]);
  for i:= 1 to Length(HouseValues)-1 do begin
    AllCusps[i] := CreateHousePosition(PJulianDay, HouseValues[i], PObliquity, PLocation);
  end;
  Asc:= CreateHousePosition(PJulianDay, AscMcValues[0], PObliquity, PLocation);
  Mc:= CreateHousePosition(PJulianDay, AscMcValues[1], PObliquity, PLocation);
  Result:= THousesResponse.Create(Mc, Asc, AllCusps, PHouseSystem);
end;

end.

