{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit XSharedDomainImpl;
{< Implementations for interfaces in SharedXchgDomain }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XSharedDomain;

type

{ Default implementation for IValidatedLocation }
TValidatedLocation = class(TInterfacedObject, IValidatedLocation)
  strict private
    LName: String;
    LLongitude, LLatitude: Double;
    function GetName: String;
    function GetLongitude: Double;
    function GetLatitude: Double;
  public
    constructor Create(PName: String; PLongitude, PLatitude: Double);
end;

{ Default implementation for IValidatedDate }
TValidatedDate = class(TInterfacedObject, IValidatedDate)
  strict private
    LYear, LMonth, LDay: Integer;
    LCalendar: String;
    LValid: Boolean;
    function GetYear: Integer;
    function GetMonth: Integer;
    function GetDay: Integer;
    function GetCalendar: String;
    function IsValid: Boolean;
  public
    constructor Create(PYear, PMonth, PDay: Integer; PCalendar: String; PValid: Boolean);
end;

{ Default implementation for IValidatedTime }
TValidatedTime = class(TInterfacedObject, IValidatedTime)
  strict private
    LDecimalTime: Double;
    LValid: Boolean;
    function GetDecimalTime: Double;
    function IsValid: Boolean;
  public
    constructor Create(PDecimalTime: Double; PValid: Boolean);
end;

{ Default implementation for IValidatedDouble }
TValidatedDouble = class(TInterfacedObject, IValidatedDouble)
  strict private
    LValue: Double;
    LValid: Boolean;
    function GetValue:Double;
    function IsValid:Boolean;
  public
    constructor Create(PValue: Double; PValid: Boolean);
end;

{ Default implementation for IReferenceFrame }
TReferenceFrame = class(TInterfacedObject, IReferenceFrame)
  strict private
    Equatorial, Topocentric, Heliocentric, Sidereal: Boolean;
    LFlags: LongInt;
    procedure DefineFlags;
    function GetFlags: LongInt;
  public
    constructor Create(PEquatorial, PTopocentric, PHeliocentric, PSidereal: Boolean);
end;

implementation

uses BeSwissDelphi;

{ TValidatedLocation }
constructor TValidatedLocation.Create(PName: String; PLongitude, PLatitude: Double);
begin
  LName:= PName;
  LLongitude:= PLongitude;
  LLatitude:= PLatitude;
end;

function TValidatedLocation.GetName: String;
begin
  Result:= LName;
end;

function TValidatedLocation.GetLongitude: Double;
begin
  Result:= LLongitude;
end;

function TValidatedLocation.GetLatitude: Double;
begin
  Result:= LLatitude;
end;


{ TValidatedDate }

constructor TValidatedDate.Create(PYear, PMonth, PDay: Integer; PCalendar: String; PValid: Boolean);
begin
  LYear:=PYear;
  LMonth:=PMonth;
  LDay:=PDay;
  LCalendar:= PCalendar;
  LValid:=PValid;
end;

function TValidatedDate.GetYear:Integer;
begin
  Result:=LYear;
end;

function TValidatedDate.GetMonth:Integer;
begin
  Result:=LMonth;
end;

function TValidatedDate.GetDay:Integer;
begin
  Result:=LDay;
end;

function TValidatedDate.GetCalendar:String;
begin
  Result:=LCalendar;
end;

function TValidatedDate.IsValid: Boolean;
begin
  Result:=LValid;
end;


{ TValidatedTime }

constructor TValidatedTime.Create(PDecimalTime: Double; PValid: Boolean);
begin
  LDecimalTime:= PDecimalTime;
  LValid:=PValid;
end;

function TValidatedTime.GetDecimalTime: Double;
begin
  Result:=LDecimalTime;
end;

function TValidatedTime.IsValid:Boolean;
begin
  Result:=LValid;
end;


{ TValidatedDouble }

constructor TValidatedDouble.Create(PValue: Double; PValid: Boolean);
begin
  LValue:=PValue;
  LValid:=PValid;
end;

function TValidatedDouble.GetValue: Double;
begin
  Result:=LValue;
end;

function TValidatedDouble.IsValid: Boolean;
begin
  Result:=LValid;
end;


{ TReferenceFrame }

constructor TReferenceFrame.Create(PEquatorial, PTopocentric, PHeliocentric, PSidereal: Boolean);
  begin
    Equatorial:= PEquatorial;
    Topocentric:= PTopocentric;
    Heliocentric:= PHeliocentric;
    Sidereal:= PSidereal;
    DefineFlags;
  end;

  procedure TReferenceFrame.DefineFlags;
  begin
    LFlags:= 0;
    if (Equatorial) then LFlags:= LFlags or SEFLG_EQUATORIAL;
    if (Topocentric) then LFlags:= LFlags or SEFLG_TOPOCTR;
    if (Heliocentric) then LFlags:= LFlags or SEFLG_HELCTR;
    if (Sidereal) then LFlags:= LFlags or SEFLG_SIDEREAL;
  end;

  function TReferenceFrame.GetFlags: LongInt;
  begin
    Result:= LFlags;
  end;


end.

