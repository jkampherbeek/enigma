unit XSharedEndpointsFactories;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XSharedEndpoints;

type

  { Factory for ValidatedLatitudeEndpoint. @br
  - Created: 2018-11-25 @br
  - Last update: 2018-11-25 }
  FactoryValidatedLatitudeEndpoint = class
    public
      class function GetInstance: IValidatedLatitudeEndpoint;
    end;

  { Factory for ValidatedLongitudeEndpoint. @br
  - Created: 2018-11-25 @br
  - Last update: 2018-11-25 }
  FactoryValidatedLongitudeEndpoint = class
    public
      class function GetInstance: IValidatedLongitudeEndpoint;
    end;

  { Factory for ValidatedDateEndpoint. @br
  - Created: 2018-11-25 @br
  - Last update: 2018-11-25 }
  FactoryValidatedDateEndpoint = class
    public
      class function GetInstance: IValidatedDateEndpoint;
    end;

  { Factory for ValidatedTimeEndpoint. @br
  - Created: 2018-11-25 @br
  - Last update: 2018-11-25 }
  FactoryValidatedTimeEndpoint = class
    public
      class function GetInstance: IValidatedTimeEndpoint;
    end;

implementation

  uses XSharedEndpointsImpl, XSharedHandlersImpl, BeSharedSeFrontend;

  { FactoryValidatedLatitudeEndpoint. }
  class function FactoryValidatedLatitudeEndpoint.GetInstance: IValidatedLatitudeEndpoint;
  begin
    Result:= TValidatedLatitudeEndpoint.Create(TLatitudeValidationHandler.Create);
  end;

  { FactoryValidatedLongitudeEndpoint. }
  class function FactoryValidatedLongitudeEndpoint.GetInstance: IValidatedLongitudeEndpoint;
  begin
    Result:= TValidatedLongitudeEndpoint.Create(TLongitudeValidationHandler.Create);
  end;

  { FactoryValidatedDateEndpoint. }
  class function FactoryValidatedDateEndpoint.GetInstance: IValidatedDateEndpoint;
  begin
    Result:= TValidatedDateEndpoint.Create(TDateValidationHandler.Create(TSeFrontend.Create));
  end;

  { FactoryValidatedTimeEndpoint. }
  class function FactoryValidatedTimeEndpoint.GetInstance: IValidatedTimeEndpoint;
  begin
    Result:= TValidatedTimeEndpoint.Create(TTimeValidationHandler.Create);
  end;

end.

