unit XSharedHandlersFactories;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XSharedHandlers;

type

  { Factory for SexagesimalValidationHandler. @br
  - Created: 2018-11-23 @br
  - Last update: 2018-11-23 }
  FactorySexagesimalValidationHandler = class
    public
      class function GetInstance(PMinDH, PMaxDH: Integer): ISexagesimalValidationHandler;
  end;


implementation

uses XSharedHandlersImpl;

  { FactorySexagesimalValidationHandler }
  class function FactorySexagesimalValidationHandler.GetInstance(PMinDH, PMaxDH: Integer): ISexagesimalValidationHandler;
    begin
      Result:= TSexagesimalValidationHandler.Create(PMinDH, PMaxDH);
    end;

end.

