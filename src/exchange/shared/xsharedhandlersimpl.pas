{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit XSharedHandlersImpl;
{< Implementations for interfaces in XSharedHandlers. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XSharedDomain, XSharedHandlers, BeSharedSeFrontend;

const
  CALENDAR_JULIAN = 'j';
  CALENDAR_GREGORIAN = 'g';
  MIN_MINUTE_SECOND = 0;
  MAX_MINUTE_SECOND = 59;
  MIN_HOUR = 0;
  MAX_HOUR = 23;
  MINUTES_PER_HOUR = 60;
  SECONDS_PER_HOUR = 3600;
  MIN_LONGITUDE = -180;
  MAX_LONGITUDE = 180;
  MIN_LATITUDE_DEGREES = -89;
  MAX_LATITUDE_DEGREES = 89;
  LONGITUDE_WEST = 'W';
  LATITUDE_SOUTH = 'S';
  ZERO = 0.0;

type
{ Implementation of ISexagesimalValidationHandler }
TSexagesimalValidationHandler = class(TInterfacedObject, ISexagesimalValidationHandler)
  strict private
    LMinDH, LMaxDH: Integer;
    LValid: Boolean;
    LValue: Double;
    procedure Validate(DegreesHours, Minutes, Seconds: Integer);
  public
    constructor Create(PMinDH, PMaxDH: Integer);
    function CalculateValue(DHText, MinutesText, SecondsText: String; NegativeSign: Boolean): IValidatedDouble;
end;

{ Default implementation of IDateValidationHandler. }
TDateValidationHandler = class(TInterfacedObject, IDateValidationHandler)
  private
    Valid: Boolean;
    Calendar: String;
    LSeFrontend: TSeFrontend;
  public
    constructor Create(PSeFrontend: TSeFrontend);
    function CalculateValue(PYear, PMonth, PDay, PCalendar: String): IValidatedDate;
end;

{ Default implementation of ITimeValidationHandler. }
TTimeValidationHandler = class(TInterfacedObject, ITimeValidationHandler)
  public
    function CalculateValue(PHour, PMinute, PSecond: String): IValidatedTime;
end;

{ Default implementation of ILongitudeValidationHandler. }
TLongitudeValidationHandler = class(TInterfacedObject, ILongitudeValidationHandler)
  strict private
    SexagesimalHandler: ISexagesimalValidationHandler;
  public
    constructor Create;
    function CalculateValue(Degrees, Minutes, Seconds, Direction: String): IValidatedDouble;
end;

{ Default implementation of ILatitudeValidationHandler. }
TLatitudeValidationHandler = class(TInterfacedObject, ILatitudeValidationHandler)
  strict private
    SexagesimalHandler: ISexagesimalValidationHandler;
  public
    constructor Create;
    function CalculateValue(Degrees, Minutes, Seconds, Direction: String): IValidatedDouble;
end;

implementation

uses XSharedDomainImpl, XSharedHandlersFactories;

{ TSexagesimalValidationHandler }

constructor TSexagesimalValidationHandler.Create(PMinDH, PMaxDH: Integer);
begin
 LMinDH := PMinDH;
 LMaxDH := PMaxDH;
end;

procedure TSexagesimalValidationHandler.Validate(DegreesHours, Minutes, Seconds: Integer);
begin
 if (DegreesHours < LMinDH)
 or (DegreesHours > LMaxDH)
 or not(Minutes in [MIN_MINUTE_SECOND..MAX_MINUTE_SECOND])
 or not(Seconds in [MIN_MINUTE_SECOND..MAX_MINUTE_SECOND]) then begin
   LValid := false;
   LValue := ZERO;
 end;
end;

function TSexagesimalValidationHandler.CalculateValue(DHText, MinutesText, SecondsText: String;
                               NegativeSign: Boolean): IValidatedDouble;
 var
   DHValue, MinutesValue, SecondsValue, ErrorCode: Integer;
 begin
   LValue := ZERO;
   LValid := true;
   Val(DHText, DHValue, ErrorCode);
   if ErrorCode <> 0 then LValid:= false;
   Val(Minutestext, MinutesValue, ErrorCode);
   if ErrorCode <> 0 then LValid:= false;
   Val(SecondsText, SecondsValue, ErrorCode);
   if ErrorCode <> 0 then LValid:= false;
   DHValue:= Abs(DHValue);
   MinutesValue:= Abs(MinutesValue);
   SecondsValue:= Abs(SecondsValue);
   if LValid then Validate(DHValue, MinutesValue, SecondsValue);
   if LValid then
   begin
     LValue := DHValue + MinutesValue/MINUTES_PER_HOUR + SecondsValue/SECONDS_PER_HOUR;
     if NegativeSign then LValue := -LValue;
   end
   else LValue := ZERO;
   Result := TValidatedDouble.Create(LValue, LValid);
 end;


{ TDateValidationHandler }

constructor TDateValidationHandler.Create(PSeFrontend: TSeFrontend);
begin
  LSeFrontend := PSeFrontend;
end;


function TDateValidationHandler.CalculateValue(PYear, PMonth, PDay, PCalendar: String):IValidatedDate;
var
  Year, Month, Day: Integer;
begin
   Valid := true;
   Val(PYear, Year, ErrorCode);
   if ErrorCode <> 0 then Valid:= false;
   Val(PMonth, Month, ErrorCode);
   if ErrorCode <> 0 then Valid:= false;
   Val(PDay, Day, ErrorCode);
   if ErrorCode <> 0 then Valid:= false;
   self.Calendar:=PCalendar;
   if (PCalendar <> CALENDAR_JULIAN) and (PCalendar <> CALENDAR_GREGORIAN) then Valid:=false;
   if Valid and not(LSeFrontend.CheckDate(Day, Month, Year, Calendar)) then Valid := false;
   if not Valid then begin
     Year:=0; Month:=1; Day:=1;
   end;
   Result:=TValidatedDate.Create( Year, Month, Day, Calendar, Valid);
end;

{ TTimeValidationHandler }
function TTimeValidationHandler.CalculateValue(PHour, PMinute, PSecond: String): IValidatedTime;
var
  Valid: Boolean;
  Hour, Minute, Second: Integer;
begin
  Valid := true;
  Val(PHour, Hour, ErrorCode);
  if ErrorCode <> 0 then Valid:= false;
  Val(PMinute, Minute, ErrorCode);
  if ErrorCode <> 0 then Valid:= false;
  Val(PSecond, Second, ErrorCode);
  if ErrorCode <> 0 then Valid:= false;
  if Valid then begin
    if (Hour < MIN_HOUR) or (Hour > MAX_HOUR)
    or (Minute < MIN_MINUTE_SECOND) or (Minute > MAX_MINUTE_SECOND)
    or (Second < MIN_MINUTE_SECOND) or (Second > MAX_MINUTE_SECOND) then Valid:=False;
  end;
  if not Valid then begin
    Hour:=0; Minute:=0; Second:=0;
  end;
  Result:=TValidatedTime.Create(Hour + Minute/MINUTES_PER_HOUR + Second/SECONDS_PER_HOUR, Valid);
end;



{ TLongitudeValidationHandler }

constructor TLongitudeValidationHandler.Create;
begin
  SexagesimalHandler := FactorySexagesimalValidationHandler.GetInstance(MIN_LONGITUDE, MAX_LONGITUDE);
end;

function TLongitudeValidationHandler.CalculateValue(Degrees, Minutes, Seconds, Direction: String): IValidatedDouble;
var
  NegativeSign: Boolean;
  SexagValue: IValidatedDouble;
begin
  if Direction = LONGITUDE_WEST then NegativeSign := true
  else NegativeSign := false;
  SexagValue:= SexagesimalHandler.CalculateValue(Degrees, Minutes, Seconds, NegativeSign);
  Result:= TValidatedDouble.Create(SexagValue.Value,SexagValue.Valid);
end;


{ TLatitudeValidationHandler }
constructor TLatitudeValidationHandler.Create;
begin
  SexagesimalHandler := FactorySexagesimalValidationHandler.GetInstance(MIN_LATITUDE_DEGREES, MAX_LATITUDE_DEGREES);
end;

function TLatitudeValidationHandler.CalculateValue(Degrees, Minutes, Seconds, Direction: String): IValidatedDouble;
var
  NegativeSign: Boolean;
  SexagValue: IValidatedDouble;
begin
  if Direction = LATITUDE_SOUTH then NegativeSign := true
  else NegativeSign:= false;
  SexagValue:= SexagesimalHandler.CalculateValue(Degrees, Minutes, Seconds, NegativeSign);
  Result:= TValidatedDouble.Create(SexagValue.Value,SexagValue.Valid);
end;


end.

