unit XSharedEndpointsImpl;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XSharedEndpoints, XSharedHandlers, XSharedDomain;

type

{ Default implementation of IValidatedLatitudeEndpoint. }
TValidatedLatitudeEndpoint = class(TInterfacedObject, IValidatedLatitudeEndpoint)
  strict private
    LHandler: ILatitudeValidationHandler;
  public
    constructor Create(PHandler: ILatitudeValidationHandler);
    function HandleInput(PDegrees, PMinutes, PSeconds, PDirection: String): IValidatedDouble;
  end;

{ Default implementation of IValidatedLongitudeEndpoint. }
TValidatedLongitudeEndpoint = class(TInterfacedObject, IValidatedLongitudeEndpoint)
  strict private
    LHandler: ILongitudeValidationHandler;
  public
    constructor Create(PHandler: ILongitudeValidationHandler);
    function HandleInput(PDegrees, PMinutes, PSeconds, PDirection: String): IValidatedDouble;
  end;

{ Default implementation of IValidatedDateEndpoint. }
TValidatedDateEndpoint = class(TInterfacedObject, IValidatedDateEndpoint)
  strict private
    LHandler: IDateValidationHandler;
  public
    constructor Create(PHandler: IDateValidationHandler);
    function HandleInput(PYear, PMonth, PDay, PCalendar: String): IValidatedDate;
  end;

{ Default implementation of IVakudatedTimeEndpoint. }
TValidatedTimeEndpoint = class(TInterfacedObject, IValidatedTimeEndpoint)
  strict private
    LHandler: ITimeValidationHandler;
  public
    constructor Create(PHandler: ITimeValidationHandler);
    function HandleInput(PHour, PMinute, PSecond: String): IValidatedTime;
end;



implementation

{ TValidatedLatitudeEndpoint }

constructor TValidatedLatitudeEndpoint.Create(PHandler: ILatitudeValidationHandler);
begin
  LHandler:= PHandler;
end;

function TValidatedLatitudeEndpoint.HandleInput(PDegrees, PMinutes, PSeconds, PDirection: String): IValidatedDouble;
begin
  Result:= LHandler.CalculateValue(PDegrees, PMinutes, PSeconds, PDirection);
end;

{ TValidatedLongitudeEndpoint }

constructor TValidatedLongitudeEndpoint.Create(PHandler: ILongitudeValidationHandler);
begin
  LHandler:= PHandler;
end;

function TValidatedLongitudeEndpoint.HandleInput(PDegrees, PMinutes, PSeconds, PDirection: String): IValidatedDouble;
begin
  Result:= LHandler.CalculateValue(PDegrees, PMinutes, PSeconds, PDirection);
end;

{ TValidatedDateEndpoint }

constructor TValidatedDateEndpoint.Create(PHandler: IDateValidationHandler);
begin
  LHandler:= PHandler;
end;

function TValidatedDateEndpoint.HandleInput(PYear, PMonth, PDay, PCalendar: String): IValidatedDate;
begin
  Result:= LHandler.CalculateValue(PYear, PMonth, PDay, PCalendar);
end;

{ TValidatedTimeEndpoint }

constructor TValidatedTimeEndpoint.Create(PHandler: ITimeValidationHandler);
begin
  LHandler:=PHandler;
end;

function TValidatedTimeEndpoint.HandleInput(PHour, PMinute, PSecond: String): IValidatedTime;
begin
  Result:= LHandler.CalculateValue(PHour, PMinute, PSecond);
end;

end.

