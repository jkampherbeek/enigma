{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit XSharedEndpoints;
{< Endpoints that are shared between different parts of Enigma. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XSharedDomain;

type

{ Endpoint for geographic latitude. Validates and converts strings with sexagesimal values to a double. @br
- Created: 2018 @br
- Last update: 2018 }
IValidatedLatitudeEndpoint = interface ['{BFEFBE3C-75A4-4E20-882C-4BD6345982A6}']
  function HandleInput(PDegrees, PMinutes, PSeconds, PDirection: String): IValidatedDouble;
end;

{ Endpoint for geographic longitude. Validates and converts input to a double.@br
- Created: 2018 @br
- Last update: 2018 }
IValidatedLongitudeEndpoint = interface ['{39B76C28-A780-44A3-8A9D-33936F0E3561}']
  function HandleInput(PDegrees, PMinutes, PSeconds, PDirection: String): IValidatedDouble;
end;

{ Endpoint for clock time. Converts strings to a TTime object.@br
- Created: 2018 @br
- Last update: 2018 }
IValidatedTimeEndpoint = interface ['{DC360C68-B58D-498B-B0D2-9768BBF8F628}']
  function HandleInput(PHour, PMinute, PSecond: String): IValidatedTime;
end;

{ Endpoint for calendar date. Converts strings to a TDate object.@br
- Created: 2018 @br
- Last update: 2018 }
IValidatedDateEndpoint = interface ['{D6BC2834-970E-4AF2-BEE1-6B5FD2EABCB5}']
  function HandleInput(PYear, PMonth, PDay, PCalendar: String): IValidatedDate;
end;

implementation

end.

