{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit XChartsEndpoints;
{< Endpoints for requests that are specific for Charts. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XChartsDomain;

type

{ Endpoint for data for housesystems.@br
  This is standing data, no insert, update or delete actions from the UI.@br
- Factory: TFactoryHouseSystemDataEndpoint @br
- Fake: FakeHouseSystemDataEndpoint @br
- Created: 2019-01-11 @br
- Last update: 2019-01-12 }
IHouseSystemDataEndpoint = Interface ['{0F1CFD0C-ED11-4B86-888A-E8998850B4A1}']
  function GetSystem(PId: Integer): IHouseSystemDto;
  function GetAllSystems: THouseSystemDtoArray;
  property AllSystems: THouseSystemDtoArray read GetAllSystems;
end;

{ Endpoint for data for ayanamshas.@br
  This is standing data, no insert, update or delete actions from the UI.@br
- Factory: TFactoryAyanamshaDataEndpoint @br
- Fake: FakeAyanamshaDataEndpoint @br
- Created: 2019-01-20 @br
- Last update: 2019-01-20 }
IAyanamshaDataEndpoint = Interface ['{DFB27BBE-1BD3-41A8-84F9-FB8B8B828E9A}']
  function GetAyanamsha(PId: Integer): IAyanamshaDto;
  function GetAllAyanamshas: TAyanamshaDtoArray;
  property AllAyanamshas: TAyanamshaDtoArray read GetAllAyanamshas;
end;

{ Endpoint for coordinate systems,@br
  This is standing data, no insert, update or delete actions from the UI.@br
- Factory: TFactoryCoordinateSystemDataEndpoint @br
- Fake: FakeCoordinateSystemDataEndpoint @br
- Created: 2019-01-21 @br
- Last update: 2019-01-23 }
ICoordinateSystemDataEndpoint = Interface ['{E386DE32-F66C-4127-81CD-86F156276C82}']
  function GetName(PId: Integer): ILookUpValueDto;
  function GetAllItems():TLookUpValueDtoArray;
  property AllItems: TLookUpValueDtoArray read GetAllItems;
end;

{ Generic endpoint for lookup values,@br
  This is standing data, no insert, update or delete actions from the UI.@br
- Factory: .... @br
- Fake: ....@br
- Created: 2019-01-23 @br
- Last update: 2019-01-23 }
ILookupValueDataEndpoint = Interface ['{FE62D43E-3428-47FF-B423-8F7CB3DFA5BD}']
  function GetName(PId: Integer): ILookUpValueDto;
  function GetAllItems():TLookUpValueDtoArray;
  property AllItems: TLookUpValueDtoArray read GetAllItems;
end;

{ Endpoint for actual glyphs for a configuration. @br
- Factory: .... @br
- Fake: ....@br
- Created: 2019-02-13 @br
- Last update: 2019-02-13 }
IGlyphsConfigurationEndpoint = Interface ['{71A43C9E-C109-47CE-BFC8-44EC7DDABD34}']
  function GetItemsForConfigAndCategory(PConfig: INteger; PCategory: String):TGlyphsConfigurationDtoArray;
end;

{ Endpoint for the calculation of a full chart. @br
- Created: 2018 @br
- Last update: 2019-03-31 }
IFullChartEndpoint = Interface ['{364E11DF-3C2B-4860-9C3E-6D8EEC984B21}']
  function HandleRequest(PFullChartRequest: IFullChartRequest): IFullChartResponse;
end;




implementation






end.

