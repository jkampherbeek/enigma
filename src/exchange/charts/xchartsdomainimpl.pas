{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit XChartsDomainImpl;
{< Implementations for XChartsDomain. }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XChartsDomain, XSharedDomain;

type

  THousePositionFullArray = Array of IHousePositionFull;

  TLookUpValueDto = class(TInterfacedObject, ILookUpValueDto)
   strict private
     LId: Integer;
     LName, LDescription: String;
   public
     constructor Create(PId: Integer; PName, PDescription: String);
     function GetId: Integer;
     function GetName: String;
     function GetDescription: String;
   end;

   TVersionDto = class(TInterfacedObject, IVersionDto)
     strict private
       Id, Major, Minor, Micro: Integer;
       Date: String;
     public
       constructor Create(PId: Integer; PMajor, PMinor, PMicro: Integer; PDate: String);
       function GetId: Integer;
       function GetMajor: Integer;
       function GetMinor: Integer;
       function GetMicro: Integer;
       function GetDate: String;
   end;

   THouseSystemDto = class(TInterfacedObject, IHouseSystemDto)
     strict private
       Id, NrOfHouses: Integer;
       Name, Description: String;
       CounterClockWise, QuadrantSystem, CuspIsStart: Boolean;
     public
       constructor Create(PId: Integer; PName, PDescription: String;
                   PNrOfHouses, PCounterClockWise, PQuadrantSystem, PCuspIsStart: Integer);
       function GetId: Integer;
       function GetName: String;
       function GetDescription: String;
       function GetNrOfHouses: Integer;
       function IsCounterClockWise: Boolean;
       function IsQuadrantSystem: Boolean;
       function IsCuspIsStart: Boolean;
   end;

   TAyanamshaDto = class(TInterfacedObject, IAyanamshaDto)
     strict private
       Id: Integer;
       Name, Description: String;
       Offset2000: Double;
     public
       constructor Create(Pid: Integer; PName, PDescription: String; POffset2000: Double);
       function GetId: Integer;
       function GetName: String;
       function GetDescription: String;
       function GetOffset2000: Double;
   end;

   TBodyDto = class(TInterfacedObject, IBodyDto)
     strict private
       Id, BodyCategory: Integer;
       Name: String;
     public
       constructor Create(PId: Integer; PName: String; PBodyCategory: Integer);
       function GetId: Integer;
       function GetName: String;
       function GetBodyCategory: Integer;
   end;

   TAspectDto = class(TInterfacedObject, IAspectDto)
     strict private
       Id, AspectCategory: Integer;
       Name: String;
       Angle: Double;
     public
       constructor Create(PId: Integer; PName: String; PAngle: Double; PAspectCategory: Integer);
       function GetId: Integer;
       function GetName: String;
       function GetAngle: Double;
       function GetAspectCategory: Integer;
   end;

   TConfigurationDto = class(TInterfacedObject, IConfigurationDto)
     strict private
       Id, CoordinateSystem, Ayanamsha, HouseSystem, ObserverPosition, ProjectionType: Integer;
       Name, Description: String;
       BaseOrbAspects, BaseOrbMidpoints: Double;
     public
       constructor Create(Pid: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
                          PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPOsition, PProjectionType: Integer);
       function GetId: Integer;
       function GetName: String;
       function GetDescription: String;
       function GetBaseOrbAspects: Double;
       function GetBaseOrbMidpoints: Double;
       function GetCoordinateSystem: Integer;
       function GetAyanamsha: Integer;
       function GetHouseSystem: integer;
       function GetObserverPosition: Integer;
       function GetProjectionType: Integer;
   end;

   TGlyphsBodiesDto = class(TInterfacedObject, IGlyphsBodiesDto)
     strict private
       BodyId: Integer;
       Glyph: String;
     public
       Constructor Create(PBodyId: Integer; PGlyph: String);
       function GetId: LongInt;
       function GetGlyph: String;
     end;

   TGlyphsConfigurationDto = class(TInterfacedObject, IGlyphsConfigurationDto)
     strict private
       ConfigId, ItemId: LongInt;
       Category, Glyph: String;
     public
       Constructor Create(PConfigId, PItemId: LongInt; PCategory, PGlyph: String);
       function GetConfigId: LongInt;
       function GetItemId: Integer;
       function GetCategory: String;
       function GetGlyph: String;
   end;

  THouseSystemSpec = class(TInterfacedObject, IHouseSystemSpec)
    strict private
      LName, LSeId, LDescription: String;
      LId, LNumberOfHouses: Integer;
      LQuadrantSystem, LCuspIsStart: Boolean;
    public
      constructor Create(PName, PSeID, PDescription: String; PId, PNumberOfHouses: Integer;
                         PQuadrantSystem, PCuspIsStart: Boolean);
      function GetName: String;
      function GetSeId: String;
      function GetDescription: String;
      function GetId: Integer;
      function GetNumberOfHouses: Integer;
      function IsQuadrantSystem: Boolean;
      function IsCuspIsStart: Boolean;
  end;

  THousePositionFull = class(TInterfacedObject, IHousePositionFull)
    strict private
      LLongitude, LRightAscension, LDeclination, LAzimuth, LAltitude: Double;
      function GetLongitude: Double;
      function GetRightAscension: Double;
      function GetDeclination: Double;
      function GetAzimuth: Double;
      function GetAltitude: Double;
    public
      constructor Create(PLongitude, PRightAscension, PDeclination, PAzimuth, PAltitude: Double);
  end;

  TCelestialObjectSimple = class(TInterfacedObject, ICelestialObjectSimple)
    strict private
      LMainPos, LDeviationPos, LDistancePos, LMainSpeed, LDeviationSpeed, LDistanceSpeed: Double;
      function GetMainPos: Double;
      function GetDeviationPos: Double;
      function GetDistancePos: Double;
      function GetMainSpeed: Double;
      function GetDeviationSpeed: Double;
      function GetDistanceSpeed: Double;
    public
      constructor Create(CalculatedValues: Array of Double);
  end;

  TCelestialObjectFull = class(TInterfacedObject, ICelestialObjectFull)
    strict private
      LObjectId: Integer;
      LEclipticalPos, LEquatorialPos, LHorizontalPos: ICelestialObjectSimple;
      function GetObjectId: Integer;
      function GetEclipticalPos: ICelestialObjectSimple;
      function GetEquatorialPos: ICelestialObjectSimple;
      function GetHorizontalPos: ICelestialObjectSimple;
    public
      constructor Create(PObjectId: Integer; PEclipticalPos, PEquatorialPos, PHorizontalPos: ICelestialObjectSimple);
  end;

  THousesResponse = class(TInterfacedObject, IHousesResponse)
    strict private
      LCusps: THousePositionFullArray;
      LMc, LAsc: IHousePositionFull;
      LHouseSystemSpec: IHouseSystemSpec;
      function GetMc: IHousePositionFull;
      function GetAsc: IHousePositionFull;
      function GetCusps: THousePositionFullArray;
      function GetHouseSystemSpec: IHouseSystemSpec;
    public
      constructor Create(PMc, PAsc: IHousePositionFull; PCusps: THousePositionFullArray; PHouseSystemSpec: IHouseSystemSpec);
  end;

  TCalculationSettings = class(TInterfacedObject, ICalculationSettings)
    strict private
      LPosition, LAyanamsha: Integer;
      LObjects: TIntArray;
      LHouseSystemSpec: IHouseSystemSpec;
      LReferenceFrame: IReferenceFrame;
    public
      constructor Create(PPosition, PAyanamsha: Integer;
                         PObjects: TIntArray;
                         PReferenceFrame: IReferenceFrame;
                         PHouseSystemSpec: IHouseSystemSpec);
      function GetPosition: Integer;
      function GetAyanamsha: Integer;
      function GetHouseSystemSpec: IHouseSystemSpec;
      function GetObjects: TIntArray;
      function GetReferenceFrame: IReferenceFrame;
  end;

  TFullChartRequest = class(TInterfacedObject, IFullChartRequest)
    strict private
      LName: String;
      LLocation: IValidatedLocation;
      LDate: IValidatedDate;
      LTime: IValidatedTime;
      LCalculationSettings: ICalculationSettings;
    public
      constructor Create(PName: String; PLocation: IValidatedLocation; PDate: IValidatedDate; PTime: IValidatedTime;
        PCalculationSettings: ICalculationSettings);
      function GetName: String;
      function GetLocation: IValidatedLocation;
      function GetDate: IValidatedDate;
      function GetTime: IValidatedTime;
      function GetCalculationSettings: ICalculationSettings;
  end;

  TFullChartResponse = class(TInterfacedObject, IFullChartResponse)
    strict private
      LName: String;
      LAllObjects: TCelestialObjectFullArray;
      LHouses: IHousesResponse;
      LFullChartRequest: IFullChartRequest;
    public
      constructor Create(PName: String;
                         PAllObjects: TCelestialObjectFullArray;
                         PHouses: IHousesResponse;
                         PFullChartRequest: IFullChartRequest);
      function GetName: String;
      function GetAllObjects: TCelestialObjectFullArray;
      function GetHouses: IHousesResponse;
      function GetFullChartRequest: IFullChartRequest;
  end;


implementation



    { TLookUpValueDto }
    constructor TLookUpValueDto.Create(PId: Integer; PName, PDescription: String);
      begin
        LId:= PId;
        LName:= PName;
        LDescription:= PDescription;
      end;

    function TLookUpValueDto.GetId: Integer;
      begin
        Result:= LId;
      end;

    function TLookUpValueDto.GetName: String;
      begin
        Result:= LName;
      end;

    function TLookUpValueDto.GetDescription: String;
      begin
        Result:= LDescription;
      end;

    { TVersionDto }
    constructor TVersionDto.Create(PId: Integer; PMajor, PMinor, PMicro: Integer; PDate: String);
      begin
        Id:= PId;
        Major:= PMajor;
        Minor:= PMinor;
        Micro:= PMicro;
        Date:= PDate;
      end;

    function TVersionDto.GetId: INteger;
      begin
        Result:= Id;
      end;

    function TVersionDto.GetMajor: Integer;
      begin
        Result:= Major;
      end;

    function TVersionDto.GetMinor: Integer;
      begin
        Result:= Minor;
      end;

    function TVersionDto.GetMicro: Integer;
      begin
        Result:= Micro;
      end;

    function TVersionDto.GetDate: String;
      begin
        Result:= Date;
      end;

    { THouseSystemDto }

    constructor THouseSystemDto.Create(PId: Integer; PName, PDescription: String;
                PNrOfHouses, PCounterClockWise, PQuadrantSystem, PCuspIsStart: Integer);
      begin
        Id:= PId;
        Name:= PName;
        Description:= PDescription;
        NrOfHouses:= PNrOfHouses;
        CounterClockWise:= Not(PCounterClockWise = 0);
        QuadrantSystem:= Not(PQuadrantSystem = 0);
        CuspIsStart:= Not(PCuspIsStart = 0);
      end;

    function THouseSystemDto.GetId: Integer;
      begin
        Result:= Id;
      end;

    function THouseSystemDto.GetName: String;
      begin
        Result:= Name;
      end;

    function THouseSystemDto.GetDescription: String;
      begin
        Result:= Description;
      end;

    function THouseSystemDto.GetNrOfHouses: Integer;
      begin
        Result:= NrOfHouses;
      end;

    function THouseSystemDto.IsCounterClockWise: Boolean;
      begin
        Result:= CounterClockWise;
      end;

    function THouseSystemDto.IsQuadrantSystem: Boolean;
      begin
        Result:= QuadrantSystem;
      end;

    function THouseSystemDto.IsCuspIsStart: Boolean;
      begin
        Result:= CuspIsStart;
      end;


    { TAyanamshaDto }

    constructor TAyanamshaDto.Create(Pid: Integer; PName, PDescription: String; POffset2000: Double);
      begin
        Id:= PId;
        Name:= PName;
        Description:= PDescription;
        Offset2000:= POffset2000;
      end;

    function TAyanamshaDto.GetId: Integer;
      begin
        Result:= Id;
      end;

    function TAyanamshaDto.GetName: String;
      begin
        Result:= Name;
      end;

    function TAyanamshaDto.GetDescription: String;
      begin
        Result:= Description;
      end;

    function TAyanamshaDto.GetOffset2000: Double;
      begin
        Result:= Offset2000;
      end;

    { TBodyDto }

    constructor TBodyDto.Create(PId: Integer; PName: String; PBodyCategory: Integer);
      begin
        Id:= Pid;
        Name:= PName;
        BodyCategory:= PBodyCategory;
      end;

    function TBodyDto.GetId: Integer;
      begin
        Result:= Id;
      end;

    function TBodyDto.GetName: String;
      begin
        Result:= Name;
      end;

    function TBodyDto.GetBodyCategory: Integer;
      begin
        Result:= BodyCategory;
      end;


    { TAspectDto }

    constructor TAspectDto.Create(PId: Integer; PName: String; PAngle: Double; PAspectCategory: Integer);
      begin
        Id:= Pid;
        Name:= PName;
        Angle:= PAngle;
        AspectCategory:= PAspectCategory;
      end;

    function TAspectDto.GetId: Integer;
      begin
        Result:= Id;
      end;

    function TAspectDto.GetName: String;
      begin
        Result:= Name;
      end;

    function TAspectDto.GetAngle: Double;
      begin
        Result:= Angle;
      end;


    function TAspectDto.GetAspectCategory: Integer;
      begin
        Result:= AspectCategory;
      end;

    { TConfigurationDto }
    constructor TConfigurationDto.Create(Pid: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
                       PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer);
      begin
        Id:= PId;
        Name:= PName;
        Description:= PDescription;
        BaseOrbAspects:= PBaseOrbAspects;
        BaseOrbMidpoints:= PBaseOrbMidpoints;
        CoordinateSystem:= PCoordinateSystem;
        Ayanamsha:= PAYanamsha;
        HouseSystem:= PHouseSystem;
        ObserverPosition:= PObserverPosition;
        ProjectionType:= PProjectionType;
      end;

    function TConfigurationDto.GetId: Integer;
      begin
        Result:= Id;
      end;

    function TConfigurationDto.GetName: String;
      begin
        Result:= Name;
      end;

    function TConfigurationDto.GetDescription: String;
      begin
        Result:= Description;
      end;

    function TConfigurationDto.GetBaseOrbAspects: Double;
      begin
        Result:= BaseOrbAspects;
      end;

    function TConfigurationDto.GetBaseOrbMidpoints: Double;
      begin
        Result:= BaseOrbMidpoints;
      end;

    function TConfigurationDto.GetCoordinateSystem: Integer;
      begin
        Result:= CoordinateSystem;
      end;

    function TConfigurationDto.GetAyanamsha: Integer;
      begin
        Result:= Ayanamsha;
      end;

    function TConfigurationDto.GetHouseSystem: integer;
     begin
       Result:= HouseSystem;
     end;

    function TConfigurationDto.GetObserverPosition: Integer;
      begin
        Result:= ObserverPosition;
      end;

    function TConfigurationDto.GetProjectionType: Integer;
      begin
        Result:= ProjectionType;
      end;

    { TGlyphsBodiesDto }
    constructor TGlyphsBodiesDto.Create(PBodyId: Integer; PGlyph: String);
      begin
        BodyId:= PBodyId;
        Glyph:= PGlyph;
      end;

    function TGlyphsBodiesDto.GetId: LongInt;
      begin
        Result:= BodyId;
      end;

    function TGlyphsBodiesDto.GetGlyph: String;
      begin
        Result:= Glyph;
      end;

    { TGlyphsConfigurationDto }
    Constructor TGlyphsConfigurationDto.Create(PConfigId, PItemId: LongInt; PCategory, PGlyph: String);
      begin
        ConfigId:= PConfigId;
        ItemId:= PItemId;
        Category:= PCategory;
        Glyph:= PGlyph;
      end;

    function TGlyphsConfigurationDto.GetConfigId: LongInt;
      begin
        Result:= ConfigId;
      end;

    function TGlyphsConfigurationDto.GetItemId: Integer;
      begin
        Result:= ItemId;
      end;

    function TGlyphsConfigurationDto.GetCategory: String;
      begin
        Result:= category;
      end;

    function TGlyphsConfigurationDto.GetGlyph: String;
      begin
        Result:= Glyph;
      end;



{ THouseSystemSpec }
  constructor THouseSystemSpec.Create(PName, PSeID, PDescription: String; PId, PNumberOFHouses: Integer;
                     PQuadrantSystem, PCuspISStart: Boolean);
    begin
      self.LName:= PName;
      self.LSeId:= PSeID;
      self.LDescription:= PDescription;
      self.LId:= PId;
      self.LNumberOfHouses:= PNumberOFHouses;
      self.LQuadrantSystem:= PQuadrantSystem;
      self.LCuspIsStart:= PCuspISStart;
    end;

  function THouseSystemSpec.GetName: String;
    begin
      Result:= LName;
    end;

  function THouseSystemSpec.GetSeId: String;
    begin
      Result:= LSeId;
    end;

  function THouseSystemSpec.GetDescription: String;
    begin
      Result:= LDescription;
    end;

  function THouseSystemSpec.GetId: Integer;
    begin
      Result:= LId;
    end;

  function THouseSystemSpec.GetNumberOfHouses: Integer;
    begin
      Result:= LNumberOfHouses;
    end;

  function THouseSystemSpec.IsQuadrantSystem: Boolean;
    begin
      Result:= LQuadrantSystem;
    end;

  function THouseSystemSpec.IsCuspIsStart: Boolean;
    begin
      Result:= LCuspIsStart;
    end;


{ THousePositionFull }

  constructor THousePositionFull.Create(PLongitude, PRightAscension, PDeclination, PAzimuth, PAltitude: Double);
    begin
      LLongitude:= PLongitude;
      LRightAscension:= PRightAscension;
      LDeclination:= PDeclination;
      LAzimuth:= PAzimuth;
      LAltitude:= PAltitude;
    end;

  function THousePositionFull.GetLongitude: Double;
    begin
      Result:= LLongitude;
    end;

  function THousePositionFull.GetRightAscension: Double;
    begin
      Result:= LRightAscension;
    end;

  function THousePositionFull.GetDeclination: Double;
    begin
      Result:= LDeclination;
    end;

  function THousePositionFull.GetAzimuth: Double;
    begin
      Result:= LAzimuth;
    end;

  function THousePositionFull.GetAltitude: Double;
    begin
      Result:= LAltitude;
    end;


{ TCelestialObjectSimple }

  constructor TCelestialObjectSimple.Create(CalculatedValues: Array of Double);
    begin
      LMainPos:= CalculatedValues[0];
      LDeviationPos:= CalculatedValues[1];
      LDistancePos:= CalculatedValues[2];
      LMainSpeed:= CalculatedValues[3];
      LDeviationSpeed:= CalculatedValues[4];
      LDistanceSpeed:= CalculatedValues[5];
    end;


  function TCelestialObjectSimple.GetMainPos: Double;
    begin
      Result:= LMainPos;
    end;

  function TCelestialObjectSimple.GetDeviationPos: Double;
    begin
      Result:= LDeviationPos;
    end;

  function TCelestialObjectSimple.GetDistancePos: Double;
    begin
      Result:= LDistancePos;
    end;

  function TCelestialObjectSimple.GetMainSpeed: Double;
    begin
      Result:= LMainSpeed;
    end;

  function TCelestialObjectSimple.GetDeviationSpeed: Double;
    begin
      Result:= LDeviationSpeed;
    end;

  function TCelestialObjectSimple.GetDistanceSpeed: Double;
    begin
      Result:= LDistanceSpeed;
    end;



{ TCelestialObjectFull }

  constructor TCelestialObjectFull.Create(PObjectId: Integer;
                     PEclipticalPos, PEquatorialPos, PHorizontalPos: ICelestialObjectSimple);
    begin
      LObjectId:= PObjectId;
      LEclipticalPos:= PEclipticalPos;
      LEquatorialPos:= PEquatorialPos;
      LHorizontalPos:= PHorizontalPos;
    end;

  function TCelestialObjectFull.GetObjectId: Integer;
    begin
      Result:= LObjectId;
    end;

  function TCelestialObjectFull.GetEclipticalPos: ICelestialObjectSimple;
    begin
      Result:= LEclipticalPos;
    end;

  function TCelestialObjectFull.GetEquatorialPos: ICelestialObjectSimple;
    begin
      Result:= LEquatorialPos;
    end;

  function TCelestialObjectFull.GetHorizontalPOs: ICelestialObjectSimple;
    begin
      Result:= LHorizontalPos;
    end;


{ THouseResponse }
  constructor THousesResponse.Create(PMc, PAsc: IHousePositionFull;
                                     PCusps: THousePositionFullArray;
                                     PHouseSystemSpec: IHouseSystemSpec);
    begin
      LMc:= PMc;
      LAsc:= PAsc;
      LCusps:= PCusps;
      LHouseSystemSpec:= PHouseSystemSpec;
    end;

  function THousesResponse.GetMc: IHousePositionFull;
    begin
      Result:= LMc;
    end;

  function THousesResponse.GetASc: IHousePositionFull;
    begin
      Result:= LAsc;
    end;

  function THousesResponse.GetCusps: THousePositionFullArray;
    begin
      Result:= LCusps;
    end;

  function THousesResponse.GetHouseSystemSpec: IHouseSystemSpec;
    begin
      Result:= LHouseSystemSpec;
    end;



{ TCalculationSettings }
  constructor TCalculationSettings.Create(PPosition, PAyanamsha: Integer;
     PObjects: TIntArray; PReferenceFrame: IReferenceFrame; PHouseSystemSpec: IHouseSystemSpec);
    begin
     LPosition:= PPosition;
     LAyanamsha:= PAyanamsha;
     LHouseSystemSpec:= PHouseSystemSpec;
     LObjects:=PObjects;
     LReferenceFrame:= PReferenceFrame;
    end;

  function TCalculationSettings.GetPosition: Integer;
    begin
      Result:=LPosition;
    end;

  function TCalculationSettings.GetAyanamsha: Integer;
    begin
      Result:=LAyanamsha;
    end;

  function TCalculationSettings.GetHouseSystemSpec: IHouseSystemSpec;
    begin
      Result:= LHouseSystemSpec;
    end;

  function TCalculationSettings.GetObjects: TIntArray;
    begin
      Result:=LObjects;
    end;

  function TCalculationSettings.GetReferenceFrame: IReferenceFrame;
    begin
      Result:= LReferenceFrame;
    end;


 { TFullChartRequest }

   constructor TFullChartRequest.Create(PName: String;
                                        PLocation: IValidatedLocation;
                                        PDate: IValidatedDate;
                                        PTime: IValidatedTime;
                                        PCalculationSettings: ICalculationSettings);
     begin
       LName:= PName;
       LLocation:= PLocation;
       LDate:= PDate;
       LTime:= PTime;
       LCalculationSettings:=PCalculationSettings;
     end;

  function TFullChartRequest.GetName: String;
    begin
      Result := LName;
    end;

  function TFullChartRequest.GetLocation: IValidatedLocation;
    begin
      Result := LLocation;
    end;

  function TFullChartRequest.GetDate: IValidatedDate;
    begin
      Result := LDate;
    end;

  function TFullChartRequest.GetTime: IValidatedTime;
    begin
      Result:= LTime;
    end;

  function TFullChartRequest.GetCalculationSettings: ICalculationSettings;
    begin
      Result:= LCalculationSettings;
    end;


{ TFullChartResponse }
  constructor TFullChartResponse.Create(PName: String; PAllObjects: TCelestialObjectFullArray; PHouses: IHousesResponse;
                                        PFullChartRequest: IFullChartRequest);
    begin
      LName:= PName;
      LAllObjects:= PAllObjects;
      LHouses:= PHouses;
      LFullChartRequest:= PFullChartRequest;
    end;

  function TFullChartResponse.GetName: String;
    begin
      Result:= LName;
    end;

  function TFullChartResponse.GetAllObjects: TCelestialObjectFullArray;
    begin
      Result:= LAllObjects;
    end;

  function TFullChartResponse.GetHouses: IHousesResponse;
    begin
      Result:= LHouses;
    end;

  function TFullChartResponse.GetFullChartRequest: IFullChartRequest;
    begin
      Result:= LFullChartRequest;
    end;




end.

