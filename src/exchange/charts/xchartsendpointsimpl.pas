{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit XChartsEndpointsImpl;
{< Implementations for XChartsEndpoints }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XChartsEndpoints, XChartsDomain, BeChartsHandlers, BeDbDao;

type

  THouseSystemDataEndpoint = class(TInterfacedObject, IHouseSystemDataEndpoint)
    strict private
      Dao: IHouseSystemsDao;
    public
      Constructor Create(PDao: IHouseSystemsDao);
      function GetSystem(PId: Integer): IHouseSystemDto;
      function GetAllSystems: THouseSystemDtoArray;
  end;

  TAyanamshaDataEndpoint = class(TInterfacedObject, IAyanamshaDataEndpoint)
    strict private
      Dao: IAyanamshasDao;
    public
      Constructor Create(PDao: IAyanamshasDao);
      function GetAyanamsha(PId: Integer): IAyanamshaDto;
      function GetAllAyanamshas: TAyanamshaDtoArray;
  end;

  TCoordinateSystemDataEndpoint = class(TInterfacedObject,ICoordinateSystemDataEndpoint)
    strict private
      Dao: ILookUpValueDao;
    public
      constructor Create(PDao: ILookUpValueDao);
      function GetName(PId: Integer): ILookUpValueDto;
      function GetAllItems():TLookUpValueDtoArray;
      property AllItems: TLookUpValueDtoArray read GetAllItems;
    end;

  TLookupValueDataEndpoint = class(TInterfacedObject,ILookupValueDataEndpoint)
    strict private
      Dao: ILookUpValueDao;
    public
      constructor Create(PDao: ILookUpValueDao);
      function GetName(PId: Integer): ILookUpValueDto;
      function GetAllItems():TLookUpValueDtoArray;
      property AllItems: TLookUpValueDtoArray read GetAllItems;
    end;

  TFullChartEndpoint = class(TInterfacedObject, IFullChartEndpoint)
    strict private
      Handler: IFullChartHandler;
    public
      constructor Create;
      function HandleRequest(PFullChartRequest: IFullChartRequest): IFullChartResponse;
  end;


implementation

  uses BeChartsHandlersImpl;

  { THouseSystemDataEndpoint }

  Constructor THouseSystemDataEndpoint.Create(PDao: IHouseSystemsDao);
    begin
      Dao:= PDao;
    end;

  function THouseSystemDataEndpoint.GetSystem(PId: Integer): IHouseSystemDto;
    begin
      Result:= Dao.Read(PId)[0];
    end;

  function THouseSystemDataEndpoint.GetAllSystems: THouseSystemDtoArray;
    begin
      Result:= Dao.ReadAll('Name');
    end;

  { TAyanamshaDataEndpoint }
  Constructor TAyanamshaDataEndpoint.Create(PDao: IAyanamshasDao);
    begin
      Dao:= PDao;
    end;

  function TAyanamshaDataEndpoint.GetAyanamsha(PId: Integer): IAyanamshaDto;
    begin
      Result:= Dao.Read(PId)[0];
    end;

  function TAyanamshaDataEndpoint.GetAllAyanamshas: TAyanamshaDtoArray;
    begin
      Result:= Dao.ReadAll('Name');
    end;



  { TCoordinateSystemDataEndpoint }
  constructor TCoordinateSystemDataEndpoint.Create(PDao: ILookUpValueDao);
    begin
      Dao:= PDao;
    end;

  function TCoordinateSystemDataEndpoint.GetName(PId: Integer): ILookUpValueDto;
    begin
      Result:= Dao.Read(PId)[0];
    end;

  function TCoordinateSystemDataEndpoint.GetAllItems():TLookUpValueDtoArray;
    begin
      Result:= Dao.ReadAll('Name');
    end;

  { TLookupValueDataEndpoint }
  constructor TLookupValueDataEndpoint.Create(PDao: ILookUpValueDao);
    begin
      Dao:= PDao;
    end;

  function TLookupValueDataEndpoint.GetName(PId: Integer): ILookUpValueDto;
    begin
      Result:= Dao.Read(PId)[0];
    end;

  function TLookupValueDataEndpoint.GetAllItems():TLookUpValueDtoArray;
    begin
      Result:= Dao.ReadAll('Name');
    end;

  { TFullChartEndpoint}
  constructor TFullChartEndpoint.Create;
  begin
    Handler := TFullChartHandler.Create;
  end;

  function TFullChartEndpoint.HandleRequest(PFullChartRequest: IFullChartRequest) : IFullChartResponse;
  begin
    Result:= Handler.HandleRequest(PFullChartRequest);
  end;


end.

