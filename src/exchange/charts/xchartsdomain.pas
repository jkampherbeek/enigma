{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit XChartsDomain;
{< Objects for charts that are used to communicate between frontend and backend. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XSharedDomain;

type

  IHousePositionFull = interface;
  ICelestialObjectFull = interface;
  THousePositionFullArray = Array of IHousePositionFull;
  TCelestialObjectFullArray = Array of ICelestialObjectFull;
  TIntArray = Array of Integer;
  ILookUpValueDto = Interface;
  IVersionDto = Interface;
  IHouseSystemDto = Interface;
  IAyanamshaDto = Interface;
  IBodyDto = Interface;
  IAspectDto = Interface;
  IConfigurationDto = Interface;
  IGlyphsBodiesDto = Interface;
  IGlyphsConfigurationDto = Interface;
  TLookUpValueDtoArray = Array of ILookUpValueDto;
  TVersionDtoArray = Array of IVersionDto;
  THouseSystemDtoArray = Array of IHouseSystemDto;
  TAyanamshaDtoArray = Array of IAyanamshaDto;
  TBodyDtoArray = Array of IBodyDto;
  TAspectDtoArray = Array of IAspectDto;
  TConfigurationDtoArray = Array of IConfigurationDto;
  TGlyphsBodiesDtoArray = Array of IGlyphsBodiesDto;
  TGlyphsConfigurationDtoArray = Array of IGlyphsConfigurationDto;

  { Dto for simple lookup values that consists of only id, name and description. @b
  - Factory: none @br
  - Fake: FakeLookUpValueDto and FakeLookUpValueDto2 @br
  - Created: 2018 @br
  - Last update: 2019-04-05  }
  ILookUpValueDto = Interface ['{EA52ABB3-FB28-4A0A-BCED-171495F2BB4A}']
    function GetId: Integer;
    function GetName: String;
    function GetDescription: String;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property Description: String read GetDescription;
  end;

  { Dto for versions, both for database and for application. @b
  - Factory: none @br
  - Fake: FakeVersionDto @br
  - Created: 2018-12-30 @br
  - Last update: 2019-04-05  }
  IVersionDto = Interface ['{DEA9345E-EEB4-4A81-A84E-A19C6466E953}']
    function GetId: Integer;
    function GetMajor: Integer;
    function GetMinor: Integer;
    function GetMicro: Integer;
    function GetDate: String;
    property Id: Integer read GetId;
    property Major: Integer read GetMajor;
    property Minor: Integer read GetMinor;
    property Micro: Integer read GetMicro;
    property Date: String read GetDate;
  end;

  { Dto for house systems. @b
  - Factory: none @br
  - Fake: FakeHouseSystemDto and FakeHouseSystemDto2 @br
  - Created: 2018-12-31 @br
  - Last update: 2019-04-05  }
  IHouseSystemDto = Interface ['{31DA7D77-217B-4656-87EE-856B8BD3E834}']
    function GetId: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetNrOfHouses: Integer;
    function IsCounterClockWise: Boolean;
    function IsQuadrantSystem: Boolean;
    function IsCuspIsStart: Boolean;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property Description: String read GetDescription;
    property NrOfHouses: Integer read GetNrOfHouses;
    property CounterClockWise: Boolean read IsCounterClockWise;
    property QuadrantSystem: Boolean read IsQuadrantSystem;
    property CuspIsStart: Boolean read IsCuspIsStart;
  end;

  { Dto for ayanamsha's. @b
  - Factory: none @br
  - Fake: FakeAyanamshaDto and FakeAyanamshaDto2 @br
  - Created: 2018-12-31 @br
  - Last update: 2019-04-05  }
  IAyanamshaDto = Interface ['{D53A8AA9-7A60-4FA9-B2F4-D9FEF449899B}']
    function GetId: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetOffset2000: Double;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property Description: String read GetDescription;
    property Offset2000: Double read GetOffset2000;
  end;

  { Dto for celestial bodies. @b
  - Factory: none @br
  - Fake: FakeBodyDto and FakeBodyDto2 @br
  - Created: 2019-01-02 @br
  - Last update: 2019-04-05  }
  IBodyDto = Interface ['{88F0164D-F983-4315-BD3C-C5B60560D59A}']
    function GetId: Integer;
    function GetName: String;
    function GetBodyCategory: Integer;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property BodyCategory: INteger read GetBodyCategory;
  end;

  { Dto for aspects. @b
  - Factory: none @br
  - Fake: FakeAspectDto and FakeAspectDto2 @br
  - Created: 2019-01-07 @br
  - Last update: 2019-04-05  }
  IAspectDto = Interface ['{7BF8D7ED-BB57-418E-8F82-58D6CA375507}']
    function GetId: Integer;
    function GetName: String;
    function GetAngle: Double;
    function GetAspectCategory: Integer;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property Angle: Double read GetAngle;
    property AspectCategory: Integer read GetAspectCategory;
  end;


  { Dto for configurations. @b
  - Factory: none @br
  - Fake: FakeConfigurationDto and FakeConfigurationDto2 @br
  - Created: 2019-01-09 @br
  - Last update: 2019-04-05  }
  IConfigurationDto = Interface ['{423EECAD-8F51-4B24-9764-771FC2B5234F}']
    function GetId: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetBaseOrbAspects: Double;
    function GetBaseOrbMidpoints: Double;
    function GetCoordinateSystem: Integer;
    function GetAyanamsha: Integer;
    function GetHouseSystem: integer;
    function GetObserverPosition: Integer;
    function GetProjectionType: Integer;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property Description: String read GetDescription;
    property BaseOrbAspects: Double read GetBaseOrbAspects;
    property BaseOrbMidpoints: Double read GetBaseOrbMidpoints;
    property CoordinateSystem: Integer read GetCoordinateSystem;
    property Ayanamsha: Integer read GetAyanamsha;
    property HouseSystem: Integer read GetHouseSystem;
    property ObserverPosition: Integer read GetObserverPosition;
    property ProjectionType: Integer read GetProjectionType;
  end;


  { Dto for supported glyph-body combinations. @b
  - Factory: none @br
  - Fake: FakeGlyphsBodiesDto and FakeGlyphsBodiesDto2 @br
  - Created: 2019-02-08 @br
  - Last update: 2019-04-05  }
  IGlyphsBodiesDto = Interface ['{261F420A-E11B-48DC-A51F-E5FE93321874}']
    function GetId: LongInt;
    function GetGlyph: String;
    property BodyId: Integer read GetId;
    property Glyph: String read GetGlyph;
  end;

  { Dto for actual glyphs as used in a configuration. @br
    The variable Category discriminates between celestial object/sign/aspect/diverse and has, in the same sequence,
    the following possivle values: O, S, A, D.
  - Factory: none @br
  - Fake: FakeGlyphsConfigurationDto and FakeGlyphsConfigurationDto2 @br
  - Created: 2019-02-11 @br
  - Last update: 2019-02-13  }
  IGlyphsConfigurationDto = Interface ['{A286634A-5A78-4868-B326-37DBA93B549F}']
    function GetConfigId: LongInt;
    function GetItemId: LongInt;
    function GetCategory: String;
    function GetGlyph: String;
    property ConfigId: Integer read GetConfigId;
    property ItemId: Integer read GetItemId;
    property Category: String read GetCategory;
    property Glyph: String read GetGlyph;
  end;

{ Specifications for a housesystem. @br
- Factory: none @br
- Fake: FakeHouseSystemSpec @br
- Created: 2018 @br
- Last update: 2018-12-09
}
IHouseSystemSpec = interface ['{DC13CF48-133C-47DC-8485-EB84986E9A96}']     { TODO: R_0.6 refactor, retrieve id for SE from new mapping class }
  function GetName: String;
  function GetSeId: String;
  function GetDescription: String;
  function GetId: Integer;
  function GetNumberOfHouses: Integer;
  function IsQuadrantSystem: Boolean;
  function IsCuspIsStart: Boolean;
  property Name:String read GetName;
  { Id as used by the Swiss Ephemeris. }
  property SeId: String read GetSeId;
  property Description: String read GetDescription;
  { Id as used by Enigma. }
  property Id: Integer read GetId;
  property NumberOfHouses: Integer read GetNumberOfHouses;
  property QuadrantSystem: Boolean read IsQuadrantSystem;
  property CuspIsStart: Boolean read IsCuspIsStart;
end;

{ Positions for a house cusp in ecliptic, equatorial and horizontal coördinates. No latitude, as this is always zero.@br
- Factory: none @br
- Fake: FakeHousePositionFull @br
- Created: 2018 @br
- Last update: 2018-12-10}
IHousePositionFull = interface ['{3FD7D6FC-B650-4E7E-8AFC-E12F186A3C5A}']
  function GetLongitude: Double;
  function GetRightAscension: Double;
  function GetDeclination: Double;
  function GetAzimuth: Double;
  function GetAltitude: Double;
  property Longitude: Double read GetLongitude;
  property RightAscension: Double read GetRightAscension;
  property Declination: Double read GetDeclination;
  property Azimuth: Double read GetAzimuth;
  property Altitude: Double read GetAltitude;
end;

{ DTO for a position of a celestial object, defines three elements of a coördinatesystem and also the speed of these
elements. @br
DeviationPos and DeviationSpeed define the deviation from the mainplain in degrees, resp. latitude, declination or
altitude.!@br
DistancePos and DistanceSpeed define the distance from the observationpoint in AU and is the same for ecliptical,
equatorial and horizontal coördinates. @br
The constructor expects an array of 6 doubles as calculated by the SE and respectively containing the vlaues for
MainPos, DeviationPos, DistancePos, MainSpeed, DeviationSpeed and DistanceSpeed in that sequence.@br
- Factory: none @br
- Fake: FakeCelestialObjectSimple @br
- Created: 2018 @br
- Last update: 2018-12-10}
ICelestialObjectSimple = interface ['{7B44D69C-14B4-4A73-9F35-644831390CED}']
  function GetMainPos: Double;
  function GetDeviationPos: Double;
  function GetDistancePos: Double;
  function GetMainSpeed: Double;
  function GetDeviationSpeed: Double;
  function GetDistanceSpeed: Double;
  property MainPos: Double read GetMainPos;
  property DeviationPos: Double read GetDeviationPos;
  property DistancePos: Double read GetDistancePos;
  property MainSpeed: Double read GetMainSpeed;
  property DeviationSpeed: Double read GetDeviationSpeed;
  property DistanceSpeed: Double read GetDistanceSpeed;
end;

{ DTO for a fully defined position of a celestial object. @br
- Factory: none @br
- Fake: FakeCelestialObjectFull @br
- Created: 2018 @br
- Last update: 2018-12-10}
ICelestialObjectFull = interface ['{AB456E97-EB4A-4F9A-8680-11FCF90BA98F}']
  function GetObjectId: Integer;
  function GetEclipticalPos: ICelestialObjectSimple;
  function GetEquatorialPos: ICelestialObjectSimple;
  function GetHorizontalPos: ICelestialObjectSimple;
  property ObjectId: Integer read GetObjectId;
  property EclipticalPos: ICelestialObjectSimple read GetEclipticalPos;
  property EquatorialPos: ICelestialObjectSimple read GetEquatorialPos;
  property HorizontalPos: ICelestialObjectSimple read GetHorizontalPos;
end;

{ Response to a request to calculate houses. For convenience, cusps start with index 1. @br
- Factory: none @br
- Fake: FakeHousesResponse @br
- Created: 2018 @br
- Last update: 2018-12-13}
IHousesResponse = interface ['{81CC71EB-1828-403C-B7F8-F9BED644BD9B}']
  function GetMc: IHousePositionFull;
  function GetAsc: IHousePositionFull;
  function GetCusps: THousePositionFullArray;
  function GetHouseSystemSpec: IHouseSystemSpec;
  property Mc: IHousePositionFull read GetMc;
  property Asc: IHousePositionFull read GetAsc;
  property HouseSystemSpec: IHouseSystemSpec read GetHouseSystemSpec;
  property Cusps: THousePositionFullArray read GetCusps;
end;

{ DTO for the settings for a calculation. Calculates the value for the SE flags.@br
- Factory: none @br
- Fake: FakeCalculationSettings @br
- Created: 2018 @br
- Last update: 2018-12-09  }
ICalculationSettings = interface ['{5085546F-A11B-4A31-BE4F-3FDC621A464B}']
  function GetPosition: Integer;
  function GetAyanamsha: Integer;
  function GetHouseSystemSpec: IHouseSystemSpec;
  function GetObjects: TIntArray;
  function GetReferenceFrame: IReferenceFrame;
  property Position: Integer read GetPosition;
  property Ayanamsha: Integer read GetAyanamsha;
  property HouseSystemSpec: IHouseSystemSpec read GetHouseSystemSpec;
  property Objects: TIntArray read GetObjects;
  property ReferenceFrame: IReferenceFrame read GetReferenceFrame;
end;

{ Request for the calculation of a chart. @br
- Factory: none @br
- Fake: FakeFullChartRequest @br
- Created: 2018 @br
- Last update: 2018-12-12  }
IFullChartRequest = interface ['{4D15622F-B0BA-4404-9056-4FF1A8E7D596}']
    function GetName: String;
    function GetLocation: IValidatedLocation;
    function GetDate: IValidatedDate;
    function GetTime: IValidatedTime;
    function GetCalculationSettings: ICalculationSettings;
    property Name: String read GetName;
    property Location: IValidatedLocation read GetLocation;
    property Date: IValidatedDate read GetDate;
    property Time: IValidatedTime read GetTime;
    property CalculationSettings: ICalculationSettings read GetCalculationSettings;
end;

{ DTO containing all data for a calculated chart. @br
- Factory: none @br
- Fake: FakeFullChartResponse @br
- Created: 2018 @br
- Last update: 2018-12-13  }
IFullChartResponse = interface ['{35789E0A-E284-4E95-A21C-8A10820B1EA7}']
  function GetName: String;
  function GetAllObjects: TCelestialObjectFullArray;
  function GetHouses: IHousesResponse;
  function GetFullChartRequest: IFullChartRequest;
  property Name: String read GetName;
  property AllObjects: TCelestialObjectFullArray read GetAllObjects;
  property Houses: IHousesResponse read GetHouses;
  property FullChartRequest: IFullChartRequest read GetFullChartRequest;
end;



implementation

end.

