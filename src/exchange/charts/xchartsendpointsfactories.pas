{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit XChartsEndpointsFactories;
{< Factories for XChartsEndpoints }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XChartsEndpoints;

type

  { Factory for HouseSystemDataEndpoint. @br
   - Created: 2019-01-12 @br
   - Last update: 2019-01-12  }
   TFactoryHouseSystemDataEndpoint = Class
     function GetInstance: IHouseSystemDataEndpoint;
   end;

   { Factory for AyanamshaDataEndpoint. @br
   - Created: 2019-01-20 @br
   - Last update: 2019-01-20  }
   TFactoryAyanamshaDataEndpoint = Class
     function GetInstance: IAyanamshaDataEndpoint;
   end;

   { Factory for CoordinateSystemDataEndpoint. @br
   - Created: 2019-01-21 @br
   - Last update: 2019-01-21  }
   TFactoryCoordinateSystemDataEndpoint = Class
     function GetInstance: ICoordinateSystemDataEndpoint;
   end;

   { Factory for LookupValueDataEndpoint. @br
   - Created: 2019-01-23 @br
   - Last update: 2019-01-23  }
   TFactoryLookupValueDataEndpoint = Class
     strict private
       TableName: String;
     public
       constructor Create(PTableName: String);
       function GetInstance: ILookupValueDataEndpoint;
   end;

implementation

uses XChartsEndpointsImpl, BeDbDao, BeDbDaoFactories;

  { TFactoryHouseSystemDataEndpoint }
  function TFactoryHouseSystemDataEndpoint.GetInstance: IHouseSystemDataEndpoint;
    begin
      Result:= THouseSystemDataEndpoint.Create(TFactoryHouseSystemsDao.Create.GetInstance);
    end;

  { TFactoryAyanamshaDataEndpoint }
  function TFactoryAyanamshaDataEndpoint.GetInstance: IAyanamshaDataEndpoint;
    begin
      Result:= TAyanamshaDataEndpoint.Create(TFactoryAyanamshasDao.Create.GetInstance);
    end;

  { TFactoryCoordinateSystemDataEndpoint }
  function TFactoryCoordinateSystemDataEndpoint.GetInstance: ICoordinateSystemDataEndpoint;
    var
      Dao: ILookUpValueDao;
      Factory: TFactoryLookUpValueDao;
    begin
      Factory:= TFactoryLookUpValueDao.create('CoordinateSystems');
      Dao:=Factory.GetInstance;
      Result:= TCoordinateSystemDataEndpoint.Create(Dao);
    end;

  { TFactoryLookupValueDataEndpoint }
  constructor TFactoryLookupValueDataEndpoint.Create(PTableName: String);
    begin
      TableName:= PTableName;
    end;

  function TFactoryLookupValueDataEndpoint.GetInstance: ILookupValueDataEndpoint;
    var
      Dao: ILookUpValueDao;
      Factory: TFactoryLookUpValueDao;
    begin
      Factory:= TFactoryLookUpValueDao.create(TableName);
      Dao:= Factory.GetInstance;
      Result:= TLookupValueDataEndpoint.Create(Dao);
    end;

end.

