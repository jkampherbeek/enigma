{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit UiSharedFormattedPositionsImpl;
{< Implementations for interfaces in UiSharedFormattedPositions.}
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UiSharedFormattedPositions;

type
  TDegreeMinuteSign = class(TInterfacedObject, IDegreeMinuteSign)
    strict private
      Position: Double;
      Degrees, Minutes, Sign: String;
      DegreesNr, DegreesNrWithSigns, MinutesNr, SignIndex: Integer;
      procedure DefineTheParts;
    public
      constructor Create(PPosition: Double);
      function GetDegrees: String;
      function GetMinutes: String;
      function GetDegreesMinutes: String;
      function GetSign: String;
  end;

  TSexagesimalPositions = class(TInterfacedObject, ISexagesimalPositions)
    strict private
      Signs: array[1..12] of String;
      PositionInDegrees: Double;
      Degrees, DegreesWithSigns, Minutes, Seconds, SignIndex: Integer;
      procedure DefineElementsForPosition;
      procedure DefineSigns;
      function CreateStringForDegrees(UseSigns: Boolean): String;
      function CreateStringForSigns: String;
    public
      constructor Create(DecimalDegrees: Double);
      function GetSexagesimalDegrees: String;
      function GetSexagesimalSigns: String;
  end;

implementation

const
  SPACE = ' ';
  ZERO = '0';
  DEGREES_IN_SIGN = 30;
  MINUTES_IN_DEGREE = 60;
  SECONDS_IN_MINUTE = 60;
  DEGREE_SIGN = '°';
  MINUTE_SIGN = '''';
  SECOND_SIGN = '"';

  { TDegreeMinuteSign }

    constructor TDegreeMinuteSign.Create(PPosition: Double);
      begin
        Position:= PPosition;
        DefineTheParts;
      end;

    function TDegreeMinuteSign.GetDegrees: String;
      begin
        Result:= Degrees;
      end;

    function TDegreeMinuteSign.GetMinutes: String;
      begin
        Result:= Minutes;
      end;

    function TDegreeMinuteSign.GetDegreesMinutes: String;
      begin
        Result:= Degrees + Minutes;
      end;

    function TDegreeMinuteSign.GetSign: String;
     begin
       Result:= Sign;
     end;

    procedure TDegreeMinuteSign.DefineTheParts;
    var
      TempAmount: Double;
    begin
      DegreesNr:= Trunc(Position);
      TempAmount:= Frac(Position) * MINUTES_IN_DEGREE;
      MinutesNr:= Trunc(TempAmount);
      SignIndex:= Trunc(DegreesNr DIV DEGREES_IN_SIGN) + 1;
      DegreesNrWithSigns:= DegreesNr - ((SignIndex - 1) * DEGREES_IN_SIGN);
      Degrees:= String(IntToStr(DegreesNrWithSigns) + DEGREE_SIGN);
      Minutes:= String(IntToStr(MinutesNr) + MINUTE_SIGN);
      Sign:= 'x';   { TODO : R_0.6 Define real value for glyph }
    end;

    { SexagesimalPositions }

    constructor TSexagesimalPositions.Create(DecimalDegrees: Double);
    begin
      PositionInDegrees:= DecimalDegrees;
      DefineSigns;
      DefineElementsForPosition;
    end;

    procedure TSexagesimalPositions.DefineSigns;
    begin
      Signs[1]:='AR';
      Signs[2]:='TA';
      Signs[3]:='GE';
      Signs[4]:='CN';
      Signs[5]:='LE';
      Signs[6]:='VI';
      Signs[7]:='LI';
      Signs[8]:='SC';
      Signs[9]:='SA';
      Signs[10]:='CP';
      Signs[11]:='AQ';
      Signs[12]:='PI';
    end;

    procedure TSexagesimalPositions.DefineElementsForPosition;
    var
      TempAmount: Double;
    begin
      Degrees:= Trunc(PositionInDegrees);
      TempAmount:= Frac(PositionInDegrees) * MINUTES_IN_DEGREE;
      Minutes:= Trunc(TempAmount);
      Seconds:= Trunc(frac(TempAmount) * SECONDS_IN_MINUTE);
      SignIndex:= Trunc(Degrees DIV DEGREES_IN_SIGN) + 1;
      DegreesWithSigns:= Degrees - ((SignIndex - 1) * DEGREES_IN_SIGN);
    end;

    function TSexagesimalPositions.CreateStringForDegrees(UseSigns: Boolean): String;
    var
      Deg, Min, Sec, FormattedText: String;
      CurrentDegrees: Integer;
    begin
      if (UseSigns) then CurrentDegrees := DegreesWithSigns
      else CurrentDegrees := Degrees;
      Deg := IntToStr(CurrentDegrees);
      Min := IntToStr(Minutes);
      Sec := IntToStr(Seconds);
      if ((CurrentDegrees < 100) and (UseSigns = false)) then Deg:= Concat(SPACE, Deg);
      if (CurrentDegrees < 10) then Deg:= Concat(SPACE, Deg);
      if (Minutes < 10) then Min:= Concat(ZERO, Min);
      if (Seconds < 10) then Sec:= Concat(ZERO, Sec);
      FormattedText:= Concat(Deg, DEGREE_SIGN, Min, MINUTE_SIGN, Sec, SECOND_SIGN);
      Result:= FormattedText;
    end;

    function TSexagesimalPositions.CreateStringForSigns: String;
    var
      FormattedText: String;
    begin
      FormattedText:= CreateStringForDegrees(true);
      Result:= Concat(FormattedText, SPACE, Signs[SignIndex]);
    end;

    function TSexagesimalPositions.GetSexagesimalDegrees: String;
    begin
      Result:= CreateStringForDegrees(false);
    end;

    function TSexagesimalPositions.GetSexagesimalSigns: String;
    begin
      Result:= CreateStringForSigns;
    end;

end.

