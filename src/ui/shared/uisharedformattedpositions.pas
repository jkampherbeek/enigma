{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit UiSharedFormattedPositions;
{< Classes that format several types of output.}
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  { Position text splitted in degrees, minutes and sign.@br
    Used for showing positions in a graphic chart..@br
  - Factory: none @br
  - Fake: none @br
  - Created: 2019-03-11 @br
  - Last update: 2019-03-11 }
  IDegreeMinuteSign = Interface ['{99F7D15C-6A3F-4F66-97B3-039D55B5F2C8}']
    { Degrees, no leading zero added. Followed by degree sign.}
    function GetDegrees: String;
    { Minutes, no leading zero added. Followed by minute sign.}
    function GetMinutes: String;
    { Get degree + degree sign plus minute and minute sign}
    function GetDegreesMinutes: String;
    { Character for glyph in font EnigmaAstrology.}
    function GetSign: String;
    property Degrees: String read GetDegrees;
    property Minutes: String read GetMinutes;
    property DegreesMinutes: String read GetDegreesMinutes;
    property Sign: String read GetSign;
   end;

  { Formats to sexagesimal (degrees, minutes, seconds) positions.
  Optionally handles signs but uses for now only abbreviations to indicate the signs. @br
  - Created: 2018 @br
  - Last update: 2019-03-31}
  ISexagesimalPositions = Interface ['{330DC74B-2CE4-4870-A27D-8C70F595B669}']
    function GetSexagesimalDegrees: String;
    function GetSexagesimalSigns: String;
    property SexagesimalDegrees: String read GetSexagesimalDegrees;
    property SexagesimalSigns: String read GetSexagesimalSigns;
  end;


implementation


end.

