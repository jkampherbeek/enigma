{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit UiSharedDomainImpl;
{< Implementations for interfaces in unit UiSharedDomain }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UiSharedDomain;
type

  TGraphicBodyPosition = class(TInterfacedObject, IGraphicBodyPosition)
    strict private
      BodyIndex: Integer;
      EclipticPosition, MundanePosition, PlotPosition: Double;
    public
      constructor Create(PBodyIndex: Integer; PEclipticPosition, PMundanePosition: Double);
      function GetBodyIndex: Integer;
      function GetEclipticPosition: Double;
      function GetMundanePosition: Double;
      procedure SetMundanePosition(PMundanePosition: Double);
      procedure SetPlotPosition(PPlotPosition: Double);
      function GetPlotPosition: Double;
  end;

  TSortedGraphicBodyPositions = class(TInterfacedObject, ISortedGraphicBodyPositions)
    strict private
      MinDistance: Double;
      AllPositions: TArrayGraphicBodyPositions;
      procedure SortOnPlotPosition;
      procedure FixOverlaps;
    public
      constructor Create(PArrayGraphicBodyPositions: TArrayGraphicBodyPositions; PMinDistance: Double);
      function GetPositions: TArrayGraphicBodyPositions;
  end;

  TPresentableValue = class(TInterfacedObject, IPresentableValue)
    private
      PresText: String;
      function ConvertInput(InputValue: Double): String; virtual; abstract;
    public
      Constructor Create(PInputValue: Double);
      function GetText: String;
    end;

   { Presentable instance for a decimal value, using decimal presentation and 8 positions for the fraction.
     If necessary the fraction is postfixed with zero's to arrive at 8 positions. }
   TDecimalValue = class(TPresentableValue)
     private
       function ConvertInput(InputValue: Double): String; override;
     end;

   { Presentable instance for a sexagesimal value, using degrees, minutes and seconds but no fraction.
     If necessary the minutes and seconds are prefixed with zero's to arrive at 2 positions. }
   TSexagesimalValue = class(TPresentableValue)
     private
       function ConvertInput(InputValue: Double): String; override;
   end;

  { Presentable instance for a sexagesimal value, using signs, degrees, minutes and seconds but no fraction.@br
    If necessary the minutes and seconds are prefixed with zero's to arrive at 2 positions. }
   TSignDMSValue = class(TPresentableValue)
     private
       Signs: array[1..12] of String;
       Glyphs: array[1..12] of String;
       SignIndex: Integer;
       procedure DefineSigns;
       procedure DefineGlyphs;
       function ConvertInput(InputValue: Double): String; override;
     public
       function GetGlyph:String;
       function GetAbbreviation: String;
     end;

   TMetaInfoValues = class
     strict private
       Name, Location, DateTime, Settings: String;
     public
       Constructor Create(PName, PLocation, PDateTime, PSettings: String);
       function GetName: String;
       function GetLocation: String;
       function GetDateTime: String;
       function GetSettings: String;
     end;


implementation

  const
    DEGREES_IN_SIGN = 30;
    MINUTES_IN_DEGREE = 60;
    SECONDS_IN_MINUTE = 60;
    DEGREE_SIGN = '°';
    MINUTE_SIGN = '''';
    SECOND_SIGN = '"';

  { TGraphicBodyPosition }
    constructor TGraphicBodyPosition.Create(PBodyIndex: Integer; PEclipticPosition, PMundanePosition: Double);
      begin
        BodyIndex:= PBodyIndex;
        EclipticPosition:= PEclipticPosition;
        MundanePosition:= PMundanePosition;
        PlotPosition:= MundanePosition;
      end;

    function TGraphicBodyPosition.GetBodyIndex: Integer;
      begin
        Result:= BodyIndex;
      end;

    function TGraphicBodyPosition.GetEclipticPosition: Double;
      begin
        Result:= EclipticPosition;
      end;

    procedure TGraphicBodyPosition.SetPlotPosition(PPlotPosition: Double);
      begin
        PlotPosition:= PPlotPosition;
      end;

    function TGraphicBodyPosition.GetMundanePosition: Double;
      begin
        Result:= MundanePosition;
      end;

    procedure TGraphicBodyPosition.SetMundanePosition(PMundanePosition: Double);
      begin
        MundanePosition:= PMundanePosition;
      end;

    function TGraphicBodyPosition.GetPlotPosition: Double;
      begin
        Result:= Plotposition;
      end;

 { TSortedGraphicBodyPositions }
   constructor TSortedGraphicBodyPositions.Create(PArrayGraphicBodyPositions: TArrayGraphicBodyPositions; PMinDistance: Double);
     begin
       MinDistance:= PMinDistance;
       AllPositions := PArrayGraphicBodyPositions;
       SortOnPlotPosition;
       FixOverlaps;
     end;

   function TSortedGraphicBodyPositions.GetPositions: TArrayGraphicBodyPositions;
     begin
       Result:= AllPositions;
     end;

   procedure TSortedGraphicBodyPositions.SortOnPlotPosition;
     var
       i, j, size: Integer;
       tmp: IGraphicBodyPosition;
     begin
       size:= Length(AllPositions);
       for i := size - 1 downto 0 do begin
         for j := 0 to i-1 do begin
           if (AllPositions[j].PlotPosition > AllPositions[j+1].PlotPosition) then
             begin
               tmp := AllPositions[j];
               AllPositions[j] := AllPositions[j+1];
               AllPositions[j+1] := tmp;
             end;
         end;
       end;
     end;

   procedure TSortedGraphicBodyPositions.FixOverlaps;
     var
       i, size: Integer;
       pos1, pos2: IGraphicBodyPosition;
       plotPos1, plotPos2: Double;
     begin
       size:= Length(AllPositions);
       for i := 0 to size-2 do begin
         pos1:= AllPositions[i];
         pos2:= AllPositions[i+1];
         plotPos1:= pos1.PlotPosition;
         plotPos2:= pos2.PlotPosition;
         if ((plotPos2 - plotPos1 ) < MinDistance) then begin
           AllPositions[i+1].PlotPosition:= AllPositions[i].PlotPosition + MinDistance;
         end;
       end;
     end;

   { TPresentableValue }

   Constructor TPresentableValue.Create(PInputValue: Double);
     begin
       PresText:= ConvertInput(PInputValue);
     end;

   function TPresentableValue.GetText: String;
     begin
       Result:= PresText;
     end;

   { TDecimalValue }
   function TDecimalValue.ConvertInput(InputValue: Double): String;
     begin
        Result:= Format('%3.8f',[InputValue]);
     end;


   { TSexagesimalValue }
   function TSexagesimalValue.ConvertInput(InputValue: Double): String;
     var
       DegTxt, MinTxt, SecTxt: String;
       Degrees, Minutes, Seconds: Integer;
       TempAmount, AbsInputValue: Double;
       IsNegative: Boolean;
     begin
       IsNegative:= (InputValue < 0.0);
       AbsInputValue:= Abs(InputValue);
       Degrees:= Trunc(AbsInputValue);
       TempAmount:= Frac(AbsInputValue) * MINUTES_IN_DEGREE;
       Minutes:= Trunc(TempAmount);
       Seconds:= Trunc(frac(TempAmount) * SECONDS_IN_MINUTE);
       DegTxt := Format('%3d',[Degrees]);
       MinTxt := Format('%2d',[Minutes]);
       SecTxt := Format('%2d',[Seconds]);
       if IsNegative then DegTxt:= '-' + TrimLeft(DegTxt);
       Result:= Concat(DegTxt, DEGREE_SIGN, MinTxt, MINUTE_SIGN, SecTxt, SECOND_SIGN);
     end;


   { TSignDMSValue }
   function TSignDMSValue.GetGlyph:String;
     begin
       Result:= Glyphs[SignIndex];
     end;

   function TSignDMSValue.GetAbbreviation:String;
     begin
       Result:= Signs[SignIndex];
     end;

   procedure TSignDMSValue.DefineSigns;
     begin
       Signs[1]:='AR';
       Signs[2]:='TA';
       Signs[3]:='GE';
       Signs[4]:='CN';
       Signs[5]:='LE';
       Signs[6]:='VI';
       Signs[7]:='LI';
       Signs[8]:='SC';
       Signs[9]:='SA';
       Signs[10]:='CP';
       Signs[11]:='AQ';
       Signs[12]:='PI';
     end;

   procedure TSignDMSValue.DefineGlyphs;        { TODO : R_0.6 Use configuration to define glyphs }
     begin
       Glyphs[1]:= '1';
       Glyphs[2]:= '2';
       Glyphs[3]:= '3';
       Glyphs[4]:= '4';
       Glyphs[5]:= '5';
       Glyphs[6]:= '6';
       Glyphs[7]:= '7';
       Glyphs[8]:= '8';
       Glyphs[9]:= '9';
       Glyphs[10]:= '0';
       Glyphs[11]:= '-';
       Glyphs[12]:= '=';
     end;

   function TSignDMSValue.ConvertInput(InputValue: Double): String;
     var
       Degrees: Integer;
       DecDegreesWithoutSign: Double;
       SexagValue: TSexagesimalValue;
     begin
       DefineSigns;
       DefineGlyphs;
       Degrees:= Trunc(InputValue);
       SignIndex:= Trunc(Degrees DIV DEGREES_IN_SIGN) + 1;
       DecDegreesWithoutSign:= InputValue - ((SignIndex - 1) * DEGREES_IN_SIGN);
       SexagValue:= TSexagesimalValue.Create(DecDegreesWithoutSign);
       Result:= SexagValue.GetText;
     end;

   {  TMetaInfoValues }
   Constructor TMetaInfoValues.Create(PName, PLocation, PDateTime, PSettings: String);
     begin
       Name := PName;
       Location:= PLocation;
       DateTime:= PDateTime;
       Settings:= PSettings;
     end;

   function TMetaInfoValues.GetName: String;
     begin
       Result:= Name;
     end;

   function TMetaInfoValues.GetLocation: String;
     begin
       Result:= Location;
     end;

   function TMetaInfoValues.GetDateTime: String;
     begin
       Result:= DateTime;
     end;

   function TMetaInfoValues.GetSettings: String;
     begin
       Result:= Settings;
     end;


end.

