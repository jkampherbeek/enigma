{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit UiSharedDomain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  { Position for drawing a glyph of a body on the chart. @br
    Eclipticpositions is the standard longitude. @br
    MundanePosition is the angle from the ascendant (counterclockwise). @br
    Plotposition is the angle used for drawing the glyph.
    Often this is the same as the mundaneposition but it could change if several celestial objects are near each other. @br
  - Factory: none @br
  - Fake: none. @br
  - Created: 2019-03-03 @br
  - Last update: 2019-03-09 }
  IGraphicBodyPosition = Interface ['{00936E5A-EAA8-4F5D-AF02-9028ADD9148F}']
    function GetBodyIndex: Integer;
    function GetEclipticPosition: Double;
    function GetMundanePosition: Double;
    procedure SetMundanePosition(PMundanePosition: Double);
    function GetPlotPosition: Double;
    procedure SetPlotPosition(PPlotPosition: Double);
    property BodyIndex: Integer read GetBodyIndex;
    property EclipticPosition: Double read GetEclipticPosition;
    property MundanePosition: Double read GetMundanePosition write SetMundanePosition;
    property PlotPosition: Double read GetPlotPosition write SetPlotPosition;
  end;

  { Array for BodyPositions, to be used in drawing a chart. }
  TArrayGraphicBodyPositions = Array of IGraphicBodyPosition;

  { All positions of bodies to be drawn on the chart. @br
    The positions are sorted (ascending) on PlotPosition and possible overlaps are corrected. @br
    If positions are tooc klose the highest position is moved upwards (higher value for plotposition).  @br
  - Factory: none @br
  - Fake: none. @br
  - Created: 2019-03-09 @br
  - Last update: 2019-03-09 }
  ISortedGraphicBodyPositions = Interface ['{F9422E93-AA35-40A8-9F75-C7611D55FCB9}']
    function GetPositions: TArrayGraphicBodyPositions;
  end;

  { Parent for presentable values. @br
  - Created: 2018 @br
  - Last update: 2019-04-04 }
  IPresentableValue = Interface ['{F19A60D4-2634-44DA-A890-12B34751BD85}']
    function GetText: String;
    property Text: String read GetText;
  end;

  { Container for presentable metainformation about chart (inputted data). @br
  - Created: 2018 @br
  - Last update: 2019-04-04 }
  IMetaInfoValues = Interface ['{940B5616-DD52-49D2-B383-310A69149E9F}']
    function GetName: String;
    function GetLocation: String;
    function GetDateTime: String;
    function GetSettings: String;
    property Name:String read GetName;
    property Location:String read GetLocation;
    property DateTime: String read GetDateTime;
    property Settings: String read GetSettings;
  end;


implementation

end.


