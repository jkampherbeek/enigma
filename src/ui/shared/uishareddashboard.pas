unit UiSharedDashboard;
{< First Form that Enigma shows to a user.
Contains button with which the user starts the different parts of the application. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  LCLTranslator, ExtCtrls, Menus, UiChartsMain, UiChartsInput;

type

  { Dashboard that is shown after starting the program. It presents a choice to select one of the main functionalities
    of Enigma.@br
    Created: 2018 @br
    Last change: 2019-04-13 @br }

  { TFormDashboard }

  TFormDashboard = class(TForm)
    Btn_Charts: TButton;
    Btn_Periods: TButton;
    Btn_Statistics: TButton;
    Btn_Exit: TButton;
    Lbl_Version: TLabel;
    Lbl_Description: TLabel;
    Lbl_Archaeo: TButton;
    Img_Ziggurath: TImage;
    Lbl_Title: TLabel;

    procedure Btn_ChartsClick;
    procedure Btn_ExitClick;
    procedure FormCreate;



  private

  public

  end;

var
  FormDashboard: TFormDashboard;
  DescriptionText, VersionText : String;

implementation

{$R *.lfm}

{ TFormDashboard }


procedure TFormDashboard.Btn_ChartsClick;
begin
  FormChartsMain.Show;
end;

procedure TFormDashboard.Btn_ExitClick;
begin
  Close;
end;

procedure TFormDashboard.FormCreate;
begin
  DescriptionText:= 'Enigma is a software package for astrological research. '+ LineEnding +
                    'It is free and open source. ' + LineEnding + LineEnding +
                    'The current version contains only minimal functionality. ' + LineEnding +
                    'You can expect much more in upcoming releases. ' + LineEnding + LineEnding +
                    'For more info please check http://radixpro.com ' + LineEnding + LineEnding +
                    'Select one of the buttons below to continue. ';
  Lbl_Description.Caption:= DescriptionText;
  VersionText:= 'Release 0.5';
  Lbl_Version.Caption:= VersionText;
end;



 // Software for astrological research. Enigma is free and open source.
// Check radixpro.com for information



end.

