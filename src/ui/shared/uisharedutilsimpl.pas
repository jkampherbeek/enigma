{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit UiSharedUtilsImpl;
{< Implementations for interfaces in UiUtil. }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UiSharedUtils;

type

  TUiCalculations = class(TInterfacedObject, IUiCalculations)
    function MakeEven(PValue: Integer): Integer;
    function Range360(PInputValue: Double): Double;
    function Range360(PInputValue: Integer): Integer;
  end;


  type TRectTriangle = Class(TInterfacedObject, IRectTriangle)
    strict private
      CenterX, CenterY, Hypothenusa: Integer;
      Angle: Double;
      CalculatedPoint: TPoint;
      function DefinePoint: TPoint;
    public
      constructor Create(PCenterX, PCenterY, PHypothenusa: Integer; PAngle: Double);
      function GetXYPoint: TPoint;
  end;

implementation

uses math;

{ TUiCalculations }
  function TUiCalculations.MakeEven(PValue: Integer): Integer;
    begin
      Result:= ((PValue + 1) DIV 2) * 2;
    end;

  function TUiCalculations.Range360(PInputValue: Double): Double;
    var
      TempValue: Real;
    begin
      TempValue:= PInputValue;
      while TempValue > 0.0 do begin
        TempValue:= TempValue - 360.0;
      end;
      while TempValue < 0.0 do begin
        TempValue:= TempValue + 360.0;
      end;
      Result:= TempValue;
    end;

  function TUiCalculations.Range360(PInputValue: Integer): Integer;
    var
      TempValue: Integer;
    begin
      TempValue:= PInputValue;
      while TempValue > 0 do begin
        TempValue:= TempValue - 360;
      end;
      while TempValue < 0 do begin
        TempValue:= TempValue + 360;
      end;
      Result:= TempValue;
    end;


  { TRectTriangle }
  constructor TRectTriangle.Create(PCenterX, PCenterY, PHypothenusa: Integer; PAngle: Double);
    begin
      CenterX:= PCenterX;
      CenterY:= PCenterY;
      Hypothenusa:= PHypothenusa;
      Angle:= PAngle;
      CalculatedPoint:= DefinePoint;
    end;

  function TRectTriangle.GetXYPoint: TPoint;
    begin
      Result:= CalculatedPoint;
    end;

  function TRectTriangle.DefinePoint: TPoint;
    var
      XSize, YSize: LongInt;
    begin
      YSize:= CenterY - Floor(Sin(DegToRad(Angle)) * Hypothenusa);
      XSize:= CenterX - Floor(Cos(DegToRad(Angle)) * Hypothenusa);
      Result:= TPoint.Create(XSize, YSize);
    end;

end.

