{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit UiChartsInput;
{< Input for chart data. Is shown in a popup and asks for location (name and coördinates), date (includes CAlendar)
and time (always UT) }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, ActnList, XChartsDomain, XSharedDomain, XSharedDomainImpl, XSharedEndpoints,
  XSharedEndpointsFactories;

type

  { Form for inputting data for the calculation of a chart. @b
  - Created: 2018 @br
  - Last update: 2019-04-13  }
  TFormChartsInput = class(TForm)
    Btn_Calculate: TButton;
    Btn_Cancel: TButton;
    ButtonHelp: TButton;
    EditDateMonth: TEdit;
    EditDateDay: TEdit;
    EditDateYear: TEdit;
    EditLatitudeDegrees: TEdit;
    EditLatitudeMinutes: TEdit;
    EditLatitudeSeconds: TEdit;
    EditNameDescription: TEdit;
    EditLongitudeDegrees: TEdit;
    EditTimeHour: TEdit;
    EditLongitudeMinutes: TEdit;
    EditTimeMinute: TEdit;
    EditLongitudeSeconds: TEdit;
    EditLocationName: TEdit;
    EditTimeSecond: TEdit;
    LabelStatus: TLabel;
    LabelTime: TLabel;
    LabelCalendar: TLabel;
    LabelDate: TLabel;
    LblLatitude: TLabel;
    LabelNameDescription: TLabel;
    Lbl_Longitude: TLabel;
    Lbl_LocationTitle: TLabel;
    LabelTitle: TLabel;
    RadioButtonCalendarGregorian: TRadioButton;
    RadioButtonCalendarJulian: TRadioButton;
    RadioButtonLatitudeNorth: TRadioButton;
    RadioButtonLatitudeSouth: TRadioButton;
    RadioButtonLongitudeWest: TRadioButton;
    RadioButtonLongitudeEast: TRadioButton;
    RadioGroupCalendarGregJul: TRadioGroup;
    RadioGroupLatitude: TRadioGroup;
    RadioGroupLongitude: TRadioGroup;
    procedure Btn_CalculateClick;
    procedure Btn_CancelClick;
    procedure FormCreate;
  private
    procedure ProcessNameDescription;
    procedure ProcessLocationName;
    procedure ProcessLongitude;
    procedure ProcessLatitude;
    procedure ProcessDate;
    procedure ProcessTime;
  public

  end;

var
  ChartReqLocationName, ErrorText: String;
  ReadyForCalc, ErrorFlag: Boolean;
  FormChartsInput: TFormChartsInput;
  FullChartRequest: IFullChartRequest;
  TempDate: IValidatedDate;
  TempTime: IValidatedTime;

implementation

{$R *.lfm}

uses
  XChartsDomainImpl;

var
  NameDescription, LocationName: String;
  Longitude, Latitude: IValidatedDouble;
  Date: IValidatedDate;
  Time: IValidatedTime;
  CalculationSettings: TCalculationSettings;

const
  ERROR_NAME_DESCRIPTION  = 'Enter a name or description';
  ERROR_LOCATION = 'Enter a name for the location.';
  ERROR_LONGITUDE = 'Correct the input for longitude.';
  ERROR_LATITUDE = 'Correct the input for latitude.';
  ERROR_DATE = 'Correct the input for date.';
  ERROR_TIME = 'Correct the input for time.';
  INSTRUCTION = 'Enter all values and press ''Calculate'' or press ''Cancel'' to close this window.';
  CAL_GREG = 'g';
  CAL_JUL = 'j';
  EMPTY_SECONDS = '00';
  LAT_NORTH = 'N';
  LAT_SOUTH = 'S';
  LONG_EAST = 'E';
  LONG_WEST = 'W';
  BG_COLOR = '$00F5F5E2';
  EMPTY_STRING = '';

{ TFormChartsInput }

  procedure TFormChartsInput.ProcessNameDescription;
    begin
      EditNameDescription.Color:= clDefault;
      NameDescription := Trim(EditNameDescription.Text);
      if Length(NameDescription) <= 0 then begin
          ErrorFlag := True;
          ErrorText := ErrorText + ERROR_NAME_DESCRIPTION + LineEnding;
          EditNameDescription.Color:= clYellow;
      end;
    end;

  procedure TFormChartsInput.ProcessLocationName;
    begin
      EditLocationName.Color:= clDefault;
      LocationName := Trim(EditLocationName.Text);
      if Length(LocationName) <= 0 then begin
          ErrorFlag := True;
          ErrorText := ErrorText + ERROR_LOCATION + LineEnding;
          EditLocationName.Color:= clYellow;
      end;
    end;

  procedure TFormChartsInput.ProcessLongitude;
    var
      CheckedLongitude: IValidatedLongitudeEndpoint;
      Direction: String;
    begin
      EditLongitudeDegrees.Color:= clDefault;
      EditLongitudeMinutes.Color:= clDefault;
      EditLongitudeSeconds.Color:= clDefault;
      if RadioButtonLongitudeEast.Checked then Direction:= LONG_EAST else Direction := LONG_WEST;
      CheckedLongitude:= FactoryValidatedLongitudeEndpoint.GetInstance;
      if EditLongitudeSeconds.Text = '' then EditLongitudeSeconds.Text := EMPTY_SECONDS;
      Longitude := CheckedLongitude.HandleInput(EditLongitudeDegrees.Text,
                                                EditLongitudeMinutes.Text,
                                                EditLongitudeSeconds.Text,
                                                Direction);
      if not Longitude.IsValid then begin
        ErrorFlag := True;
        ErrorText := ErrorText + ERROR_LONGITUDE + LineEnding;
        EditLongitudeDegrees.Color:= clYellow;
        EditLongitudeMinutes.Color:= clYellow;
        EditLongitudeSeconds.Color:= clYellow;
      end;
    end;


  procedure TFormChartsInput.ProcessLatitude;
    var
      CheckedLatitude: IValidatedLatitudeEndpoint;
      Direction: String;
    begin
      EditLatitudeDegrees.Color:= clDefault;
      EditLatitudeMinutes.Color:= clDefault;
      EditLatitudeSeconds.Color:= clDefault;
      if RadioButtonLatitudeNorth.Checked then Direction := LAT_NORTH
      else Direction := LAT_SOUTH;
      CheckedLatitude:=FactoryValidatedLatitudeEndpoint.GetInstance;
      if EditLatitudeSeconds.Text = '' then EditLatitudeSeconds.Text := EMPTY_SECONDS;
      Latitude := CheckedLatitude.HandleInput(EditLatitudeDegrees.Text,
                                              EditLatitudeMinutes.Text,
                                              EditLatitudeSeconds.Text,
                                              Direction);
      if not Latitude.IsValid then begin
        ErrorFlag := True;
        ErrorText := ErrorText + ERROR_LATITUDE + LineEnding;
        EditLatitudeDegrees.Color:= clYellow;
        EditLatitudeMinutes.Color:= clYellow;
        EditLatitudeSeconds.Color:= clYellow;
      end;
    end;

  procedure TFormChartsInput.ProcessDate;
    var
      DateEndpoint: IValidatedDateEndpoint;
      Calendar: String;
    begin
      EditDateDay.Color := clDefault;
      EditDateMonth.Color := clDefault;
      EditDateYear.Color := clDefault;
      if RadioButtonCalendarGregorian.Checked then Calendar:= CAL_GREG
      else Calendar:= CAL_JUL;
      DateEndpoint:= FactoryValidatedDateEndpoint.GetInstance;
      Date:=DateEndpoint.HandleInput(EditDateYear.Text,
                                    EditDateMonth.Text,
                                    EditDateDay.Text,
                                    Calendar);
      if not Date.Valid then  begin
        ErrorFlag := True;
        ErrorText := ErrorText + ERROR_DATE + LineEnding;
        EditDateDay.Color := clYellow;
        EditDateMonth.Color := clYellow;
        EditDateYear.Color := clYellow;
      end;
    end;

  procedure TFormChartsInput.ProcessTime;
    var
      TimeEndpoint: IValidatedTimeEndpoint;
    begin
      EditTimeHour.Color:=clDefault;
      EditTimeMinute.Color:=clDefault;
      EditTimeSecond.Color:=clDefault;
      TimeEndpoint:= FactoryValidatedTimeEndpoint.GetInstance;
      if EditTimeSecond.Text = '' then EditTimeSecond.Text := EMPTY_SECONDS;
      Time:=TimeEndpoint.HandleInput(EditTimeHour.Text,
                                     EditTimeMinute.Text,
                                     EditTimeSecond.Text);
      if not Time.Valid then begin
        ErrorFlag := True;
        ErrorText := ErrorText + ERROR_TIME + LineEnding;
        EditTimeHour.Color := clYellow;
        EditTimeMinute.Color := clYellow;
        EditTimeSecond.Color := clYellow;
      end;
    end;



  procedure TFormChartsInput.Btn_CalculateClick;
    var
      Location: IValidatedLocation;
      i, PositionType, Ayanamsha: Integer;
      Objects: TIntArray;
      ReferenceFrame: IReferenceFrame;
      HouseSystem: IHouseSystemSpec;
    begin
      LabelStatus.Color:= StringToColor(BG_COLOR);
      LabelStatus.Caption:= INSTRUCTION;
      ErrorFlag := False;
      ErrorText := EMPTY_STRING;
      ProcessNameDescription;
      ProcessLocationName;
      ProcessLongitude;
      ProcessLatitude;
      ProcessDate;
      ProcessTime;

      if ErrorFlag then begin
        LabelStatus.Color:= clYellow;
        LabelStatus.Caption:= ErrorText;
      end else begin
          {TODO: R_0.6 derive values that are used in Calculationsettings from preferences and input }
        ReferenceFrame := TReferenceFrame.Create(false, false, false, false);
        HouseSystem:= THouseSystemSpec.Create('Placidus', 'P', 'Placidus', 1, 12, true, true);
        PositionType:= 1;
        Ayanamsha:= 0;
        SetLength(Objects, 12);
        for i:= 0 to 10 do Objects[i]:= i;     // Sun up to Mean Node
        Objects[11]:= 15;                    // Cheiron
        CalculationSettings := TCalculationSettings.Create(PositionType, Ayanamsha, Objects, ReferenceFrame, HouseSystem);
        Location:= TValidatedLocation.Create(LocationName, Longitude.GetValue, Latitude.GetValue);
        FullChartRequest := TFullChartRequest.Create(NameDescription, Location, Date, Time, CalculationSettings);
        ReadyForCalc:= true;
        close;
      end;
    end;

  procedure TFormChartsInput.Btn_CancelClick;
    begin
      Close;
    end;

  procedure TFormChartsInput.FormCreate;
    begin
      ReadyForCalc:= false;
      LabelStatus.Color:= StringToColor(BG_COLOR);
      LabelStatus.Caption:= INSTRUCTION;
    end;


end.

