{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit UiChartsDrawing;
{< Graphic methods to draw a chart. }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, BGRABitmapTypes, BGRABitmap, Dialogs,
  XChartsDomain, UiChartsUtils, UiChartsUtilsFactories, UiSharedDomain, UiSharedUtils, UiSharedFormattedPositions;

type

  { TFormChartsDrawing }

  { Draws a circular chart which is fully resizable. @br
  - Created: 2019-02-02 @br
  - Last update: 2019-03-16 }
  TFormChartsDrawing = class(TForm)
    procedure FormCreate;
    procedure FormPaint;
    procedure DrawCircles;
    procedure DrawSigns;
    procedure DrawHouses;
    procedure DrawBodies;
    procedure DrawDegrees;
    procedure DrawSingleCusp(PWidth: Single; PAngle: Double);
    procedure DrawSignsSeparator(POffset: Integer);
    procedure DrawSignsGlyphs(POffset: Integer; PGlyph: String);
    procedure DrawLine(x1, y1, x2, y2, w: Single; c: TBGRAPixel);
    procedure DrawCircle(x, y, r, w: Single);
    procedure DrawGlyph(x, y: Single; Glyph: String; GlyphColor: TBGRAPixel);
    procedure SetChart(PChart: IFullChartResponse);
    procedure ShowCuspPosition(PPosition, PAngle: Double);
    procedure SetGlyphFont;
    procedure SetTextfont;
    procedure DefineGlyphsForSigns;
    procedure DefineCelestialObjectNames;
    function DefinePoint(PRadius: Integer; POffset: Real): TPoint;


    private
      ColorCircle: TBGRAPixel;
      ColorCusps: TBGRAPixel;
      ColorBodyGlyphs: TBGRAPixel;
      ColorSignGlyphs: TBGRAPixel;
      ColorConnectLines: TBGRAPixel;
      ColorPositions: TBGRAPixel;
      image: TBGRABitmap;
      UiCalc: IUiCalculations;
      DegreeMinuteSign: IDegreeMinuteSign;
      AngleAsc, AngleMc, OffsetAsc: Integer;
      Metrics: IChartDrawing2DMetrics;
      MetricsFactory: ChartDrawing2DMetricsFactory;
      PercZodiacCircle, PercHousesCircle, PercDegreesCircle, PercAspectsCircle, PercSignsCircle, PercPlanetsCircle,
        PercConnectLinesCircle, PercPositionBodyCircle, PercPositionHouseCircle: Integer;
      Chart: IFullChartResponse;
      SignGlyphs, CelestialObjectNames: Array of String;
      cuspAngles: Array of Real;
      AllBodyPositions: TArrayGraphicBodyPositions;
      StartPoint, EndPoint, GlyphPoint: TPoint;
      x1, y1, x2, y2: Single;

    public

  end;

var
  FormChartsDrawing: TFormChartsDrawing;

implementation

  {$R *.lfm}

  Uses
    Math, UiSharedDomainImpl, UiSharedUtilsImpl,
    UiSharedFormattedPositionsImpl;

  { TFormChartsDrawing }

  procedure TFormChartsDrawing.FormCreate;
  begin
    PercZodiacCircle:= 92;
    PercSignsCircle:= 86;
    PercPositionHouseCircle:=  86;
    PercHousesCircle:= 80;
    PercDegreesCircle:= 79;
    PercPositionBodyCircle:= 70;
    PercPlanetsCircle:= 60;
    PercConnectLinesCircle:= 54;
    PercAspectsCircle:= 46;
    ColorCircle := ColorToBGRA(ColorToRGB(clBlue));
    ColorCusps:= ColorToBGRA(ColorToRGB(clLtGray));
    ColorBodyGlyphs:= ColorToBGRA(ColorToRGB(clBlack));
    ColorSignGlyphs:= ColorToBGRA(ColorToRGB(clLtGray));
    ColorConnectLines:= ColorToBGRA(ColorToRGB(clGreen));
    ColorPositions:= ColorToBGRA(ColorToRGB(TColor($001a4d)));
    UiCalc:= TUiCalculations.Create;
    MetricsFactory:= ChartDrawing2DMetricsFactory.Create();
    Metrics:= MetricsFactory.GetInstance(Width, Height, PercZodiacCircle, PercHousesCircle, PercDegreesCircle,
              PercAspectsCircle, PercSignsCircle, PercPlanetsCircle, PercConnectLinesCircle, PercPositionBodyCircle,
              PercPositionHouseCircle);
  end;

  procedure TFormChartsDrawing.FormPaint;
    begin
      image := TBGRABitmap.Create(ClientWidth,ClientHeight,ColorToBGRA(ColorToRGB(clWhite)));
      Metrics.Resize(Width, Height);
      DrawDegrees;
      DrawCircles;
      DrawSigns;
      DrawHouses;
      DrawBodies;
      image.Draw(Canvas,0,0,True);
      image.free;
    end;



  procedure TFormChartsDrawing.DrawCircles;
    var
      x, y: Integer;
    begin
      x:= Metrics.CenterX;
      y:= Metrics.CenterY;
      DrawCircle(x, y, Metrics.ZodiacCircleRadius, 2);
      DrawCircle(x, y, Metrics.HousesCircleRadius, 2);
      DrawCircle(x, y, Metrics.AspectCircleRadius, 1);
    end;

  procedure TFormChartsDrawing.DrawSigns;
    var
      SignNr, Angle, i: Integer;
    begin
      SignNr:= AngleAsc div 30;
      for i:= 0 to 11 do begin
        Angle:= UiCalc.Range360(-(i * 30 - OffsetAsc));
        DrawSignsSeparator(Angle);
      end;
      DefineGlyphsForSigns;
      SetGlyphFont;
      for i:= 0 to 11 do begin
         Angle:= UiCalc.Range360(-(i * 30 - OffsetAsc) - 15);
         DrawSignsGlyphs(Angle, SignGlyphs[SignNr]);
         Inc(SignNr);
         if (SignNr > 11) then SignNr := 0;
      end;
    end;

  procedure TFormChartsDrawing.DrawDegrees;
    var
      i, StartAngle, DegreeAngle, ActualAngle, Additional: Integer;
    begin
      StartAngle:= UiCalc.Range360( OffsetAsc - 15);
      for i:= 0 to 359 do begin
        DegreeAngle:= StartAngle + i;
        ActualAngle:= UiCalc.Range360(DegreeAngle);
        Additional:= 0;
        if (i mod 5 = 0) then Additional:= 5;
        StartPoint:= DefinePoint(UiCalc.MakeEven(Metrics.DegreesCircleRadius - Additional), ActualAngle);
        Endpoint:= DefinePoint(UiCalc.MakeEven(Metrics.HousesCircleRadius), ActualAngle);
        x1:= StartPoint.x;
        y1:= StartPoint.y;
        x2:= EndPoint.x;
        y2:= EndPoint.y;
        DrawLine(x1,y1,x2,y2,1, ColorCusps);
      end;
    end;

  procedure TFormChartsDrawing.DrawHouses;
    var
      McAngle, IcAngle, AscAngle, DescAngle, ascLongitude: Real;
      i: Integer;
    begin
      SetTextfont;
      SetLength(cuspangles, Length(Chart.Houses.Cusps));
      ascLongitude:= Chart.Houses.Asc.Longitude;
      AscAngle:= 0.0;
      DescAngle:= 180;
      McAngle:= UiCalc.Range360(-(Chart.Houses.Mc.Longitude - ascLongitude));
      IcAngle:= UiCalc.Range360(McAngle + 180);
      for i:= 1 to 12 do begin              // index for cusps starts with 1
        cuspAngles[i]:= UiCalc.Range360(-(Chart.Houses.Cusps[i].Longitude - ascLongitude));
      end;
      DrawSingleCusp(3, AscAngle);
      ShowCuspPosition(Chart.Houses.Asc.Longitude, AscAngle);
      DrawSingleCusp(3, DescAngle);
      ShowCuspPosition(UiCalc.Range360(Chart.Houses.Asc.Longitude + 180), DescAngle);
      DrawSingleCusp(3, McAngle);
      ShowCuspPosition(Chart.Houses.Mc.Longitude, McAngle);
      DrawSingleCusp(3, IcAngle);
      ShowCuspPosition(UiCalc.Range360(Chart.Houses.Mc.Longitude + 180), IcAngle);
      for i:= 1 to 12 do begin
        if not((Chart.Houses.HouseSystemSpec.QuadrantSystem) and (i in [1,4,7,10])) then begin
          DrawSingleCusp(1, cuspAngles[i]);
          ShowCuspPosition(Chart.Houses.Cusps[i].Longitude, cuspAngles[i]);
        end;
      end;
    end;

  procedure TFormChartsDrawing.DrawSingleCusp(PWidth: Single; PAngle: Double);
    begin
      StartPoint:= DefinePoint(UiCalc.MakeEven(Metrics.AspectCircleRadius), PAngle);
      Endpoint:= DefinePoint(UiCalc.MakeEven(Metrics.HousesCircleRadius), PAngle);
      x1:= StartPoint.x;
      y1:= StartPoint.y;
      x2:= EndPoint.x;
      y2:= EndPoint.y;
      DrawLine(x1, y1, x2, y2, PWidth, ColorCusps);
    end;

  procedure TFormChartsDrawing.DrawLine(x1, y1, x2, y2, w: Single; c: TBGRAPixel);
    begin

      image.DrawLineAntialias(x1, y1, x2, y2, c, w, false);
    end;

  procedure TFormChartsDrawing.DrawCircle(x, y, r, w: Single);
    begin
      image.EllipseAntialias(x, y, r, r, ColorCircle, w);
    end;

  procedure TFormChartsDrawing.DrawGlyph(x, y: Single; Glyph: String; GlyphColor: TBGRAPixel);
    begin
      image.TextOut(x, y, Glyph, GlyphColor);
    end;

  procedure TFormChartsDrawing.DrawSignsSeparator(POffset: Integer);
    begin
      StartPoint:= DefinePoint(UiCalc.MakeEven(Metrics.HousesCircleRadius), POffset);
      EndPoint:= DefinePoint(UiCalc.MakeEven(Metrics.ZodiacCircleRadius), POffset);
      DrawLine(StartPoint.x, StartPoint.y, Endpoint.x, Endpoint.y, 1, ColorCircle);
    end;

  procedure TFormChartsDrawing.DrawSignsGlyphs(POffset: Integer; PGlyph: String);
    var
      XOffset, YOffset: Integer;
    begin
      XOffset:= Metrics.GlyphXOfset;
      YOffset:= Metrics.GlyphYOfset;
      GlyphPoint:= DefinePoint(UiCalc.MakeEven(Metrics.SignsCircleRadius), POffset);
      DrawGlyph(GlyphPoint.x + XOffset, GlyphPoint.y + YOffset, PGlyph, ColorSignGlyphs);
    end;

  procedure TFormChartsDrawing.DrawBodies;
    var
      i, BodyIndex, NrOfObjects: Integer;
      GraphicBodyPosition: IGraphicBodyPosition;
      ObjectId: Integer;
      XOffset, YOffset: Integer;
      ConnectPointCircle, ConnectPointGlyph, PositionPoint: TPoint;
      Longitude, MundanePos: Double;
      Glyph: String;
      SortedPositions: ISortedGraphicBodyPositions;
    begin
      XOffset:= Metrics.GlyphXOfset;
      YOffset:= Metrics.GlyphYOfset;
      SetGlyphFont;
      DefineCelestialObjectNames;
      NrOfObjects:= Length(Chart.AllObjects);
      SetLength(AllBodyPositions, NrOfObjects);
      for i:= 1 to NrOfObjects do begin
        ObjectId:= Chart.AllObjects[i-1].ObjectId;
        Longitude:= Chart.AllObjects[i-1].EclipticalPos.MainPos;
        MundanePos:= UiCalc.Range360(Chart.Houses.Asc.Longitude - Longitude);
        GraphicBodyPosition:= TGraphicBodyPosition.Create(ObjectId, Longitude, MundanePos);
        AllBodyPositions[i-1]:= GraphicBodyPosition;
      end;
      SortedPositions:= TSortedGraphicBodyPositions.Create(AllBodyPositions, 8.0);
      SetGlyphFont;
      for i:= 1 to NrOfObjects do begin        // handle glyphs
        BodyIndex:= SortedPositions.GetPositions[i-1].BodyIndex;
        Glyph:= CelestialObjectNames[BodyIndex];
        GlyphPoint:= DefinePoint(UiCalc.MakeEven(Metrics.PlanetscircleRadius), Floor(SortedPositions.GetPositions[i-1].PlotPosition));
        ConnectPointGlyph:= DefinePoint(UiCalc.MakeEven(Metrics.ConnectLinesCircleRadius),Floor(SortedPositions.GetPositions[i-1].PlotPosition));
        ConnectPointCircle:= DefinePoint(UiCalc.MakeEven(Metrics.AspectCircleRadius), Floor(SortedPositions.GetPositions[i-1].MundanePosition));
        DrawGlyph(GlyphPoint.x + XOffset, GlyphPoint.y + YOffset, Glyph, ColorBodyGlyphs);
        DrawLine(ConnectPointGlyph.x, ConnectPointGlyph.y, ConnectPointCircle.x, ConnectPointCircle.y, 1, ColorConnectLines);
      end;
      SetTextfont;
      for i:= 1 to NrOfObjects do begin       // handle text for position and connectline
        DegreeMinuteSign := TDegreeMinuteSign.Create(SortedPositions.GetPositions[i-1].EclipticPosition);
        PositionPoint:= DefinePoint(Metrics.PositionBodyCircleRadius, Floor(SortedPositions.GetPositions[i-1].PlotPosition));
        image.TextOut(PositionPoint.x + XOffset, POsitionPoint.y + YOffset, DegreeMinuteSign.DegreesMinutes, ColorPositions);
      end;
    end;

  procedure TFormChartsDrawing.SetChart(PChart: IFullChartResponse);
    begin
      Chart:= PChart;
      Caption:= Chart.Name;
      AngleMc:= round(Chart.GetHouses.Mc.Longitude);
      AngleAsc:= round(Chart.GetHouses.Asc.Longitude);
      OffsetAsc:= AngleAsc Mod 30;
    end;

 
  procedure TFormChartsDrawing.ShowCuspPosition(PPosition, PAngle: Double);
    var
      PositionPoint: TPoint;
    begin
      DegreeMinuteSign := TDegreeMinuteSign.Create(PPosition);
      PositionPoint:= DefinePoint(Metrics.PositionHouseCircleRadius, Floor(PAngle));
      image.TextOut(PositionPoint.x - 12, PositionPoint.y -12, DegreeMinuteSign.DegreesMinutes, ColorPositions);
    end;

  procedure TFormChartsDrawing.SetGlyphFont;
    begin
      image.FontName:= 'EnigmaAstrology';
      image.FontHeight:= Metrics.GetFontSizeGlyphs;
    end;

  procedure TFormChartsDrawing.SetTextfont;
    begin
      image.FontName:= 'Arial';
      image.FontHeight:= Metrics.FontSizeText;
    end;

  procedure TFormChartsDrawing.DefineGlyphsForSigns;
    begin
      { TODO: R_0.6 Replace with values from endpoint for GlyphsConfiguration }
      SetLength(SignGlyphs, 12);
      SignGlyphs[0]:='1';
      SignGlyphs[1]:='2';
      SignGlyphs[2]:='3';
      SignGlyphs[3]:='4';
      SignGlyphs[4]:='5';
      SignGlyphs[5]:='6';
      SignGlyphs[6]:='7';
      SignGlyphs[7]:='8';
      SignGlyphs[8]:='9';
      SignGlyphs[9]:='0';
      SignGlyphs[10]:='-';
      SignGlyphs[11]:='=';
    end;

  procedure TFormChartsDrawing.DefineCelestialObjectNames;
    begin
      { TODO: R_0.6 Replace with values from endpoint for GlyphsConfiguration. This is a code duplicate from ChartsMain. }
      SetLength(CelestialObjectNames, 16);
      CelestialObjectNames[0]:='a';
      CelestialObjectNames[1]:='b';
      CelestialObjectNames[2]:='c';
      CelestialObjectNames[3]:='d';
      CelestialObjectNames[4]:='f';
      CelestialObjectNames[5]:='g';
      CelestialObjectNames[6]:='h';
      CelestialObjectNames[7]:='i';
      CelestialObjectNames[8]:='j';
      CelestialObjectNames[9]:='k';
      CelestialObjectNames[10]:='{';
      CelestialObjectNames[11]:='}';
      CelestialObjectNames[12]:='e';
      CelestialObjectNames[13]:='e';
      CelestialObjectNames[14]:='e';
      CelestialObjectNames[15]:='w';
    end;

  function TFormChartsDrawing.DefinePoint(PRadius: Integer; POffset: Real): TPoint;
    var
      RectTriangle: IRectTriangle;
    begin
      RectTriangle:= TRectTriangle.Create(Metrics.CenterX, Metrics.CenterY, PRadius, POffset);
      Result:= RectTriangle.XYPoint;
    end;

end.

