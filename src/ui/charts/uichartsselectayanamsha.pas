{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit UiChartsSelectAyanamsha;
{< Selection form for ayanamsha's. }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, XChartsEndpoints, XChartsDomain;

type

  { TFormSelectAyanamsha }

  TFormSelectAyanamsha = class(TForm)
    Button1: TButton;
    ButtonOK: TButton;
    LabelTitle: TLabel;
    ListBoxAyanamshas: TListBox;
    MemoDetails: TMemo;
    StaticText1: TStaticText;
    procedure ButtonOKClick;
    procedure FormCreate;
    procedure FormShow;
    procedure ListBoxAyanamshasClick;
  private
    AllAyanamshas: TAyanamshaDtoArray;
    Endpoint: IAyanamshaDataEndpoint;
    AyanamshasList: TStringList;
    IndexList: Array of Integer;
    SelectedIndex: Integer;
    SelectedName: String;
    procedure RetrieveAyanamshas;
    procedure ShowDetails;
  public
    function GetSelectedIndex: Integer;
    function GetSelectedName: String;
  end;

var
  FormSelectAyanamsha: TFormSelectAyanamsha;

implementation

{$R *.lfm}

uses
  XChartsEndpointsFactories;

  { TFormSelectAyanamsha }
  procedure TFormSelectAyanamsha.FormShow;
    begin
      RetrieveAyanamshas;
    end;

  procedure TFormSelectAyanamsha.ButtonOKClick;
  { TODO : R_0.6 check for default values if nothing has been selected }
    begin
      SelectedIndex:= AllAyanamshas[ListBoxAyanamshas.ItemIndex].Id;
      SelectedName:= AllAyanamshas[ListBoxAyanamshas.ItemIndex].Name;
      Close;
    end;

  procedure TFormSelectAyanamsha.FormCreate;
  begin

  end;


  procedure TFormSelectAyanamsha.ListBoxAyanamshasClick;
  begin
    ShowDetails;
  end;

  procedure TFormSelectAyanamsha.RetrieveAyanamshas;
    var
      i, Count: Integer;
      Dto: IAyanamshaDto;
    begin
      Endpoint:= TFactoryAyanamshaDataEndpoint.Create.GetInstance;
      AllAyanamshas:= Endpoint.AllAyanamshas;
      Count:= Length(AllAyanamshas);
      AyanamshasList:=TStringList.Create;
      SetLength(IndexList, Count);
      for i:= 0 to Count -1 do begin
        Dto:= AllAyanamshas[i];
        AyanamshasList.Add(Dto.Name);
        IndexList[i]:= Dto.Id;
      end;
      ListBoxAyanamshas.Clear;
      ListBoxAyanamshas.Items.Assign(AyanamshasList);
    end;


  procedure TFormSelectAyanamsha.ShowDetails;
    var
      Index: Integer;
      Dto: IAyanamshaDto;
    begin
      Index:= ListBoxAyanamshas.ItemIndex;
      if (index >= 0) then begin
        Dto:= AllAyanamshas[index];
        MemoDetails.Clear;
        MemoDetails.Append(Dto.Description);
      end;
    end;

  function TFormSelectAyanamsha.GetSelectedIndex: Integer;
    begin
      Result:= SelectedIndex;
    end;

  function TFormSelectAyanamsha.GetSelectedName: String;
    begin
      Result:= SelectedName;
    end;

end.


