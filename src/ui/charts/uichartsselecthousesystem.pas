{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit UiChartsSelectHouseSystem;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, RTTICtrls, RTTIGrids, Forms, Controls, Graphics,
  Dialogs, StdCtrls, ActnList, XChartsEndpoints, XChartsDomain;

type

  { TFormSelectHouseSystem }

  TFormSelectHouseSystem = class(TForm)
    ButtonOK: TButton;
    ButtonCancel: TButton;
    LabelTitle: TLabel;
    ListBoxHouseSystems: TListBox;
    MemoDetails: TMemo;
    TextMakeSelection: TStaticText;
    procedure ButtonCancelClick;
    procedure ButtonOKClick;
    procedure FormShow;
    procedure ListBoxHouseSystemsClick;
  private
    AllSystems: THouseSystemDtoArray;
    Endpoint: IHouseSystemDataEndpoint;
    HousesList: TStringList;
    IndexList: Array of Integer;
    SelectedIndex: Integer;
    SelectedName: String;
    procedure RetrieveSystems;
    procedure ShowDetails;
  public
    procedure SetCurrentIndex(CurrentIndex: Integer);
    function GetSelectedIndex: Integer;
    function GetSelectedName: String;
  end;

var
  FormSelectHouseSystem: TFormSelectHouseSystem;

implementation

{$R *.lfm}

uses
  XChartsEndpointsFactories;

  { TFormSelectHouseSystem }
  procedure TFormSelectHouseSystem.FormShow;
    begin
      RetrieveSystems;
    end;

  procedure TFormSelectHouseSystem.ButtonOKClick;
    begin
      SelectedIndex:= AllSystems[ListBoxHouseSystems.ItemIndex].Id;
      SelectedName:= AllSystems[ListBoxHouseSystems.ItemIndex].Name;
      Close;
    end;

  procedure TFormSelectHouseSystem.ButtonCancelClick;
    begin
      Close;
    end;

  procedure TFormSelectHouseSystem.ListBoxHouseSystemsClick;
    begin
      ShowDetails;
    end;

  procedure TFormSelectHouseSystem.RetrieveSystems;
    var
      i, Count, ActiveItem: Integer;
      Dto: IHouseSystemDto;
    begin
      Endpoint:= TFactoryHouseSystemDataEndpoint.Create.GetInstance;
      AllSystems:= Endpoint.AllSystems;
      Count:= Length(AllSystems);
      HousesList:=TStringList.Create;
      SetLength(IndexList, Count);
      for i:= 0 to Count -1 do begin
        Dto:= AllSystems[i];
        HousesList.Add(Dto.Name);
        IndexList[i]:= Dto.Id;
        if (Dto.Id = SelectedIndex) then  // save previous name
          begin
            SelectedName:= Dto.Name;
            ActiveItem:= i;
          end;
      end;
      ListBoxHouseSystems.Clear;
      ListBoxHouseSystems.Items.Assign(HousesList);
      ListBoxHouseSystems.ItemIndex:= ActiveItem;
    end;

  procedure TFormSelectHouseSystem.ShowDetails;
    var
      Index: Integer;
      Dto: IHouseSystemDto;
      YesNo: String;
    begin
      Index:= ListBoxHouseSystems.ItemIndex;
      if (index >= 0) then begin
        Dto:= AllSystems[index];
        MemoDetails.Clear;
        MemoDetails.Append(Dto.Description);
        MemoDetails.Append('');
        MemoDetails.Append('Number of houses : ' + Dto.NrOfHouses.toString);
        if (Dto.QuadrantSystem) then YesNo:= 'Yes' else YesNo:= 'No';
        MemoDetails.Append('Quadrant system : ' + YesNo);
        if (Dto.CounterClockWise) then YesNo:= 'Yes' else YesNo:= 'No';
        MemoDetails.Append('Counter Clockwise : ' + YesNo);
        if (Dto.CuspIsStart) then YesNo:= 'Yes' else YesNo:= 'No';
        MemoDetails.Append('Cusp is start of house : ' + YesNo);
      end;
    end;

  function TFormSelectHouseSystem.GetSelectedIndex: Integer;
    begin
      Result:= SelectedIndex;
    end;

  function TFormSelectHouseSystem.GetSelectedName: String;
    begin
      Result:= SelectedName;
    end;

  procedure TFormSelectHouseSystem.SetCurrentIndex(CurrentIndex: Integer);
    begin
      SelectedIndex:= CurrentIndex;
    end;

end.

