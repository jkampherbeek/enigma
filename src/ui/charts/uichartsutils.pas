{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit uichartsutils;
{< Utils for drawing charts.}
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics;

type

  { Metrics for a two dimensional chart.@br
  - Factory: ChartDrawing2DMetricsFactory @br
  - Created: 2019-02-25 @br
  - Last update: 2019-04-03   }
  IChartDrawing2DMetrics  = Interface ['{6B7838E1-910B-4305-AF26-3F74029EEB6F}']
    procedure Resize(PNewWidth, PNewHeight: Integer);
    function GetAspectCircleRadius: Integer;
    function GetHousesCircleRadius: Integer;
    function GetDegreesCircleRadius: Integer;
    function GetSignsCircleRadius: Integer;
    function GetZodiacCircleRadius: Integer;
    function GetPlanetsCircleRadius: Integer;
    function GetConnectLinesCircleRadius: Integer;
    function GetPositionBodyCircleRadius: Integer;
    function GetPositionHouseCircleRadius: Integer;
    function GetCenterX: Integer;
    function GetCenterY: Integer;
    function GetFontSizeGlyphs: Integer;
    function GetFontSizeText: Integer;
    function GetGlyphXOffset: Integer;
    function GetGlyphYOffset: Integer;

    property AspectCircleRadius: Integer read GetAspectCircleRadius;
    property HousesCircleRadius: Integer read GetHousesCircleRadius;
    property SignsCircleRadius: Integer read GetSignsCircleRadius;
    property DegreesCircleRadius: Integer read GetDegreesCircleRadius;
    property ZodiacCircleRadius: Integer read GetZodiacCircleRadius;
    property PlanetscircleRadius: Integer read GetPlanetsCircleRadius;
    property ConnectLinesCircleRadius: Integer read GetConnectLinesCircleRadius;
    property PositionBodyCircleRadius: Integer read GetPositionBodyCircleRadius;
    property PositionHouseCircleRadius: INteger read GetPositionHouseCircleRadius;
    property CenterX: Integer read GetCenterX;
    property CenterY: Integer read GetCenterY;
    property FontSizeGlyphs: Integer read GetFontSizeGlyphs;
    property FontSizeText: Integer read GetFontSizeText;
    property GlyphXOfset: Integer read GetGlyphXOffset;
    property GlyphYOfset: Integer read GetGlyphYOffset;

  end;


implementation

end.

