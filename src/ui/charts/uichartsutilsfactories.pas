unit UiChartsUtilsFactories;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UiChartsUtils, UiSharedUtils;

type
  ChartDrawing2DMetricsFactory = Class
    private
      UiCalculations: IUiCalculations;
    public
      function GetInstance(PInitialWidth, PInitialHeight, PPercZodiacCircle, PPercHousesCircle,
                           PPercDegreesCircle, PPercAspectsCircle, PPercSignsCircle, PPercPlanetsCircle,
                           PPercConnectLinesCircle, PPercPositionBodyCircle,
                           PPercPositionHouseCircle: Integer): IChartDrawing2DMetrics;
  end;

implementation

uses
  UiChartsUtilsImpl, UiSharedUtilsImpl;

  { ChartDrawing2DMetricsFactory }
  function ChartDrawing2DMetricsFactory.GetInstance(PInitialWidth, PInitialHeight, PPercZodiacCircle, PPercHousesCircle,
                         PPercDegreesCircle, PPercAspectsCircle, PPercSignsCircle, PPercPlanetsCircle,
                         PPercConnectLinesCircle, PPercPositionBodyCircle,
                         PPercPositionHouseCircle: Integer): IChartDrawing2DMetrics;
    begin
      UiCalculations:= TUiCalculations.Create();
      Result:= TChartDrawing2DMetrics.Create(UiCalculations, PInitialWidth, PInitialHeight, PPercZodiacCircle,
               PPercHousesCircle, PPercDegreesCircle, PPercAspectsCircle, PPercSignsCircle, PPercPlanetsCircle,
               PPercConnectLinesCircle, PPercPositionBodyCircle, PPercPositionHouseCircle );
    end;

end.

