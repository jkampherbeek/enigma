{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit UiChartsSelectLov;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  XChartsEndpoints, XChartsDomain;

type

  { TFormSelectLov }

  TFormSelectLov = class(TForm)
    ButtonCancel: TButton;
    ButtonOK: TButton;
    LabelSelections: TLabel;
    LabelValues: TLabel;
    ListBoxLovs: TListBox;
    MemoDetails: TMemo;

    procedure ButtonOKClick;
    procedure FormShow;
    procedure ListBoxLovsClick;
  private
    AllItems: TLookupValueDtoArray;
    Endpoint: ILookupValueDataEndpoint;
    ItemsList: TStringList;
    IndexList: Array of Integer;
    SelectedIndex: Integer;
    SelectedName: String;
    TableName: String;
    procedure RetrieveItems;
    procedure ShowDetails;
  public
    function GetSelectedIndex: Integer;
    function GetSelectedName: String;
    procedure SetTableName(PTableName: String);
  end;

var
  FormSelectLov: TFormSelectLov;

implementation

{$R *.lfm}

uses
  XChartsEndpointsFactories;

{ TFormSelectLov }


  procedure TFormSelectLov.FormShow;
    begin
      RetrieveItems;
    end;

  procedure TFormSelectLov.ListBoxLovsClick;
    begin
      ShowDetails;
    end;

  procedure TFormSelectLov.ButtonOKClick;
  { TODO : R_0.6 check for default values if nothing has been selected }
    begin
      SelectedIndex:= AllItems[ListBoxLovs.ItemIndex].Id;
      SelectedName:= AllItems[ListBoxLovs.ItemIndex].Name;
      Close;
    end;

  procedure TFormSelectLov.RetrieveItems;
    var
      i, Count: Integer;
      Dto: ILookUpValueDto;
    begin
      Endpoint:= TFactoryLookupValueDataEndpoint.Create(TableName).GetInstance;
      AllItems:= Endpoint.AllItems;
      Count:= Length(AllItems);
      ItemsList:=TStringList.Create;
      SetLength(IndexList, Count);
      for i:= 0 to Count -1 do begin
        Dto:= AllItems[i];
        ItemsList.Add(Dto.Name);
        IndexList[i]:= Dto.Id;
      end;
      ListBoxLovs.Clear;
      ListBoxLovs.Items.Assign(ItemsList);
    end;

  procedure TFormSelectLov.ShowDetails;
    var
      Index: Integer;
      Dto: ILookUpValueDto;
    begin
      Index:= ListBoxLovs.ItemIndex;
      if (index >= 0) then begin
        Dto:= AllItems[index];
        MemoDetails.Clear;
        MemoDetails.Append(Dto.Description);
      end;
    end;

  function TFormSelectLov.GetSelectedIndex: Integer;
    begin
      Result:= SelectedIndex;
    end;

  function TFormSelectLov.GetSelectedName: String;
    begin
      Result:= SelectedName;
    end;

  procedure TFormSelectLov.SetTableName(PTableName: String);
    begin
      TableName:= PTableName;
    end;


end.


