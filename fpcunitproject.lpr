program fpcunitproject;

{$mode objfpc}{$H+}

uses
  Interfaces, Forms, UiSharedDomainImpl, GuiTestRunner,
  TestCaseEndpoints, TestCaseXchgDomain, testcasexchartsdomain,
  FakesXChartsDomain, TestCaseUiSharedDomain, TestCaseXSharedDomain,
  TestCaseXSharedEndpoints, TestCaseBeChartsDomain, TestCaseXSharedHandlers,
  TestCaseBeChartsHandlers, TestCaseXChartsEndpoints, FakesBeChartsDomain,
  FakesXSharedDomain, TestCaseBeDbDatabaseUpdater,
  DbTestUtils, TestCaseBeDbDao, FakesDao,
  FakesXChartsEndpoints, TestCaseUiChartsUtils, FakesUiChartsUtils,
  TestCaseUiSharedUtils, TestCaseUiSharedFormattedPositions, TestCaseUiChartsDataVault;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TGuiTestRunner, TestRunner);
  Application.Run;
end.

