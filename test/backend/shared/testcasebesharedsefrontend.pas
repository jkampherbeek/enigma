unit TestCaseBeSharedSeFrontend;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, BeSharedSeFrontend, BeChartsDomain, XChartsDomain, DomainModel,
  XSharedDomain, XSharedDomainImpl;

type

TestSeFrontend = class(TTestCase)
  published
    procedure TestCheckDateHappyFlowGregorian;
    procedure TestCheckDateErrorGregorian;
    procedure TestJulDayHappyFlow;
    procedure TestJulDayJulianCal;
    procedure TestJulDayBCE;
    procedure TestJulDayStart;
    procedure TestJulDayBeforeStart;
    procedure TestCalculateCelestialObject;
    procedure TestCalculateAzimuthAndAltitude;
    procedure TestCalculateCelestialObjectFull;
    procedure TestCalculateObliquity;
    procedure TestEcliptic2Equatorial;
    procedure TestCalculateHouses;
end;


implementation

uses
  XChartsDomainImpl, BeSwissDelphi;

const
  MARGIN = 0.000000001;

procedure TestSeFrontend.TestCheckDateHappyFlowGregorian;
var
  SeFrontend: TSeFrontend;
  Day, Month, Year: Integer;
  PCalendarGJ: String;
begin
  SeFrontend:= TSeFrontend.Create;
  Day:=23;
  Month:=7;
  Year:=2018;
  PCalendarGJ:='G';
  CheckTrue(SeFrontend.CheckDate(Day, Month, Year, PCalendarGJ));
end;

procedure TestSeFrontend.TestCheckDateErrorGregorian;
var
  SeFrontend: TSeFrontend;
  Day, Month, Year: Integer;
  PCalendarGJ: String;
begin
  SeFrontend:= TSeFrontend.Create;
  Day:=23;
  Month:=15;
  Year:=2018;
  PCalendarGJ:='G';
  CheckFalse(SeFrontend.CheckDate(Day, Month, Year, PCalendarGJ));
end;

procedure TestSeFrontend.TestJulDayHappyFlow;
var
  SeFrontend: TSeFrontend;
  Date: TValidatedDate;
  Time: TValidatedTime;
  Expected, Result: Double;
begin
  SeFrontend:= TSeFrontend.Create;
  Date:= TValidatedDate.Create(2000, 1, 1, 'G', true);
  Time:= TValidatedTime.Create(12.0, true);
  Expected:= 2451545.0;
  Result:= SeFrontend.JulDay(Date, Time);
  AssertEquals(Expected, Result, MARGIN);
end;

procedure TestSeFrontend.TestJulDayJulianCal;
var
  SeFrontend: TSeFrontend;
  Date: TValidatedDate;
  Time: TValidatedTime;
  Expected, Result: Double;
begin
  SeFrontend:= TSeFrontend.Create;
  Date:= TValidatedDate.Create(837, 4, 10, 'J', true);
  Time:= TValidatedTime.Create(7.2, true);
  Expected:= 2026871.8;
  Result:= SeFrontend.JulDay(Date, Time);
  AssertEquals(Expected, Result, MARGIN);
end;

procedure TestSeFrontend.TestJulDayBCE;
var
  SeFrontend: TSeFrontend;
  Date: TValidatedDate;
  Time: TValidatedTime;
  Expected, Result: Double;
begin
  SeFrontend:= TSeFrontend.Create;
  Date:= TValidatedDate.Create(-1000, 7, 12, 'J', true);
  Time:= TValidatedTime.Create(12.0, true);
  Expected:= 1356001.0;
  Result:= SeFrontend.JulDay(Date, Time);
  AssertEquals(Expected, Result, MARGIN);
end;

procedure TestSeFrontend.TestJulDayStart;
var
  SeFrontend: TSeFrontend;
  Date: TValidatedDate;
  Time: TValidatedTime;
  Expected, Result: Double;
begin
  SeFrontend:= TSeFrontend.Create;
  Date:= TValidatedDate.Create(-4712, 1, 1, 'J', true);
  Time:= TValidatedTime.Create(12.0, true);
  Expected:= 0.0;
  Result:= SeFrontend.JulDay(Date, Time);
  AssertEquals(Expected, Result, MARGIN);
end;

procedure TestSeFrontend.TestJulDayBeforeStart;
var
  SeFrontend: TSeFrontend;
  Date: TValidatedDate;
  Time: TValidatedTime;
  Expected, Result: Double;
begin
  SeFrontend:= TSeFrontend.Create;
  Date:= TValidatedDate.Create(-4713, 12, 31, 'J', true);
  Time:= TValidatedTime.Create(12.0, true);
  Expected:= -1.0;
  Result:= SeFrontend.JulDay(Date, Time);
  AssertEquals(Expected, Result, MARGIN);
end;


procedure TestSeFrontend.TestCalculateCelestialObject;
var
  SeFrontend: TSeFrontend;
  JulianDay, Expected, Calculated: Double;
  PlanetId: Integer;
  Flags: LongInt;
  CelObject: ICelestialObjectSimple;
begin
  { Results compared with SwissEph for 10 sept. 2018 20:00:18 UT }
  Flags:= SEFLG_SWIEPH or SEFLG_SPEED;
  SeFrontend:= TSeFrontend.Create;
  JulianDay:= 2458372.33354167;
  PlanetId := 3;    // Venus
  Expected := 211.0136487057;
  CelObject:= SeFrontend.CalculateCelestialObject(JulianDay, PlanetId, Flags);
  Calculated:=CelObject.MainPos;
  AssertEquals(Expected, Calculated, MARGIN);
end;

procedure TestSeFrontend.TestCalculateAzimuthAndAltitude;
var
  SeFrontend: TSeFrontend;
  Positions: Array[0..2] of Double;
  JulianDay, Expected, Calculated: Double;
  HorizontalPosition: IHorizontalPosition;
  CelObject: TCelestialObjectSimple;
  Location: IValidatedLocation;
begin
  { Testdata:
    Location 52N13 6E54
    Date 2018-Oct-01, Time 0:00:00 UT
    Julian Day for ET: 2458392.5
    Celestial Object: Venus
    Azimuth according to Horizons: 327.1088
    Altitude according to Horizons: -55.8339,

    already calculated:
    longitude: 10 SC 24'26"    latitude -06 gr 49 m 16 s
    220.4072222222   /  -6.8211111111



    volgens sf az 327 gr 06 m  alti  -55 gr 49'}
  SeFrontend:= TSeFrontend.Create;
  JulianDay:= 2458392.5;
  Positions[0]:= 220.4072222222;
  Positions[1]:= -6.8211111111;
  Positions[2]:= 0.5;   // not used
  Location:= TValidatedLocation.Create('TestLocation', 6.9, 52.2166667);
  CelObject:= TCelestialObjectSimple.Create(Positions);
  HorizontalPosition:= SeFrontend.CalculateAzimuthAndAltitude(JulianDay, Location, CelObject);
  Expected:= -55.81667;                   // - 55 gr 49 m       eigen calc -55.83008776
  Calculated:= HorizontalPosition.Altitude;
  AssertEquals(Expected, Calculated, MARGIN);
  Expected:= 147.1;                          //   327°06'
  Calculated:= HorizontalPosition.Azimuth;    //   0 gr is noord   SE 0 gr is zuid // eigen calc 147.10816175
  AssertEquals(Expected, Calculated, MARGIN);
end;

procedure TestSeFrontend.TestCalculateCelestialObjectFull;
var
  SeFrontend: TSeFrontend;
  CelObject: ICelestialObjectFull;
  JulianDay, Expected, Calculated: Double;
  Location: IValidatedLocation;
  Flags: Longint;
  PlanetId: Integer;
begin
  SeFrontend:= TSeFrontend.Create;
  Flags:= SEFLG_SWIEPH or SEFLG_SPEED;
  Location:= TValidatedLocation.Create('TestLocation', 6.9, 52.2166667);
  JulianDay:= 2458372.33354167;
  PlanetId := 3;    // Venus
  Expected := 211.0136487057;
  CelObject:= SeFrontend.CalculateCelestialObjectFull(JulianDay, PlanetId, Flags, Location);
  Calculated:=CelObject.EclipticalPos.GetMainPos;
  AssertEquals(Expected, Calculated, MARGIN);
  Expected := -15.9909943736503;
  Calculated:= CelObject.EquatorialPos.GetDeviationPos;
  AssertEquals(Expected, Calculated, MARGIN);
  Expected := -12.2770659359009;
  Calculated:= CelObject.HorizontalPos.GetDeviationPos;
  AssertEquals(Expected, Calculated, MARGIN);
end;

procedure TestSeFrontend.TestCalculateObliquity;
var
  SeFrontend: TSeFrontend;
  JulianDay, Expected, Calculated: Double;
begin
  SeFrontend:= TSeFrontend.Create;
  JulianDay:= 2458372.33354167;
  Expected:= 23.4368475480757;
  Calculated:= SeFrontend.CalculateObliquity(JulianDay);
  AssertEquals(Expected, Calculated, MARGIN);
end;


procedure TestSeFrontend.TestEcliptic2Equatorial;
var
  SeFrontend: TSeFrontend;
  Longitude, Latitude, Epsilon, ExpectedRA, ExpectedDecl: Double;
  Results: TDoubleArray;
begin
  SeFrontend:= TSeFrontend.Create;
  Longitude:= 100.0;
  Latitude:= 1.0;
  Epsilon := 23.447;
  Results:= SeFrontend.Ecliptic2Equatorial(Longitude, Latitude, Epsilon);
  ExpectedRA:= 100.798372648199;
  ExpectedDecl:= -22.0728126054502;
  CheckEquals(ExpectedRA, Results[0], MARGIN);
  CheckEquals(ExpectedDecl, Results[1], MARGIN);
end;

procedure TestSeFrontend.TestCalculateHouses;
var
  SeFrontend: TSeFrontend;
  JulianDay, Expected, Result, Calculated, Obliquity: Double;
  CalculatedHouses: IHousesResponse;
  HouseSystemSpec: IHouseSystemSpec;
  Location: IValidatedLocation;
begin
  SeFrontend:= TSeFrontend.Create;
  JulianDay:= 2458372.33354167;
  Obliquity:= 23.4368475480757;
  Location:= TValidatedLocation.Create('TestLocation', 6.9, 52.2166667);
  HouseSystemSpec:= THouseSystemSpec.Create('Placidus', 'P', 'Description', 1, 12, true, true);
  CalculatedHouses:= SeFrontend.CalculateHouses(JulianDay, Obliquity, Location, HouseSystemSpec);
  Expected:= 79.8572331912747;
  Result:= CalculatedHouses.Cusps[2].Longitude;
  CheckEquals(Expected, Result, MARGIN);
end;


initialization

  RegisterTest(TestSeFrontend);


end.

