unit TestCaseXSharedHandlers;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, XSharedHandlers, XSharedDomain;
type

TestDateValidationHandler = class(TTestCase)
published
  procedure TestHappyFlow;
  procedure TestHappyFlowBCE;
  procedure NotNumeric;
  procedure OutOfRange;
end;

TestTimeValidationHandler = class(TTestCase)
published
  procedure TestHappyFlow;
  procedure NotNumeric;
  procedure OutOfRange;
end;

TestLongitudeValidationHandler = class(TTestCase)
published
  procedure TestHappyFlow;
  procedure TestHappyFlowWest;
  procedure NotNumeric;
  procedure OutOfRange;
end;

TestLatitudeValidationHandler = class(TTestCase)
published
  procedure TestHappyFlow;
  procedure TestHappyFlowSouth;
  procedure NotNumeric;
  procedure OutOfRange;
end;

TestSexagesimalValidationHandler = class(TTestCase)
published
  procedure TestHappyFlow;
  procedure TestHappyFlowNegativeIndication;
  procedure TestHappyFlowNegativeValue;
  procedure TestErrorDHOutOfRange;
  procedure TestErrorMinutesOutOfRange;
  procedure TestErrorSecondsOutOfRange;
end;

TestFactorySexagesimalValidationHandler = class(TTestCase)
published
  procedure GetInstance;
end;


implementation

uses XSharedHandlersImpl, XSharedHandlersFactories, BeSharedSeFrontend;

const
  MARGIN = 0.000000001;

{ TestDateValidationHandler }

procedure TestDateValidationHandler.TestHappyFlow;
var
  Handler: IDateValidationHandler;
  CalculatedValue : IValidatedDate;
begin
  Handler := TDateValidationHandler.Create(TSeFrontend.Create);
  CalculatedValue := Handler.CalculateValue('1953','1','29','g');
  CheckEquals(1953, CalculatedValue.Year, 'Returned wrong value for year.');
  CheckEquals(1, CalculatedValue.Month, 'Returned wrong value for month.');
  CheckEquals(29, CalculatedValue.Day, 'Returned wrong value for day.');
  CheckTrue(CalculatedValue.Valid);
end;

procedure TestDateValidationHandler.TestHappyFlowBCE;
var
  Handler: IDateValidationHandler;
  CalculatedValue : IValidatedDate;
begin
  Handler := TDateValidationHandler.Create(TSeFrontend.Create);
  CalculatedValue := Handler.CalculateValue('-53','1','29','j');
  CheckEquals(-53, CalculatedValue.Year, 'Returned wrong value for year.');
  CheckEquals(1, CalculatedValue.Month, 'Returned wrong value for month.');
  CheckEquals(29, CalculatedValue.Day, 'Returned wrong value for day.');
  CheckTrue(CalculatedValue.Valid);
end;

procedure TestDateValidationHandler.NotNumeric;
var
  Handler: IDateValidationHandler;
  CalculatedValue : IValidatedDate;
begin
  Handler := TDateValidationHandler.Create(TSeFrontend.Create);
  CalculatedValue := Handler.CalculateValue('a','1','29','j');
  CheckEquals(0, CalculatedValue.Year, 'Returned wrong value for year while input was not numeric.');
  CheckEquals(1, CalculatedValue.Month, 'Returned wrong value for month while input was not numeric.');
  CheckEquals(1, CalculatedValue.Day, 'Returned wrong value for day while input was not numeric.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('1953','b','29','j');
  CheckEquals(0, CalculatedValue.Year, 'Returned wrong value for year while input was not numeric.');
  CheckEquals(1, CalculatedValue.Month, 'Returned wrong value for month while input was not numeric.');
  CheckEquals(1, CalculatedValue.Day, 'Returned wrong value for day while input was not numeric.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('1953','1','c','j');
  CheckEquals(0, CalculatedValue.Year, 'Returned wrong value for year while input was not numeric.');
  CheckEquals(1, CalculatedValue.Month, 'Returned wrong value for month while input was not numeric.');
  CheckEquals(1, CalculatedValue.Day, 'Returned wrong value for day while input was not numeric.');
  CheckFalse(CalculatedValue.Valid);
end;

procedure TestDateValidationHandler.OutOfRange;
var
  Handler: IDateValidationHandler;
  CalculatedValue : IValidatedDate;
begin
  Handler := TDateValidationHandler.Create(TSeFrontend.Create);
  CalculatedValue := Handler.CalculateValue('1953','13','29','g');
  CheckEquals(0, CalculatedValue.Year, 'Returned wrong value for year while month is Out of range.');
  CheckEquals(1, CalculatedValue.Month, 'Returned wrong value for month while month is Out of range.');
  CheckEquals(1, CalculatedValue.Day, 'Returned wrong value for day while month is Out of range.');
  CheckFalse(CalculatedValue.Valid);

  CalculatedValue := Handler.CalculateValue('1953','1','32','g');
  CheckEquals(0, CalculatedValue.Year, 'Returned wrong value for year while day is Out of range.');
  CheckEquals(1, CalculatedValue.Month, 'Returned wrong value for month while day is Out of range.');
  CheckEquals(1, CalculatedValue.Day, 'Returned wrong value for day while day is Out of range.');
  CheckFalse(CalculatedValue.Valid);
end;

{ TestTimeValidationHandler }

procedure TestTimeValidationHandler.TestHappyFlow;
var
  Handler: ITimeValidationHandler;
  CalculatedValue : IValidatedTime;
begin
  Handler := TTimeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('10','30','20');
  CheckEquals(10.505555555555556, CalculatedValue.DecimalTime, MARGIN, 'Returned wrong value for decimal time.');
  CheckTrue(CalculatedValue.Valid);
end;

procedure TestTimeValidationHandler.NotNumeric;
var
  Handler: ITimeValidationHandler;
  CalculatedValue : IValidatedTime;
begin
  Handler := TTimeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('a','30','20');
  CheckEquals(0, CalculatedValue.DecimalTime, 'Returned wrong value for decimal time while input was not numeric.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('10','b','20');
  CheckEquals(0, CalculatedValue.DecimalTime, 'Returned wrong value for decimal time while input was not numeric.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('10','30','c');
  CheckEquals(0, CalculatedValue.DecimalTime, 'Returned wrong value for decimal time while input was not numeric.');
  CheckFalse(CalculatedValue.Valid);
end;

procedure TestTimeValidationHandler.OutOfRange;
var
  Handler: ITimeValidationHandler;
  CalculatedValue : IValidatedTime;
begin
  Handler := TTimeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('25','30','20');
  CheckEquals(0, CalculatedValue.DecimalTime, 'Returned wrong value for hour while hour is Out of range.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('10','60','20');
  CheckEquals(0, CalculatedValue.DecimalTime, 'Returned wrong value for hour while hour is Out of range.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('10','30','-20');
  CheckEquals(0, CalculatedValue.DecimalTime, 'Returned wrong value for hour while hour is Out of range.');
  CheckFalse(CalculatedValue.Valid);
end;

{ TestLongitudeValidationHandler }

procedure TestLongitudeValidationHandler.TestHappyFlow;
var
  Handler: ILongitudeValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TLongitudeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('1','30','0','E');
  CheckEquals(1.5, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value.');
  CheckTrue(CalculatedValue.Valid);
end;

procedure TestLongitudeValidationHandler.TestHappyFlowWest;
var
  Handler: ILongitudeValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TLongitudeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('1','30','0','W');
  CheckEquals(-1.5, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value.');
  CheckTrue(CalculatedValue.Valid);
end;

procedure TestLongitudeValidationHandler.NotNumeric;
var
  Handler: ILongitudeValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TLongitudeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('a','30','10','E');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value for not-numeric degrees.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('1','b','10','E');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value for not-numeric minutes.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('1','2','c','E');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value for not-numeric second.');
  CheckFalse(CalculatedValue.Valid);
end;

procedure TestLongitudeValidationHandler.OutOfRange;
var
  Handler: ILongitudeValidationHandler;
  CalculatedValue :IValidatedDouble;
begin
  Handler := TLongitudeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('190','30','10','E');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value while degrees are out of range.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('1','60','10','E');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value while minutes are out of range.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('1','2','60','E');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value while seconds are out of range.');
  CheckFalse(CalculatedValue.Valid);
end;

{ TestLatitudeValidationHandler }

procedure TestLatitudeValidationHandler.TestHappyFlow;
var
  Handler: ILatitudeValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TLatitudeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('12','45','0','N');
  CheckEquals(12.75, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value.');
  CheckTrue(CalculatedValue.Valid);
end;

procedure TestLatitudeValidationHandler.TestHappyFlowSouth;
var
  Handler: ILatitudeValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TLatitudeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('12','45','0','S');
  CheckEquals(-12.75, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value.');
  CheckTrue(CalculatedValue.Valid);
end;

procedure TestLatitudeValidationHandler.NotNumeric;
var
  Handler: ILatitudeValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TLatitudeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('a','33','10','N');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value for not-numeric degrees.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('11','b','12','N');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value for not-numeric minutes.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('11','22','c','N');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value for not-numeric second.');
  CheckFalse(CalculatedValue.Valid);
end;

procedure TestLatitudeValidationHandler.OutOfRange;
var
  Handler: ILatitudeValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TLatitudeValidationHandler.Create;
  CalculatedValue := Handler.CalculateValue('90','30','10','N');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value while degrees are out of range.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('1','60','10','N');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value while minutes are out of range.');
  CheckFalse(CalculatedValue.Valid);
  CalculatedValue := Handler.CalculateValue('1','2','60','N');
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Returned wrong calculated value while seconds are out of range.');
  CheckFalse(CalculatedValue.Valid);
end;

{ TestSexagesimalValidationHandler }

procedure TestSexagesimalValidationHandler.TestHappyFlow;
var
  Handler: ISexagesimalValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TSexagesimalValidationHandler.Create(-23, 23);
  CalculatedValue := Handler.CalculateValue('8', '30', '0', false);
  CheckEquals(8.5, CalculatedValue.Value, MARGIN, 'Wrong calculatedvalue for InputSexagesimal');
  CheckTrue(CalculatedValue.Valid);
end;

procedure TestSexagesimalValidationHandler.TestHappyFlowNegativeIndication;
var
  Handler: ISexagesimalValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TSexagesimalValidationHandler.Create(-23, 23);
  CalculatedValue := Handler.CalculateValue('8', '30', '0', true);
  CheckEquals(-8.5, CalculatedValue.Value, MARGIN, 'Wrong calculatedvalue for InputSexagesimal');
  CheckTrue(CalculatedValue.Valid);
end;

procedure TestSexagesimalValidationHandler.TestHappyFlowNegativeValue;
var
  Handler: ISexagesimalValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TSexagesimalValidationHandler.Create(-23, 23);
  CalculatedValue := Handler.CalculateValue('-8', '30', '0', true);
  CheckEquals(-8.5, CalculatedValue.Value, MARGIN, 'Wrong calculatedvalue for InputSexagesimal');
  CheckTrue(CalculatedValue.Valid);
end;

procedure TestSexagesimalValidationHandler.TestErrorDHOutOfRange;
var
  Handler: ISexagesimalValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TSexagesimalValidationHandler.Create(-23, 23);
  CalculatedValue := Handler.CalculateValue('100', '30', '0', false);
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Wrong calculatedvalue for InputSexagesimal');
  CheckFalse(CalculatedValue.Valid);
end;

procedure TestSexagesimalValidationHandler.TestErrorMinutesOutOfRange;
var
  Handler: ISexagesimalValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TSexagesimalValidationHandler.Create(-23, 23);
  CalculatedValue := Handler.CalculateValue('12', '60', '0', false);
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Wrong calculatedvalue for InputSexagesimal');
  CheckFalse(CalculatedValue.Valid);
end;

procedure TestSexagesimalValidationHandler.TestErrorSecondsOutOfRange;
var
  Handler: ISexagesimalValidationHandler;
  CalculatedValue : IValidatedDouble;
begin
  Handler := TSexagesimalValidationHandler.Create(-23, 23);
  CalculatedValue := Handler.CalculateValue('12', '10', '60', false);
  CheckEquals(0.0, CalculatedValue.Value, MARGIN, 'Wrong calculatedvalue for InputSexagesimal.');
  CheckFalse(CalculatedValue.Valid);
end;

{ TestFactorySexagesimalValidationHandler }
procedure TestFactorySexagesimalValidationHandler.GetInstance;
begin
  assertTrue(FactorySexagesimalValidationHandler.GetInstance(0, 90) is ISexagesimalValidationHandler) ;
end;



initialization

RegisterTest('XSharedHandlers',TestDateValidationHandler);
RegisterTest('XSharedHandlers',TestTimeValidationHandler);
RegisterTest('XSharedHandlers',TestLongitudeValidationHandler);
RegisterTest('XSharedHandlers',TestLatitudeValidationHandler);
RegisterTest('XSharedHandlers',TestSexagesimalValidationHandler);
RegisterTest('XSharedHandlers',TestFactorySexagesimalValidationHandler);

end.

