unit TestCaseBeChartsDomain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, BeChartsDomain, BeChartsDomainImpl;

type
  TestHorizontalPosition = class(TTestCase)
  published
    procedure SetUp; override;
    procedure TestAzimuth;
    procedure TestAltitude;
end;


implementation

uses XSharedDomain;

const
  MARGIN = 0.000000001;

var
  HorizontalPositionAltitude: Double = 23.5;
  HorizontalPositionAzimuth: Double = 221.22;
  HorizontalPosition: IHorizontalPosition;

{ TestHorizontalPosition }

procedure TestHorizontalPosition.SetUp;
begin
  HorizontalPosition:= THorizontalPosition.Create(HorizontalPositionAltitude, HorizontalPositionAzimuth);
end;

procedure TestHorizontalPosition.TestAltitude;
begin
  CheckEquals(HorizontalPositionAltitude, HorizontalPosition.Altitude, MARGIN);
end;

procedure TestHorizontalPosition.TestAzimuth;
begin
  CheckEquals(HorizontalPositionAzimuth, HorizontalPosition.Azimuth, MARGIN);
end;




initialization
  RegisterTest('BeChartsDomain',TestHorizontalPosition);

end.

