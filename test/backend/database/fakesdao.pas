unit FakesDao;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, BeDbDao, XSharedDomain, XChartsDomain, FakesXSharedDomain, FakesXChartsDomain;

type
  FakeLookUpValueDao = class (TInterfacedObject, ILookUpValueDao)
    function Read(PId: Integer): TLookUpValueDtoArray;
    function ReadAll(SortColumn: String): TLookUpValueDtoArray;
    function Insert(PId: Integer; PName, PDescription: String): String;
    function Update(PId: Integer; PName, PDescription: String): String;
    function Delete(PId: Integer): String;
  end;

  FakeHouseSystemsDao = class(TInterfacedObject, IHouseSystemsDao)
    function Read(PId: Integer): THouseSystemDtoArray;
    function ReadAll(PSortColumn: String): THouseSystemDtoArray;
    function Insert(PId: Integer; PName, PDescription: String; PNrOfHouses: Integer;
                    PClockWise, PQuadrantSystem, PCuspIsStart: Boolean): String;
    function Update(PId: Integer; PName, PDescription: String; PNrOfHouses: Integer;
                    PClockWise, PQuadrantSystem, PCuspIsStart: Boolean): String;
    function Delete(PId: Integer): String;
  end;

  FakeVersionsDao = class(TInterfacedObject, IVersionsDao)
    function Read(PId: Integer): TVersionDtoArray;
    function ReadAll(PSortColumn: String): TVersionDtoArray;
    function Insert(PId, PMajor, PMinor, PMicro: Integer; PDate: String): String;
  end;

  FakeAyanamshasDao = class(TInterfacedObject, IAyanamshasDao)
    function Read(PId: Integer): TAyanamshaDtoArray;
    function ReadAll(PSortColumn: String): TAyanamshaDtoArray;
    function Insert(PId: Integer; PName, PDescription: String; POffset2000: Double): String;
    function Update(PId: Integer; PName, PDescription: String; POffset2000: Double): String;
    function Delete(PId: Integer): String;
  end;

  FakeBodiesDao = class(TInterfacedObject, IBodiesDao)
    function Read(PId: Integer): TBodyDtoArray;
    function ReadAll(PSortColumn: String):  TBodyDtoArray;
    function Insert(PId: Integer; PName: String; BodyCategory: Integer): String;
    function Update(PId: Integer; PName: String; BodyCategory: Integer): String;
    function Delete(PId: Integer): String;
  end;

  FakeAspectsDao = class(TInterfacedObject, IAspectsDao)
    function Read(PId: Integer): TAspectDtoArray;
    function ReadAll(PSortColumn: String):  TAspectDtoArray;
    function Insert(PId: Integer; PName: String; Angle: Double; AspectCategory: Integer): String;
    function Update(PId: Integer; PName: String; Angle: Double; AspectCategory: Integer): String;
    function Delete(PId: Integer): String;
  end;

  FakeConfigurationsDao = class(TInterfacedObject, IConfigurationsDao)
    function Read(PId: Integer): TConfigurationDtoArray;
    function ReadAll(PSortColumn: String): TConfigurationDtoArray;
    function Insert(PId: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
                    PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer): String;
    function Update(PId: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
                    PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer): String;
    function Delete(PId: Integer): String;
  end;

  FakeGlyphsBodiesDao = class(TInterfacedObject, IGlyphsBodiesDao)
    function Read(PBodyId: Integer): TGlyphsBodiesDtoArray;
    function ReadAll(PSortColumn: String): TGlyphsBodiesDtoArray;
    function Insert(PBodyId: Integer; PGlyph: String): String;
  end;

  FakeGlyphsConfigurationDao = class(TInterfacedObject, IGlyphsConfigurationDao)
    function Read(PConfigId, PItemId: Integer; PCategory: String): TGlyphsConfigurationDtoArray;
    function ReadAll(PSortColumn, PCategory: String): TGlyphsConfigurationDtoArray;
    function Insert(PConfigId, PItemId: Integer; PCategory, PGlyph: String): String;
  end;

implementation


  { FakeLookUpValueDao }
  function FakeLookUpValueDao.Read(PId: Integer): TLookUpValueDtoArray;
    var
      AllResults: TLookUpValueDtoArray;
    begin
      SetLength(AllResults, 1);
      AllResults[0]:= FakeLookUpValueDto.Create;
      Result:= AllResults;
    end;

  function FakeLookUpValueDao.ReadAll(SortColumn: String): TLookUpValueDtoArray;
    var
      AllResults: TLookUpValueDtoArray;
    begin
      SetLength(AllResults, 2);
      AllResults[0]:= FakeLookUpValueDto.Create;
      AllResults[1]:= FakeLookUpValueDto2.Create;
      Result:= AllResults;
    end;

  function FakeLookUpValueDao.Insert(PId: Integer; PName, PDescription: String): String;
    begin
      Result:= 'OK';
    end;

  function FakeLookUpValueDao.Update(PId: Integer; PName, PDescription: String): String;
    begin
      Result:= 'OK';
    end;

  function FakeLookUpValueDao.Delete(PId: Integer): String;
    begin
      Result:= 'OK';
    end;


  { FakeHouseSystemsDao }
  function FakeHouseSystemsDao.Read(PId: Integer): THouseSystemDtoArray;
    var
      AllResults: THouseSystemDtoArray;
    begin
      SetLength(AllResults, 1);
      AllResults[0]:= FakeHouseSystemDto.Create;
      Result:= AllResults;
    end;

  function FakeHouseSystemsDao.ReadAll(PSortColumn: String): THouseSystemDtoArray;
    var
      AllResults: THouseSystemDtoArray;
    begin
      SetLength(AllResults, 2);
      AllResults[0]:= FakeHouseSystemDto.Create;
      AllResults[1]:= FakeHouseSystemDto2.Create;
      Result:= AllResults;
    end;

  function FakeHouseSystemsDao.Insert(PId: Integer; PName, PDescription: String; PNrOfHouses: Integer;
                  PClockWise, PQuadrantSystem, PCuspIsStart: Boolean): String;
    begin
      Result:= 'OK';
    end;

  function FakeHouseSystemsDao.Update(PId: Integer; PName, PDescription: String; PNrOfHouses: Integer;
                  PClockWise, PQuadrantSystem, PCuspIsStart: Boolean): String;
    begin
      Result:= 'OK';
    end;

  function FakeHouseSystemsDao.Delete(PId: Integer): String;
    begin
      Result:= 'OK';
    end;

  { FakeVersionsDao }
  function FakeVersionsDao.Read(PId: Integer): TVersionDtoArray;
    var
      AllResults: TVersionDtoArray;
    begin
      SetLength(AllResults, 1);
      AllResults[0]:= FakeVersionDto.Create;
      Result:= AllResults;
    end;

  function FakeVersionsDao.ReadAll(PSortColumn: String): TVersionDtoArray;
    var
      AllResults: TVersionDtoArray;
    begin
      SetLength(AllResults, 2);
      AllResults[0]:= FakeVersionDto.Create;
      AllResults[2]:= FakeVersionDto2.Create;
      Result:= AllResults;
    end;

  function FakeVersionsDao.Insert(PId, PMajor, PMinor, PMicro: Integer; PDate: String): String;
    begin
      Result:= 'OK';
    end;

  { FakeAyanamshasDao }

  function FakeAyanamshasDao.Read(PId: Integer): TAyanamshaDtoArray;
    var
      AllResults: TAyanamshaDtoArray;
    begin
      SetLength(AllResults, 1);
      AllResults[0]:= FakeAyanamshaDto.Create;
      Result:= AllResults;
    end;

  function FakeAyanamshasDao.ReadAll(PSortColumn: String): TAyanamshaDtoArray;
    var
      AllResults: TAyanamshaDtoArray;
    begin
      SetLength(AllResults, 2);
      AllResults[0]:= FakeAyanamshaDto.Create;
      AllResults[1]:= FakeAyanamshaDto2.Create;
      Result:= AllResults;
    end;

  function FakeAyanamshasDao.Insert(PId: Integer; PName, PDescription: String; POffset2000: Double): String;
    begin
      Result:= 'OK';
    end;

  function FakeAyanamshasDao.Update(PId: Integer; PName, PDescription: String; POffset2000: Double): String;
    begin
      Result:= 'OK';
    end;

  function FakeAyanamshasDao.Delete(PId: Integer): String;
    begin
      Result:= 'OK';
    end;


  { FakeBodiesDao }

  function FakeBodiesDao.Read(PId: Integer): TBodyDtoArray;
    var
      AllResults: TBodyDtoArray;
    begin
      SetLength(AllResults, 1);
      AllResults[0]:= FakeBodyDto.Create;
      Result:= AllResults;
    end;

  function FakeBodiesDao.ReadAll(PSortColumn: String):  TBodyDtoArray;
    var
      AllResults: TBodyDtoArray;
    begin
      SetLength(AllResults, 2);
      AllResults[0]:= FakeBodyDto.Create;
      AllResults[1]:= FakeBodyDto.Create;
      Result:= AllResults;
    end;

  function FakeBodiesDao.Insert(PId: Integer; PName: String; BodyCategory: Integer): String;
    begin
      Result:= 'OK';
    end;

  function FakeBodiesDao.Update(PId: Integer; PName: String; BodyCategory: Integer): String;
    begin
      Result:= 'OK';
    end;

  function FakeBodiesDao.Delete(PId: Integer): String;
    begin
      Result:= 'OK';
    end;

  { FakeAspectsDao }

  function FakeAspectsDao.Read(PId: Integer): TAspectDtoArray;
    var
      AllResults: TAspectDtoArray;
    begin
      SetLength(AllResults, 1);
      AllResults[0]:= FakeAspectDto.Create;
      Result:= AllResults;
    end;


  function FakeAspectsDao.ReadAll(PSortColumn: String):  TAspectDtoArray;
    var
      AllResults: TAspectDtoArray;
    begin
      SetLength(AllResults, 2);
      AllResults[0]:= FakeAspectDto.Create;
      AllResults[1]:= FakeAspectDto.Create;
      Result:= AllResults;
    end;

  function FakeAspectsDao.Insert(PId: Integer; PName: String; Angle: Double; AspectCategory: Integer): String;
    begin
      Result:= 'OK';
    end;

  function FakeAspectsDao.Update(PId: Integer; PName: String; Angle: Double; AspectCategory: Integer): String;
    begin
      Result:= 'OK';
    end;

  function FakeAspectsDao.Delete(PId: Integer): String;
    begin
      Result:= 'OK';
    end;


  { FakeConfigurationsDao }
  function FakeConfigurationsDao.Read(PId: Integer): TConfigurationDtoArray;
    var
      AllResults: TConfigurationDtoArray;
    begin
      SetLength(AllResults, 1);
      AllResults[0]:= FakeConfigurationDto.Create;
      Result:= AllResults;
    end;

  function FakeConfigurationsDao.ReadAll(PSortColumn: String): TConfigurationDtoArray;
    var
      AllResults: TConfigurationDtoArray;
    begin
      SetLength(AllResults, 2);
      AllResults[0]:= FakeConfigurationDto.Create;
      AllResults[1]:= FakeConfigurationDto2.Create;
      Result:= AllResults;
    end;

  function FakeConfigurationsDao.Insert(PId: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
                  PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer): String;
    begin
      Result:= 'OK';
    end;
  function FakeConfigurationsDao.Update(PId: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
                  PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer): String;
    begin
      Result:= 'OK';
    end;
  function FakeConfigurationsDao.Delete(PId: Integer): String;
    begin
      Result:= 'OK';
    end;

  { FakeGlyphsBodiesDao }
    function FakeGlyphsBodiesDao.Read(PBodyId: Integer): TGlyphsBodiesDtoArray;
      var
        AllResults: TGlyphsBodiesDtoArray;
      begin
        SetLength(AllResults, 1);
        AllResults[0]:= FakeGlyphsBodiesDto.Create;
        Result:= AllResults;
      end;

    function FakeGlyphsBodiesDao.ReadAll(PSortColumn: String): TGlyphsBodiesDtoArray;
      var
        AllResults: TGlyphsBodiesDtoArray;
      begin
        SetLength(AllResults, 2);
        AllResults[0]:= FakeGlyphsBodiesDto.Create;
        AllResults[1]:= FakeGlyphsBodiesDto2.Create;
        Result:= AllResults;
      end;

    function FakeGlyphsBodiesDao.Insert(PBodyId: Integer; PGlyph: String): String;
      begin
        Result:= 'OK';
      end;

  { FakeGlyphsConfigurationDao }
    function FakeGlyphsConfigurationDao.Read(PConfigId, PItemId: Integer; PCategory: String): TGlyphsConfigurationDtoArray;
      var
        AllResults: TGlyphsConfigurationDtoArray;
      begin
        SetLength(AllResults, 1);
        AllResults[0]:= FakeGlyphsConfigurationDto.Create;
        Result:= AllResults;
      end;

    function FakeGlyphsConfigurationDao.ReadAll(PSortColumn, PCategory: String): TGlyphsConfigurationDtoArray;
      var
        AllResults: TGlyphsConfigurationDtoArray;
      begin
        SetLength(AllResults, 1);
        AllResults[0]:= FakeGlyphsConfigurationDto.Create;
        AllResults[0]:= FakeGlyphsConfigurationDto.Create;
        Result:= AllResults;
      end;

    function FakeGlyphsConfigurationDao.Insert(PConfigId, PItemId: Integer; PCategory, PGlyph: String): String;
      begin
        Result:= 'OK';
      end;



end.

