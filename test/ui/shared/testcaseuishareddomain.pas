{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseUiSharedDomain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, UiSharedDomain, UiSharedDomainImpl;

const
     DEGREE_SIGN = '°';
     MINUTE_SIGN = '''';
     SECOND_SIGN = '"';
     SPACE = ' ';
     MARGIN = 0.000000001;
type
   TestDecimalValue = class(TTestCase)
   published
     procedure TestHappyFlow;
     procedure TestShortFraction;
   end;

   TestSexagesimalValue = class(TTestCase)
     published
       procedure TestHappyFlow;
       procedure TestShortFraction;
       procedure TestNegativeValue;
   end;

   TestMetaInfoValues = class(TTestCase)
     published
       procedure TestHappyFlow;
   end;

   TestGraphicBodyPosition = class(TTestCase)
     private
       BodyIndex: Integer;
       EclipticPosition, MundanePosition: Double;
       GraphicBodyPosition: IGraphicBodyPosition;
     published
       procedure SetUp; override;
       procedure TestBodyIndex;
       procedure TestEclipticPosition;
       procedure TestMundanePosition;
       procedure TestMundanePositionUsingSetter;
       procedure PlotPosition;
   end;

   TestSortedGraphicBodyPositions = class(TTestCase)
     private
       MinDistance: Integer;
       SortedPositions, SortedPositionsWithOverlap: TArrayGraphicBodyPositions;
       SortedGraphicBodyPositions, SortedGraphicBodyPositionsWithOverlap: ISortedGraphicBodyPositions;
     published
       procedure SetUp; override;
       procedure TestSortedPositions;
       procedure TestSortedPositionsWithOverlap;
       procedure TestPositionValues;
       procedure TestPositionValuesWithOverlap;
   end;


implementation

{ TestDecimalValue }
procedure TestDecimalValue.TestHappyFlow;
var
  DecValue : Double;
  Expected, Result: String;
  DecimalValue: TDecimalValue;
begin
  DecValue:= 123.458987654321098;
  Expected:= '123,45898765';
  DecimalValue:= TDecimalValue.Create(DecValue);
  Result:= DecimalValue.GetText;
  CheckEquals(Expected, Result);
end;


procedure TestDecimalValue.TestShortFraction;
var
  DecValue : Double;
  Expected, Result: String;
  DecimalValue: TDecimalValue;
begin
  DecValue:= 123.45;
  Expected:= '123,45000000';
  DecimalValue:= TDecimalValue.Create(DecValue);
  Result:= DecimalValue.GetText;
  CheckEquals(Expected, Result);
end;

{ TestSexagesimalValue }

procedure TestSexagesimalValue.TestHappyFlow;
var
  DecValue : Double;
  Expected, Result: String;
  SexagValue: TSexagesimalValue;
begin
  DecValue:= 123.458987654321098;
  Expected:= '123'+ DEGREE_SIGN +  '27' + MINUTE_SIGN + '32' + SECOND_SIGN;
  SexagValue:= TSexagesimalValue.Create(DecValue);
  Result:= SexagValue.GetText;
  CheckEquals(Expected, Result);
end;

procedure TestSexagesimalValue.TestShortFraction;
var
  DecValue : Double;
  Expected, Result: String;
  SexagValue: TSexagesimalValue;
begin
  DecValue:= 1.1016666666666668;
  Expected:= '  1'+ DEGREE_SIGN + ' 6' + MINUTE_SIGN + ' 6' + SECOND_SIGN;
  SexagValue:= TSexagesimalValue.Create(DecValue);
  Result:= SexagValue.GetText;
  CheckEquals(Expected, Result);
end;

procedure TestSexagesimalValue.TestNegativeValue;
var
  DecValue : Double;
  Expected, Result: String;
  SexagValue: TSexagesimalValue;
begin
  DecValue:= -123.458987654321098;
  Expected:= '-123'+ DEGREE_SIGN +  '27' + MINUTE_SIGN + '32' + SECOND_SIGN;
  SexagValue:= TSexagesimalValue.Create(DecValue);
  Result:= SexagValue.GetText;
  CheckEquals(Expected, Result);
end;



{ TestMetaInfoValues }
procedure TestMetaInfoValues.TestHappyFlow;
var
  Name, Location, DateTime, Settings: String;
  MetaInfoValues: TMetaInfoValues;
begin
  Name:= 'MyName';
  Location:= 'MyLocation';
  DateTime:= 'MyDateTime';
  Settings:= 'MySettings';
  MetaInfoValues:= TMetaInfoValues.Create(Name, Location, DateTime, Settings);
  CheckEquals(Name, MetaInfoValues.GetName);
  CheckEquals(Location, MetaInfoValues.GetLocation);
  CheckEquals(DateTime, MetaInfoValues.GetDateTime);
  CheckEquals(Settings, MetaInfoValues.GetSettings);
end;

{ TestGraphicBodyPosition }
  procedure TestGraphicBodyPosition.SetUp;
    begin
      BodyIndex:= 3;
      EclipticPosition:= 320.5;
      MundanePosition:= 10.7;
      GraphicBodyPosition:= TGraphicBodyPosition.Create(BodyIndex, EclipticPosition, MundanePosition);
    end;

  procedure TestGraphicBodyPosition.TestBodyIndex;
    begin
      AssertEquals(BodyIndex, GraphicBodyPosition.BodyIndex);
    end;

  procedure TestGraphicBodyPosition.TestEclipticPosition;
    begin
      AssertEquals(EclipticPosition, GraphicBodyPosition.EclipticPosition, MARGIN);
    end;

  procedure TestGraphicBodyPosition.TestMundanePosition;
    begin
      AssertEquals(MundanePosition, GraphicBodyPosition.MundanePosition, MARGIN);
    end;

  procedure TestGraphicBodyPosition.TestMundanePositionUsingSetter;
    var
      AltMundanePosition: Double;
    begin
      AltMundanePosition:= 10.3;
      GraphicBodyPosition.SetMundanePosition(AltMundanePosition);
      AssertEquals(AltMundanePosition, GraphicBodyPosition.MundanePosition, MARGIN);
    end;

  procedure TestGraphicBodyPosition.PlotPosition;
    var
      PlotPositionToSet: Double;
    begin
      PlotPositionToSet:= 10.9;
      GraphicBodyPosition.SetPlotPosition(PlotPositionToSet);
      AssertEquals(PlotPositionToSet, GraphicBodyPosition.PlotPosition, MARGIN);
    end;

{ TestSortedGraphicBodyPositions }
  procedure TestSortedGraphicBodyPositions.SetUp;
    begin
      MinDistance:= 2;
      SetLength(SortedPositions, 5);
      SortedPositions[0]:= TGraphicBodyPosition.Create(0, 310.0, 210.0);
      SortedPositions[1]:= TGraphicBodyPosition.Create(1, 30.5, 290.5);
      SortedPositions[2]:= TGraphicBodyPosition.Create(2, 200.1, 100.1);
      SortedPositions[3]:= TGraphicBodyPosition.Create(3, 0.0, 260.0);
      SortedPositions[4]:= TGraphicBodyPosition.Create(4, 90.2, 350.2);
      SortedGraphicBodyPositions:= TSortedGraphicBodyPositions.Create(SortedPositions, MinDistance);
      SetLength(SortedPositionsWithOverlap, 5);
      SortedPositionsWithOverlap[0]:= TGraphicBodyPosition.Create(0, 310.1, 210.1);
      SortedPositionsWithOverlap[1]:= TGraphicBodyPosition.Create(1, 30.5, 290.5);
      SortedPositionsWithOverlap[2]:= TGraphicBodyPosition.Create(2, 311.0, 211.0);
      SortedPositionsWithOverlap[3]:= TGraphicBodyPosition.Create(3, 0.0, 260.0);
      SortedPositionsWithOverlap[4]:= TGraphicBodyPosition.Create(4, 90.2, 350.2);
      SortedGraphicBodyPositionsWithOverlap:= TSortedGraphicBodyPositions.Create(SortedPositionsWithOverlap, MinDistance);
    end;

  procedure TestSortedGraphicBodyPositions.TestSortedPositions;
    begin
      AssertEquals(2, SortedGraphicBodyPositions.GetPositions[0].BodyIndex);
      AssertEquals(0, SortedGraphicBodyPositions.GetPositions[1].BodyIndex);
      AssertEquals(3, SortedGraphicBodyPositions.GetPositions[2].BodyIndex);
      AssertEquals(1, SortedGraphicBodyPositions.GetPositions[3].BodyIndex);
      AssertEquals(4, SortedGraphicBodyPositions.GetPositions[4].BodyIndex);
    end;

  procedure TestSortedGraphicBodyPositions.TestSortedPositionsWithOverlap;
    begin
      AssertEquals(0, SortedGraphicBodyPositionsWithOverlap.GetPositions[0].BodyIndex);
      AssertEquals(2, SortedGraphicBodyPositionsWithOverlap.GetPositions[1].BodyIndex);
      AssertEquals(3, SortedGraphicBodyPositionsWithOverlap.GetPositions[2].BodyIndex);
      AssertEquals(1, SortedGraphicBodyPositionsWithOverlap.GetPositions[3].BodyIndex);
      AssertEquals(4, SortedGraphicBodyPositionsWithOverlap.GetPositions[4].BodyIndex);
    end;

  procedure TestSortedGraphicBodyPositions.TestPositionValues;
    begin
      AssertEquals(200.1, SortedGraphicBodyPositions.GetPositions[0].EclipticPosition, MARGIN);
      AssertEquals(100.1, SortedGraphicBodyPositions.GetPositions[0].MundanePosition, MARGIN);
      AssertEquals(100.1, SortedGraphicBodyPositions.GetPositions[0].PlotPosition, MARGIN);
    end;

  procedure TestSortedGraphicBodyPositions.TestPositionValuesWithOverlap;
    begin
      AssertEquals(210.1, SortedGraphicBodyPositionsWithOverlap.GetPositions[0].PlotPosition, MARGIN);
      AssertEquals(212.1, SortedGraphicBodyPositionsWithOverlap.GetPositions[1].PlotPosition, MARGIN); // overlap: moved
    end;


initialization

  RegisterTest('UiSharedDomain', TestDecimalValue);
  RegisterTest('UiSharedDomain', TestSexagesimalValue);
  RegisterTest('UiSharedDomain', TestMetaInfoValues);
  RegisterTest('UiSharedDomain', TestGraphicBodyPosition);
  RegisterTest('UiSharedDomain', TestSortedGraphicBodyPositions);


end.

