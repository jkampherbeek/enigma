{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseUiSharedUtils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, UiSharedUtils;

type

  TestUiCalculations = class(TTestCase)
  strict private
    UiCalc: IUiCalculations;
  published
    procedure SetUp; override;
    procedure TestMakeEvenInputEven;
    procedure TestMakeEvenInputOdd;
    procedure TestMakeEvenInputNegative;
    procedure TestRange360Int;
    procedure TestRange360IntLowInput;
    procedure TestRange360IntHighInput;
    procedure TestRange360Dbl;
    procedure TestRange360DblLowInput;
    procedure TestRange360DblHighInput;
  end;

  TestRectTriangle = class(TTestCase)
    strict private
      CenterX, CenterY, Hypothenusa: Integer;
      Angle: Double;
      RectTriangle: IRectTriangle;
    published
      procedure SetUp; override;
      procedure TestXYPointSmall;
      procedure TestXYPointLarge;
  end;

implementation

  uses
    UiSharedUtilsImpl;

  const
    MARGIN = 0.000000001;

{ TestUiCalculations }
  procedure TestUiCalculations.SetUp;
    begin
      UiCalc:= TUiCalculations.Create;
    end;

  procedure TestUiCalculations.TestMakeEvenInputEven;
    begin
      AssertEquals(6, UiCalc.MakeEven(6));
    end;

  procedure TestUiCalculations.TestMakeEvenInputOdd;
    begin
      AssertEquals(8, UiCalc.MakeEven(7));
    end;

  procedure TestUiCalculations.TestMakeEvenInputNegative;
    begin
      AssertEquals(-6, UiCalc.MakeEven(-7));
    end;

  procedure TestUiCalculations.TestRange360Int;
    begin
      AssertEquals( 222, UiCalc.Range360(222));
    end;

  procedure TestUiCalculations.TestRange360IntLowInput;
    begin
      AssertEquals(40, UiCalc.Range360(-680));
    end;

  procedure TestUiCalculations.TestRange360IntHighInput;
    begin
      AssertEquals(100, UiCalc.Range360(820));
    end;

  procedure TestUiCalculations.TestRange360Dbl;
    begin
      AssertEquals(123.456, UiCalc.Range360(123.456), MARGIN);
    end;

  procedure TestUiCalculations.TestRange360DblLowInput;
    begin
      AssertEquals(259.5, UiCalc.Range360(-100.5), MARGIN);
    end;

  procedure TestUiCalculations.TestRange360DblHighInput;
    begin
      AssertEquals(33.8, UiCalc.Range360(753.8), MARGIN);
    end;

  { TestRectTriangle }
  procedure TestRectTriangle.SetUp;
    begin
      CenterX:= 200;
      CenterY:= 400;
      Hypothenusa:= 100;
    end;

  procedure TestRectTriangle.TestXYPointSmall;
    var
      ExpectedX, ExpectedY: Integer;
    begin
      Angle:= 30.0;
      ExpectedX:= 114;
      ExpectedY:= 350;
      RectTriangle:= TRectTriangle.Create(CenterX, CenterY, Hypothenusa, Angle);
      AssertEquals(ExpectedX, RectTriangle.XYPoint.x);
      AssertEquals(ExpectedY, RectTriangle.XYPoint.y);
    end;

  procedure TestRectTriangle.TestXYPointLarge;
    var
      ExpectedX, ExpectedY: Integer;
    begin
      Angle:= 211.0;
      ExpectedX:= 286;
      ExpectedY:= 452;
      RectTriangle:= TRectTriangle.Create(CenterX, CenterY, Hypothenusa, Angle);
      AssertEquals(ExpectedX, RectTriangle.XYPoint.x);
      AssertEquals(ExpectedY, RectTriangle.XYPoint.y);
    end;



initialization

  RegisterTest('UiSharedUtils',TestUiCalculations);
  RegisterTest('UiSharedUtils',TestRectTriangle);

end.

