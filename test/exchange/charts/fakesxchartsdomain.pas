unit FakesXChartsDomain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XChartsDomain, XSharedDomain;

type

    FakeLookUpValueDto = class (TInterfacedObject, ILookUpValueDto)
    function GetId: Integer;
    function GetName: String;
    function GetDescription: String;
  end;

  FakeLookUpValueDto2 = class (TInterfacedObject, ILookUpValueDto)
    function GetId: Integer;
    function GetName: String;
    function GetDescription: String;
  end;

  FakeVersionDto = class (TInterfacedObject, IVersionDto)
    function GetId: Integer;
    function GetMajor: Integer;
    function GetMinor: Integer;
    function GetMicro: Integer;
    function GetDate: String;
  end;

  FakeVersionDto2 = class (TInterfacedObject, IVersionDto)
    function GetId: Integer;
    function GetMajor: Integer;
    function GetMinor: Integer;
    function GetMicro: Integer;
    function GetDate: String;
  end;

  FakeHouseSystemDto = class(TInterfacedObject, IHouseSystemDto)
     function Getid: Integer;
     function GetName: String;
     function GetDescription: String;
     function GetNrOfHouses: Integer;
     function IsCounterClockWise: Boolean;
     function IsQuadrantSystem: Boolean;
     function IsCuspIsStart: Boolean;
  end;

  FakeHouseSystemDto2 = class(TInterfacedObject, IHouseSystemDto)
     function Getid: Integer;
     function GetName: String;
     function GetDescription: String;
     function GetNrOfHouses: Integer;
     function IsCounterClockWise: Boolean;
     function IsQuadrantSystem: Boolean;
     function IsCuspIsStart: Boolean;
  end;

  FakeAyanamshaDto = class(TInterfacedObject, IAyanamshaDto)
    function Getid: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetOffset2000: Real;
  end;

  FakeAyanamshaDto2 = class(TInterfacedObject, IAyanamshaDto)
    function Getid: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetOffset2000: Real;
  end;

  FakeBodyDto = class(TInterfacedObject, IBodyDto)
    function Getid: Integer;
    function GetName: String;
    function GetBodyCategory: Integer;
  end;

  FakeBodyDto2 = class(TInterfacedObject, IBodyDto)
    function Getid: Integer;
    function GetName: String;
    function GetBodyCategory: Integer;
  end;

  FakeAspectDto = class(TInterfacedObject, IAspectDto)
    function Getid: Integer;
    function GetName: String;
    function GetAngle: Double;
    function GetAspectCategory: Integer;
  end;

  FakeAspectDto2 = class(TInterfacedObject, IAspectDto)
    function Getid: Integer;
    function GetName: String;
    function GetAngle: Double;
    function GetAspectCategory: Integer;
  end;

  FakeConfigurationDto = class(TInterfacedObject, IConfigurationDto)
    function Getid: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetBaseOrbAspects: Double;
    function GetBaseOrbMidpoints: Double;
    function GetCoordinateSystem: Integer;
    function GetAyanamsha: Integer;
    function GetHouseSystem: integer;
    function GetObserverPosition: Integer;
    function GetProjectionType: Integer;
  end;

  FakeConfigurationDto2 = class(TInterfacedObject, IConfigurationDto)
    function Getid: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetBaseOrbAspects: Double;
    function GetBaseOrbMidpoints: Double;
    function GetCoordinateSystem: Integer;
    function GetAyanamsha: Integer;
    function GetHouseSystem: integer;
    function GetObserverPosition: Integer;
    function GetProjectionType: Integer;
  end;

  FakeGlyphsBodiesDto = class(TInterfacedObject, IGlyphsBodiesDto)
    function GetId: LongInt;
    function GetGlyph: String;
  end;

  FakeGlyphsBodiesDto2 = class(TInterfacedObject, IGlyphsBodiesDto)
    function GetId: LongInt;
    function GetGlyph: String;
  end;

  FakeGlyphsConfigurationDto = class(TInterfacedObject, IGlyphsConfigurationDto)
    function GetConfigId: LongInt;
    function GetItemId: LongInt;
    function GetCategory: String;
    function GetGlyph: String;
  end;

  FakeGlyphsConfigurationDto2 = class(TInterfacedObject, IGlyphsConfigurationDto)
    function GetConfigId: LongInt;
    function GetItemId: LongInt;
    function GetCategory: String;
    function GetGlyph: String;
  end;


  FakeCelestialObjectSimple = class(TInterfacedObject, ICelestialObjectSimple)
    function GetMainPos: Double;
    function GetDeviationPos: Double;
    function GetDistancePos: Double;
    function GetMainSpeed: Double;
    function GetDeviationSpeed: Double;
    function GetDistanceSpeed: Double;
  end;

  FakeCelestialObjectFull = class (TInterfacedObject, ICelestialObjectFull)
    function GetObjectId: Integer;
    function GetEclipticalPos: ICelestialObjectSimple;
    function GetEquatorialPos: ICelestialObjectSimple;
    function GetHorizontalPos: ICelestialObjectSimple;
 end;

  FakeHouseSystemSpec = class(TInterfacedObject, IHouseSystemSpec)
    function GetName: String;
    function GetSeId: String;
    function GetDescription: String;
    function GetId: Integer;
    function GetNumberOfHouses: Integer;
    function IsQuadrantSystem: Boolean;
    function IsCuspIsStart: Boolean;
  end;

  FakeHousePositionFull = class(TInterfacedObject, IHousePositionFull)
    function GetLongitude: Double;
    function GetRightAscension: Double;
    function GetDeclination: Double;
    function GetAzimuth: Double;
    function GetAltitude: Double;
  end;

  FakeCalculationSettings = class(TInterfacedObject, ICalculationSettings)
    function GetPosition: Integer;
    function GetAyanamsha: Integer;
    function GetHouseSystemSpec: IHouseSystemSpec;
    function GetObjects: TIntArray;
    function GetReferenceFrame: IReferenceFrame;
  end;

  FakeHousesResponse = class(TInterfacedObject, IHousesResponse)
    function GetMc: IHousePositionFull;
    function GetAsc: IHousePositionFull;
    function GetCusps: THousePositionFullArray;
    function GetHouseSystemSpec: IHouseSystemSpec;
  end;

  FakeFullChartRequest = class(TInterfacedObject, IFullChartRequest)
    function GetName: String;
    function GetLocation: IValidatedLocation;
    function GetDate: IValidatedDate;
    function GetTime: IValidatedTime;
    function GetCalculationSettings: ICalculationSettings;
  end;

  FakeFullChartResponse = class(TInterfacedObject, IFullChartResponse)
    function GetName: String;
    function GetAllObjects: TCelestialObjectFullArray;
    function GetHouses: IHousesResponse;
    function GetFullChartRequest: IFullChartRequest;
  end;

  FakeFullChartResponse2 = class(TInterfacedObject, IFullChartResponse)
    function GetName: String;
    function GetAllObjects: TCelestialObjectFullArray;
    function GetHouses: IHousesResponse;
    function GetFullChartRequest: IFullChartRequest;
  end;

implementation

uses FakesXSharedDomain, XSharedDomainImpl;


  { FakeLookUpValueDto }
  function FakeLookUpValueDto.GetId: Integer;
    begin
      Result:= 21;
    end;

  function FakeLookUpValueDto.GetName: String;
    begin
      Result:= 'MyName';
    end;

  function FakeLookUpValueDto.GetDescription: String;
    begin
      Result:= 'MyDescription';
    end;


  { FakeLookUpValueDto2 }
  function FakeLookUpValueDto2.GetId: Integer;
    begin
      Result:= 22;
    end;

  function FakeLookUpValueDto2.GetName: String;
    begin
      Result:= 'MyName2';
    end;

  function FakeLookUpValueDto2.GetDescription: String;
    begin
      Result:= 'MyDescription2';
    end;


  { FakeVersionDto }

  function FakeVersionDto.GetId: Integer;
    begin
      Result:= 1;
    end;

  function FakeVersionDto.GetMajor: Integer;
    begin
      Result:= 3;
    end;

  function FakeVersionDto.GetMinor: Integer;
    begin
      Result:= 2;
    end;

  function FakeVersionDto.GetMicro: Integer;
    begin
      Result:= 1;
    end;

  function FakeVersionDto.GetDate: String;
    begin
      Result:= '2000-01-01';
    end;


  { FakeVersionDto2 }

  function FakeVersionDto2.GetId: Integer;
    begin
      Result:= 12;
    end;

  function FakeVersionDto2.GetMajor: Integer;
    begin
      Result:= 32;
    end;

  function FakeVersionDto2.GetMinor: Integer;
    begin
      Result:= 22;
    end;

  function FakeVersionDto2.GetMicro: Integer;
    begin
      Result:= 12;
    end;

  function FakeVersionDto2.GetDate: String;
    begin
      Result:= '2000-01-02';
    end;

  { FakeHouseSystemDto }
  function FakeHouseSystemDto.Getid: Integer;
    begin
      Result:= 100;
    end;

  function FakeHouseSystemDto.GetName: String;
    begin
      Result:= 'HouseSystemName';
    end;

  function FakeHouseSystemDto.GetDescription: String;
    begin
      Result:= 'HouseSystemDescription';
    end;

  function FakeHouseSystemDto.GetNrOfHouses: Integer;
    begin
      Result:= 12;
    end;

  function FakeHouseSystemDto.IsCounterClockWise: Boolean;
    begin
      Result:= true;
    end;

  function FakeHouseSystemDto.IsQuadrantSystem: Boolean;
    begin
      Result:= true;
    end;

  function FakeHouseSystemDto.IsCuspIsStart: Boolean;
    begin
      Result:= true;
    end;


  { FakeHouseSystemDto2 }
  function FakeHouseSystemDto2.Getid: Integer;
    begin
      Result:= 102;
    end;

  function FakeHouseSystemDto2.GetName: String;
    begin
      Result:= 'HouseSystemName2';
    end;

  function FakeHouseSystemDto2.GetDescription: String;
    begin
      Result:= 'HouseSystemDescription2';
    end;

  function FakeHouseSystemDto2.GetNrOfHouses: Integer;
    begin
      Result:= 22;
    end;

  function FakeHouseSystemDto2.IsCounterClockWise: Boolean;
    begin
      Result:= true;
    end;

  function FakeHouseSystemDto2.IsQuadrantSystem: Boolean;
    begin
      Result:= false;
    end;

  function FakeHouseSystemDto2.IsCuspIsStart: Boolean;
    begin
      Result:= true;
    end;

  { FakeAyanamshaDto }
  function FakeAyanamshaDto.Getid: Integer;
    begin
      Result:= 400;
    end;

  function FakeAyanamshaDto.GetName: String;
    begin
      Result:= 'AyanamshaName';
    end;

  function FakeAyanamshaDto.GetDescription: String;
    begin
      Result:= 'AyanamshaDescription';
    end;

  function FakeAyanamshaDto.GetOffset2000: Real;
    begin
      Result:= 29.123;
    end;

  { FakeAyanamshaDto2 }
  function FakeAyanamshaDto2.Getid: Integer;
    begin
      Result:= 402;
    end;

  function FakeAyanamshaDto2.GetName: String;
    begin
      Result:= 'AyanamshaName2';
    end;

  function FakeAyanamshaDto2.GetDescription: String;
    begin
      Result:= 'AyanamshaDescription2';
    end;

  function FakeAyanamshaDto2.GetOffset2000: Real;
    begin
      Result:= 30.123;
    end;

  { FakeBodyDto }
  function FakeBodyDto.Getid: Integer;
    begin
      Result:= 777;
    end;

  function FakeBodyDto.GetName: String;
    begin
      Result:= 'BodyName';
    end;

  function FakeBodyDto.GetBodyCategory: Integer;
    begin
      Result:= 5;
    end;

  { FakeBodyDto2 }
  function FakeBodyDto2.Getid: Integer;
    begin
      Result:= 772;
    end;

  function FakeBodyDto2.GetName: String;
    begin
      Result:= 'BodyName2';
    end;

  function FakeBodyDto2.GetBodyCategory: Integer;
    begin
      Result:= 2;
    end;

  { FakeAspectDto }
  function FakeAspectDto.Getid: Integer;
    begin
      Result:= 799;
    end;

  function FakeAspectDto.GetName: String;
    begin
      Result:= 'AspectName';
    end;

  function FakeAspectDto.GetAngle: Double;
    begin
      Result:= 22.33;
    end;

  function FakeAspectDto.GetAspectCategory: Integer;
    begin
      Result:= 1;
    end;

  { FakeAspectDto2 }
  function FakeAspectDto2.Getid: Integer;
    begin
      Result:= 792;
    end;

  function FakeAspectDto2.GetName: String;
    begin
      Result:= 'AspectName2';
    end;

  function FakeAspectDto2.GetAngle: Double;
    begin
      Result:= 33.22;
    end;

  function FakeAspectDto2.GetAspectCategory: Integer;
    begin
      Result:= 2;
    end;

  { FakeConfigurationDto }
  function FakeConfigurationDto.Getid: Integer;
    begin
      Result:= 2;
    end;

  function FakeConfigurationDto.GetName: String;
    begin
      Result:= 'ConfigName1';
    end;

  function FakeConfigurationDto.GetDescription: String;
    begin
      Result:= 'ConfigDescription1';
    end;

  function FakeConfigurationDto.GetBaseOrbAspects: Double;
    begin
      Result:= 7.1;
    end;

  function FakeConfigurationDto.GetBaseOrbMidpoints: Double;
    begin
      Result:= 1.1;
    end;

  function FakeConfigurationDto.GetCoordinateSystem: Integer;
    begin
      Result:= 1;
    end;

  function FakeConfigurationDto.GetAyanamsha: Integer;
    begin
      Result:= 11;
    end;

  function FakeConfigurationDto.GetHouseSystem: integer;
    begin
      Result:= 21;
    end;

  function FakeConfigurationDto.GetObserverPosition: Integer;
    begin
      Result:= 3;
    end;

  function FakeConfigurationDto.GetProjectionType: Integer;
    begin
      Result:= 4;
    end;

  { FakeConfigurationDto2 }
   function FakeConfigurationDto2.Getid: Integer;
     begin
       Result:= 3;
     end;

   function FakeConfigurationDto2.GetName: String;
     begin
       Result:= 'ConfigName2';
     end;

   function FakeConfigurationDto2.GetDescription: String;
     begin
       Result:= 'ConfigDescription2';
     end;

   function FakeConfigurationDto2.GetBaseOrbAspects: Double;
     begin
       Result:= 7.2;
     end;

   function FakeConfigurationDto2.GetBaseOrbMidpoints: Double;
     begin
       Result:= 1.2;
     end;

   function FakeConfigurationDto2.GetCoordinateSystem: Integer;
     begin
       Result:= 2;
     end;

   function FakeConfigurationDto2.GetAyanamsha: Integer;
     begin
       Result:= 12;
     end;

   function FakeConfigurationDto2.GetHouseSystem: integer;
     begin
       Result:= 22;
     end;

   function FakeConfigurationDto2.GetObserverPosition: Integer;
     begin
       Result:= 1;
     end;

   function FakeConfigurationDto2.GetProjectionType: Integer;
     begin
       Result:= 2;
     end;

  { FakeGlyphsBodiesDto }
    function FakeGlyphsBodiesDto.GetId: LongInt;
      begin
        Result:= 3;
      end;

    function FakeGlyphsBodiesDto.GetGlyph: String;
      begin
        Result:= 'z';
      end;

  { FakeGlyphsBodiesDto2 }
    function FakeGlyphsBodiesDto2.GetId: LongInt;
      begin
        Result:= 8;
      end;

    function FakeGlyphsBodiesDto2.GetGlyph: String;
      begin
        Result:= 'y';
      end;

  { FakeGlyphsConfigurationDto }
    function FakeGlyphsConfigurationDto.GetConfigId: LongInt;
      begin
        Result:= 0;
      end;

    function FakeGlyphsConfigurationDto.GetItemId: LongInt;
      begin
        Result:= 3;
      end;

    function FakeGlyphsConfigurationDto.GetCategory: String;
      begin
        Result:= 'A';
      end;

    function FakeGlyphsConfigurationDto.GetGlyph: String;
      begin
        Result:= 'W';
      end;

  { FakeGlyphsConfigurationDto2 }
    function FakeGlyphsConfigurationDto2.GetConfigId: LongInt;
      begin
        Result:= 1;
      end;

    function FakeGlyphsConfigurationDto2.GetItemId: LongInt;
      begin
        Result:= 4;
      end;

    function FakeGlyphsConfigurationDto2.GetCategory: String;
      begin
        Result:= 'S';
      end;

    function FakeGlyphsConfigurationDto2.GetGlyph: String;
      begin
        Result:= 'P';
      end;



{ FakeCelestialObjectSimple }

  function FakeCelestialObjectSimple.GetMainPos: Double;
    begin
      Result:= 60.0;
    end;

  function FakeCelestialObjectSimple.GetDeviationPos: Double;
    begin
      Result:= 1.0;
    end;

  function FakeCelestialObjectSimple.GetDistancePos: Double;
    begin
      Result:= 5.0;
    end;

  function FakeCelestialObjectSimple.GetMainSpeed: Double;
    begin
      Result:= 0.5;
    end;

  function FakeCelestialObjectSimple.GetDeviationSpeed: Double;
    begin
      Result:= -0.04;
    end;

  function FakeCelestialObjectSimple.GetDistanceSpeed: Double;
    begin
      Result:= 0.01;
    end;

{ FakeCelestialObjectFull }
  function FakeCelestialObjectFull.GetObjectId: Integer;
    begin
      Result:= 6;
    end;

  function FakeCelestialObjectFull.GetEclipticalPos: ICelestialObjectSimple;
    begin
      Result:= FakeCelestialObjectSimple.Create;
    end;

  function FakeCelestialObjectFull.GetEquatorialPos: ICelestialObjectSimple;
    begin
      Result:= FakeCelestialObjectSimple.Create;
    end;

  function FakeCelestialObjectFull.GetHorizontalPos: ICelestialObjectSimple;
    begin
      Result:= FakeCelestialObjectSimple.Create;
    end;



{ FakeHouseSystemSpec }
  function FakeHouseSystemSpec.GetName: String;
    begin
      Result:= 'MySystem';
    end;

  function FakeHouseSystemSpec.GetSeId: String;
    begin
      Result:= 'M';
    end;

  function FakeHouseSystemSpec.GetDescription: String;
    begin
      Result:= 'MyDescription';
    end;

  function FakeHouseSystemSpec.GetId: Integer;
    begin
      Result:= 12;
    end;

  function FakeHouseSystemSpec.GetNumberOfHouses: Integer;
    begin
      Result:= 16;
    end;

  function FakeHouseSystemSpec.IsQuadrantSystem: Boolean;
    begin
      Result:= false;
    end;

  function FakeHouseSystemSpec.IsCuspIsStart: Boolean;
    begin
      Result:= true;
    end;


{ FakeHousePositionFull }
  function FakeHousePositionFull.GetLongitude: Double;
    begin
      Result:= 111.22;
    end;

  function FakeHousePositionFull.GetRightAscension: Double;
    begin
      Result:= 112.33;
    end;

  function FakeHousePositionFull.GetDeclination: Double;
    begin
      Result:= -1.44;
    end;

  function FakeHousePositionFull.GetAzimuth: Double;
    begin
      Result:= 88.88;
    end;


  function FakeHousePositionFull.GetAltitude: Double;
    begin
      Result:= 13.46;
    end;


{ FakeCalculationSettings }
  function FakeCalculationSettings.GetPosition: Integer;
    begin
      Result:= 1;
    end;

  function FakeCalculationSettings.GetAyanamsha: Integer;
    begin
      Result:= 0;
    end;

  function FakeCalculationSettings.GetHouseSystemSpec: IHouseSystemSpec;
    begin
      Result:= FakeHouseSystemSpec.Create;
    end;

  function FakeCalculationSettings.GetObjects: TIntArray;
    var
      Objects: TIntArray;
    begin
      SetLength(Objects, 2);
      Objects[0]:= 3;
      Objects[1]:= 4;
      Result:= Objects;
    end;

  function FakeCalculationSettings.GetReferenceFrame: IReferenceFrame;
    begin
      Result:= FakeReferenceFrame.Create;
    end;

{ FakeHousesResponse }
  function FakeHousesResponse.GetMc: IHousePositionFull;
    begin
      Result:= FakeHousePositionFull.Create;
    end;

  function FakeHousesResponse.GetAsc: IHousePositionFull;
    begin
      Result:= FakeHousePositionFull.Create;
    end;

  function FakeHousesResponse.GetCusps: THousePositionFullArray;
    var
      HousePositions: THousePositionFullArray;
    begin
      SetLength(HousePositions, 1);
      HousePositions[0] := FakeHousePositionFull.Create;
      Result:= HousePositions;
    end;

  function FakeHousesResponse.GetHouseSystemSpec: IHouseSystemSpec;
    begin
      Result:= FakeHouseSystemSpec.Create;;
    end;

{ FakeFullChartRequest }
  function FakeFullChartRequest.GetName: String;
    begin
      Result:= 'TheName';
    end;

  function FakeFullChartRequest.GetLocation: IValidatedLocation;
    begin
      Result:= FakeValidatedLocation.Create;
    end;

  function FakeFullChartRequest.GetDate: IValidatedDate;
    begin
      Result:= TValidatedDate.Create(2018, 9, 6, 'g', true);
    end;

  function FakeFullChartRequest.GetTime: IValidatedTime;
    begin
      Result:= TValidatedTime.Create(2.25, true);
    end;

  function FakeFullChartRequest.GetCalculationSettings: ICalculationSettings;
    begin
      Result:= FakeCalculationSettings.Create;
    end;

{ FakeFullChartResponse }
  function FakeFullChartResponse.GetName: String;
    begin
      Result:= 'Full Chart Name';
    end;

  function FakeFullChartResponse.GetAllObjects: TCelestialObjectFullArray;
    var
      ObjectArray: TCelestialObjectFullArray;
    begin
      SetLength(ObjectArray, 1);
      ObjectArray[0]:= FakeCelestialObjectFull.Create;
      Result:= ObjectArray;
    end;

  function FakeFullChartResponse.GetHouses: IHousesResponse;
    begin
      Result:= FakeHousesResponse.Create;
    end;

  function FakeFullChartResponse.GetFullChartRequest: IFullChartRequest;
    begin
      Result:= FakeFullChartRequest.Create;
    end;

  { FakeFullChartResponse2 }
    function FakeFullChartResponse2.GetName: String;
      begin
        Result:= 'Full Chart 2 Name';
      end;

    function FakeFullChartResponse2.GetAllObjects: TCelestialObjectFullArray;
      var
        ObjectArray: TCelestialObjectFullArray;
      begin
        SetLength(ObjectArray, 1);
        ObjectArray[0]:= FakeCelestialObjectFull.Create;
        Result:= ObjectArray;
      end;

    function FakeFullChartResponse2.GetHouses: IHousesResponse;
      begin
        Result:= FakeHousesResponse.Create;
      end;

    function FakeFullChartResponse2.GetFullChartRequest: IFullChartRequest;
      begin
        Result:= FakeFullChartRequest.Create;
      end;

end.

