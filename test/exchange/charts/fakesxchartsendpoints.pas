{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit FakesXChartsEndpoints;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XSharedEndpoints, XChartsEndpoints, XChartsDomain, FakesXChartsDomain;

type
  FakeHouseSystemDataEndpoint = class(TInterfacedObject, IHouseSystemDataEndpoint)
    function GetSystem(PId: Integer): IHouseSystemDto;
    function GetAllSystems: THouseSystemDtoArray;
  end;

  FakeAyanamshaDataEndpoint = class(TInterfacedObject, IAyanamshaDataEndpoint)
    function GetAyanamsha(PId: Integer): IAyanamshaDto;
    function GetAllAyanamshas: TAyanamshaDtoArray;
  end;

  FakeCoordinateSystemDataEndpoint = class(TInterfacedObject, ICoordinateSystemDataEndpoint)
    function GetName(PId: Integer): ILookUpValueDto;
    function GetAllItems():TLookUpValueDtoArray;
  end;

implementation

  { FakeHouseSystemDataEndpoint }
  function FakeHouseSystemDataEndpoint.GetSystem(PId: Integer): IHouseSystemDto;
    begin
      Result:= FakeHouseSystemDto.Create;
    end;

  function FakeHouseSystemDataEndpoint.GetAllSystems: THouseSystemDtoArray;
    var
      AllSystems: THouseSystemDtoArray;
    begin
      SetLength(AllSystems, 2);
      AllSystems[0]:= FakeHouseSystemDto.Create;
      AllSystems[1]:= FakeHouseSystemDto2.Create;
      Result:= AllSystems;
    end;

  { FakeAyanamshaDataEndpoint }
  function FakeAyanamshaDataEndpoint.GetAyanamsha(PId: Integer): IAyanamshaDto;
    begin
      Result:= FakeAyanamshaDto.Create;
    end;

  function FakeAyanamshaDataEndpoint.GetAllAyanamshas: TAyanamshaDtoArray;
    var
      AllAyanamshas: TAyanamshaDtoArray;
    begin
      SetLength(AllAyanamshas, 2);
      AllAyanamshas[0]:= FakeAyanamshaDto.Create;
      AllAyanamshas[1]:= FakeAyanamshaDto2.Create;
      Result:= AllAyanamshas;
    end;

  { FakeCoordinateSystemDataEndpoint }
  function FakeCoordinateSystemDataEndpoint.GetName(PId: Integer): ILookUpValueDto;
    begin
      Result:= FakeLookUpValueDto.Create;
    end;

  function FakeCoordinateSystemDataEndpoint.GetAllItems():TLookUpValueDtoArray;
    var
      AllValues: TLookUpValueDtoArray;
    begin
      SetLength(AllValues, 2);
      AllValues[0]:= FakeLookUpValueDto.Create;
      AllValues[1]:= FakeLookUpValueDto2.Create;
      Result:= AllValues;
    end;

end.

