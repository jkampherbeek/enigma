{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseXSharedEndpoints;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, XSharedDomain, XSharedEndpoints;

type
  TestValidatedLatitudeEndpoint = class(TTestCase)
  published
    procedure TestHandleInput;
    procedure TestHandleInputError;
  end;

  TestValidatedLongitudeEndpoint = class(TTestCase)
  published
    procedure TestHandleInput;
    procedure TestHandleInputError;
  end;

   TestValidatedDateEndpoint = class(TTestCase)
  published
    procedure TestHandleInput;
    procedure TestHandleInputError;
  end;

  TestValidatedTimeEndpoint = class(TTestCase)
  published
    procedure TestHandleInput;
    procedure TestHandleInputError;
  end;


implementation

uses XSharedEndpointsImpl, XSharedHandlers, XSharedHandlersImpl, BeSharedSeFrontend;

const
  MARGIN = 0.000000001;

{ TestValidatedLatitudeEndpoint }
procedure TestValidatedLatitudeEndpoint.TestHandleInput;
var
  Degree, Minute, Second, Direction: String;
  Latitude: IValidatedDouble;
  Endpoint: TValidatedLatitudeEndpoint;
begin
  Endpoint:=TValidatedLatitudeEndpoint.Create(TLatitudeValidationHandler.Create);
  Degree:='50';
  Minute:='30';
  Second:='0';
  Direction:='N';
  Latitude:=Endpoint.HandleInput(Degree, Minute, Second, Direction);
  CheckEquals(50.5, Latitude.Value);
  CheckTrue(Latitude.Valid);
end;

procedure TestValidatedLatitudeEndpoint.TestHandleInputError;
var
  Degree, Minute, Second, Direction: String;
  Latitude: IValidatedDouble;
  Endpoint: TValidatedLatitudeEndpoint;
begin
  Endpoint:=TValidatedLatitudeEndpoint.Create(TLatitudeValidationHandler.Create);
  Degree:='91';
  Minute:='30';
  Second:='0';
  Direction:='N';
  Latitude:=Endpoint.HandleInput(Degree, Minute, Second, Direction);
  CheckEquals(0.0, Latitude.Value);
  CheckFalse(Latitude.Valid);
end;

{ TestValidatedLongitudeEndpoint }
procedure TestValidatedLongitudeEndpoint.TestHandleInput;
var
  Degree, Minute, Second, Direction: String;
  Longitude: IValidatedDouble;
  Endpoint: TValidatedLongitudeEndpoint;
begin
  Endpoint:=TValidatedLongitudeEndpoint.Create(TLongitudeValidationHandler.Create);
  Degree:='22';
  Minute:='30';
  Second:='0';
  Direction:='E';
  Longitude:=Endpoint.HandleInput(Degree, Minute, Second, Direction);
  CheckEquals(22.5, Longitude.Value);
  CheckTrue(Longitude.Valid);
end;

procedure TestValidatedLongitudeEndpoint.TestHandleInputError;
var
  Degree, Minute, Second, Direction: String;
  Longitude: IValidatedDouble;
  Endpoint: TValidatedLongitudeEndpoint;
begin
  Endpoint:=TValidatedLongitudeEndpoint.Create(TLongitudeValidationHandler.Create);
  Degree:='22';
  Minute:='60';
  Second:='0';
  Direction:='E';
  Longitude:=Endpoint.HandleInput(Degree, Minute, Second, Direction);
  CheckEquals(0.0, Longitude.Value);
  CheckFalse(Longitude.Valid);
end;


{ TestValidatedDateEndpoint }
procedure TestValidatedDateEndpoint.TestHandleInput;
var
  Year, Month, Day, Calendar: String;
  Date: IValidatedDate;
  CheckedDate: TValidatedDateEndpoint;
begin
  CheckedDate:=TValidatedDateEndpoint.Create(TDateValidationHandler.Create(TSeFrontend.Create));
  Year:='1953';
  Month:='1';
  Day:='29';
  Calendar:='g';
  Date:=CheckedDate.HandleInput(Year, Month, Day, Calendar);
  CheckEquals(1953, Date.Year);
  CheckEquals(1, Date.Month);
  CheckEquals(29, Date.Day);
  CheckEquals(Calendar, Date.Calendar);
  CheckTrue(Date.Valid);
end;

procedure TestValidatedDateEndpoint.TestHandleInputError;
var
  Year, Month, Day, Calendar: String;
  Date: IValidatedDate;
  CheckedDate: IValidatedDateEndpoint;
  SeFrontend: TSeFrontend;
begin
  SeFrontend:= TSeFrontend.Create;
  CheckedDate:=TValidatedDateEndpoint.Create(TDateValidationHandler.Create(TSeFrontend.Create));
  Year:='1953';
  Month:='13';
  Day:='29';
  Calendar:='g';
  Date:=CheckedDate.HandleInput(Year, Month, Day, Calendar);
  CheckFalse(Date.Valid);
end;

{ TestValidatedTimeEndpoint }
procedure TestValidatedTimeEndpoint.TestHandleInput;
var
  Hour, Minute, Second: String;
  Time: IValidatedTime;
  TimeEndpoint: TValidatedTimeEndpoint;
begin
  TimeEndpoint:=TValidatedTimeEndpoint.Create(TTimeValidationHandler.Create);
  Hour:='7';
  Minute:='48';
  Second:='23';
  Time:=TimeEndpoint.HandleInput(Hour, Minute, Second);
  CheckEquals(7.806388888888889, Time.DecimalTime, Margin);
  CheckTrue(Time.Valid);
end;

procedure TestValidatedTimeEndpoint.TestHandleInputError;
var
  Hour, Minute, Second: String;
  Time: IValidatedTime;
  TimeEndpoint: TValidatedTimeEndpoint;
begin
  TimeEndpoint:=TValidatedTimeEndpoint.Create(TTimeValidationHandler.Create);
  Hour:='-7';
  Time:=TimeEndpoint.HandleInput(Hour, Minute, Second);
  CheckEquals(0.0, Time.DecimalTime);
  CheckFalse(Time.Valid);
end;




initialization

RegisterTest('XSharedEndpoints',TestValidatedLatitudeEndpoint);
RegisterTest('XSharedEndpoints',TestValidatedLongitudeEndpoint);
RegisterTest('XSharedEndpoints',TestValidatedDateEndpoint);
RegisterTest('XSharedEndpoints',TestValidatedTimeEndpoint);

end.

