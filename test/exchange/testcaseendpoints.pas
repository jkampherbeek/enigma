unit TestCaseEndpoints;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, XSharedDomain, XSharedEndpoints;

type

  TestFullChartEndpoint = class(TTestCase)
  published
    procedure TestHandleRequest;
  end;


implementation

const
  MARGIN = 0.000000001;






{ TestFullChartEndpoint }
procedure TestFullChartEndpoint.TestHandleRequest;

begin
  CheckTrue(True);
end;

initialization



  RegisterTest('Endpoints',TestFullChartEndpoint);

end.

